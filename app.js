var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require("body-parser");
bodyParser.urlencoded({limit: '50mb', extended: true});
var querys = require('./model/dbQuerys/querys');
var mids = require('./middleware')
var app = express();
var url = require('url') ;
var statics = require('./static');
var indexRouter = require('./routes/index');
var permissionDeniedRouter = require('./routes/permission_denied');
var loginRouter = require('./routes/auth/login');
var siteContentHomeRouter = require('./routes/SiteContent/home');
var siteContentLoyalityRouter = require('./routes/SiteContent/loyality');
var brandRouter = require('./routes/Brand/brand');
var blogRouter = require('./routes/Blog/blog');
var termsRouter = require('./routes/Terms/terms');
var privacyRouter = require('./routes/Privacy/privacy');
var historyRouter = require('./routes/History/history');
var cookieRouter = require('./routes/Cookie/cookie');
var faqRouter = require('./routes/Faq/faq');
var filterRouter = require('./routes/Filter/filter');
var filterPropertiesRouter = require('./routes/FilterProperties/filterProperties');
var categoriesRouter = require('./routes/Categories/categories');
var productRouter = require('./routes/Product/product');
var translateRouter = require('./routes/Translate/translate');
var aboutRouter = require('./routes/About/about');
var deliveryRouter = require('./routes/Delivery/delivery');
var refPolicyRouter = require('./routes/RefPolicy/refPolicy');
var litrageRouter = require('./routes/Liitrage/litrage');
var settingsRouter = require('./routes/Settings/settings');
var ordersRouter = require('./routes/Orders/orders');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ extended: false,limit: '50mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var middleware = require('./middleware')
// check Cookie exists or not
app.use(function(req, res, next) {
    var cookie = req.cookies['clin'];

    if(typeof cookie === 'undefined'){
        if(req.originalUrl === '/login'){
            next();
        }else{
            res.redirect('/login');
        }
    }else{
        querys.checkUser(cookie)
            .then(function (user) {

                if (user.length == 1) {
                    req.userInfo = user[0];
                    if(req.originalUrl === '/login'){
                        res.redirect('/');
                    }else{
                        next();
                    }

                }else{
                    if(req.originalUrl === '/login'){
                        next();
                    }else{
                        res.redirect('/login');
                    }
                }
            })
            .catch(function (error) {
                res.redirect('/login');
            })
    }
    // next()
});

// select all require info
app.use(function(req, res, next) {
    req.siteData = {};
    req.siteData.newCounts = {};
    req.siteData.ourMoney = 0;
    req.siteData.getNewOrdersCount = 0;
    var allQuery =
        [
            querys.findAllPromise('language',null)
        ];
    Promise.all(allQuery)
        .then(function ([language]) {
            // console.log('.........',C2CNewCount[0].counts);
            req.language = language;
            next();
        })
        .catch(function (error) {
            console.log('error',error)
        })
});

app.use(function(req, res, next) {
    var pathname = req.originalUrl;
    var pathName = pathname.split('/');
    // console.log('pathName', pathName)
    app.locals.pathName = pathName[1];
    app.locals.SpathName = pathName[2];
    app.locals.TpathName = pathName[3];
    // console.log('pathName', pathName[1])
    // console.log('SpathName', pathName[2])
    // console.log('TpathName', pathName[3]);
    app.locals.API_FOLDER_NAME = statics.API_FOLDER_NAME
    app.locals.API_URL = statics.API_URL
    app.locals.MAIN_URL = statics.MAIN_URL
    next();
});

app.use(function (req, res, next) {middleware.getNewOrdersCount(req, res, next)});


app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/permission-denied', permissionDeniedRouter);
app.use('/site-content/home', siteContentHomeRouter);
app.use('/loyality', siteContentLoyalityRouter);
app.use('/brand', brandRouter);
app.use('/blog', blogRouter);
app.use('/terms', termsRouter);
app.use('/privacy', privacyRouter);
app.use('/history', historyRouter);
app.use('/cookie', cookieRouter);
app.use('/faq', faqRouter);
app.use('/filter', filterRouter);
app.use('/filter-properties', filterPropertiesRouter);
app.use('/categories', categoriesRouter);
app.use('/product', productRouter);
app.use('/translate', translateRouter);
app.use('/about', aboutRouter);
app.use('/delivery', deliveryRouter);
app.use('/ref-policy', refPolicyRouter);
app.use('/litrage', litrageRouter);
app.use('/settings', settingsRouter);
app.use('/orders', ordersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.render('not_found');
    // next(createError(404));
});




module.exports = app;
