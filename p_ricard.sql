-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Ноя 25 2020 г., 17:08
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `p_ricard`
--

-- --------------------------------------------------------

--
-- Структура таблицы `additional_filters`
--

CREATE TABLE `additional_filters` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `additional_filters_language`
--

CREATE TABLE `additional_filters_language` (
  `id` int(11) NOT NULL,
  `additional_filters_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `val` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `additional_filters_language`
--

INSERT INTO `additional_filters_language` (`id`, `additional_filters_id`, `product_id`, `lang_id`, `name`, `val`) VALUES
(23, NULL, 26, 1, 'Aged', '50 years'),
(24, NULL, 26, 2, 'Տարիք', '50 տարի'),
(25, NULL, 26, 1, 'Packaging', 'Luxury gift box'),
(26, NULL, 26, 2, 'Փաթեթավորում', 'luxury gift box'),
(27, NULL, 26, 1, 'Producer', ' ARARAT (Yerevan Brandy Company)'),
(28, NULL, 26, 2, 'Արտադրող', 'ARARAT (Yerevan Brandy Company)'),
(29, NULL, 27, 1, 'Packaging', 'Cardboard gift box'),
(30, NULL, 27, 2, 'Փաթեթավորում', 'ստվարաթղթե տուփ'),
(31, NULL, 27, 1, 'Producer', ' ARARAT (Yerevan Brandy Company)'),
(32, NULL, 27, 2, 'Արտադրող', 'ARARAT (Yerevan Brandy Company)'),
(33, NULL, 28, 1, 'Volume', ' 0.5 L, 0.7 L'),
(34, NULL, 28, 2, 'Ծավալ', '0.5 L, 0.7 L'),
(35, NULL, 28, 1, 'Producer', 'ARARAT (Yerevan Brandy Company)'),
(36, NULL, 28, 2, 'Արտադրող', ' ARARAT (Yerevan Brandy Company)'),
(37, NULL, 29, 1, 'Համային', 'lime'),
(38, NULL, 29, 2, 'Flavour', 'lime'),
(39, NULL, 29, 1, 'Արտադրող', 'ABSOLUT'),
(40, NULL, 29, 2, 'Producer', 'ABSOLUT'),
(41, NULL, 30, 1, 'Flavour', 'grapefruit'),
(42, NULL, 30, 2, 'Համային', 'grapefruit'),
(43, NULL, 30, 1, 'Producer', ' ABSOLUT'),
(44, NULL, 30, 2, 'Արտադրող', 'ABSOLUT'),
(45, NULL, 31, 1, 'Producer', 'WYBOROWA'),
(46, NULL, 31, 2, 'Արտադրող', 'WYBOROWA'),
(47, NULL, 32, 1, 'Producer', 'ABSOLUT '),
(48, NULL, 32, 2, 'Արտադրող', 'ABSOLUT'),
(49, NULL, 33, 1, 'Producer', 'ABSOLUT'),
(50, NULL, 33, 2, 'Համային', 'մանդարին'),
(51, NULL, 34, 1, 'Classification /cognac/', ' VS (Very Special)'),
(52, NULL, 34, 2, 'Դասակարգում', 'VS (Very Special)'),
(53, NULL, 34, 1, 'Packaging', 'Cardboard gift box'),
(54, NULL, 34, 2, 'Փաթեթավորում', 'Cardboard gift box'),
(55, NULL, 34, 1, 'Producer', 'MARTELL'),
(56, NULL, 34, 2, 'Արտադրող', 'MARTELL'),
(57, NULL, 35, 1, 'Classification /cognac/', 'XO (Extra Old)'),
(58, NULL, 35, 2, 'Դասակարգում', 'XO (Extra Old)'),
(59, NULL, 35, 1, 'Packaging', 'Cardboard gift box'),
(60, NULL, 35, 2, 'Փաթեթավորում', 'Cardboard gift box'),
(61, NULL, 35, 1, 'Producer', 'MARTELL'),
(62, NULL, 35, 2, 'Արտադրող', 'MARTELL'),
(63, NULL, 36, 1, 'Flavour', 'vanilla'),
(64, NULL, 36, 2, 'Համային', 'վանիլ'),
(65, NULL, 36, 1, 'Producer', ' ABSOLUT'),
(66, NULL, 36, 2, 'Արտադրող', ' ABSOLUT'),
(67, NULL, 37, 1, 'Flavour', 'blackcurrant'),
(68, NULL, 37, 2, 'Համային', 'սև հաղարջ'),
(69, NULL, 37, 1, 'Producer', 'ABSOLUT'),
(70, NULL, 37, 2, 'Արտադրող', 'ABSOLUT '),
(71, NULL, 38, 1, 'Flavour', 'raspberry'),
(72, NULL, 38, 2, 'Համային', 'ազնվամորի'),
(73, NULL, 38, 1, 'Producer', ' ABSOLUT'),
(74, NULL, 38, 2, 'Արտադրող', 'ABSOLUT');

-- --------------------------------------------------------

--
-- Структура таблицы `admin_login`
--

CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL,
  `log` varchar(100) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admin_login`
--

INSERT INTO `admin_login` (`id`, `log`, `pass`, `token`, `type`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', '59ea08d4rd3945sdf2fd3cecace01b98b39e1c66', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `insert_date` datetime NOT NULL DEFAULT current_timestamp(),
  `ord` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog`
--

INSERT INTO `blog` (`id`, `image`, `slug`, `insert_date`, `ord`) VALUES
(1, '1043182-1605180899488.jpeg', 'title-am-u-1', '2020-11-12 15:08:01', 0),
(3, '693353-1605260232903.jpeg', 'w-eqewqe-3', '2020-11-13 13:37:12', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `blog_language`
--

CREATE TABLE `blog_language` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog_language`
--

INSERT INTO `blog_language` (`id`, `blog_id`, `title`, `text`, `lang_id`) VALUES
(1, 1, 'Title end', '<p><span style=\"color: rgb(45, 55, 72); font-family: Roboto;\">Text En</span><br></p>', 1),
(2, 1, 'Title am u', '<p><span style=\"color: rgb(45, 55, 72); font-family: Roboto;\">Text am</span><br></p>', 2),
(5, 3, 'sad', '<p>as ad sa dasd</p><p>as</p><p>d as</p>', 1),
(6, 3, 'w eqewqe', '<p>wq wq e</p><p>qw e</p><p>wq</p>', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `image` varchar(250) NOT NULL,
  `brand_logo` varchar(200) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `ord` int(11) NOT NULL DEFAULT 0,
  `insert_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `brand`
--

INSERT INTO `brand` (`id`, `cat_id`, `image`, `brand_logo`, `slug`, `ord`, `insert_date`) VALUES
(15, 4, '864324-1605546494456.jpeg', '910572-1605546494455.png', 'ararat-15', 0, '2020-11-16 21:08:14'),
(17, 3, '7712-1605602938382.jpeg', '172691-1605602938379.png', 'glenlivet-17', 2, '2020-11-17 12:48:58'),
(18, 10, '644692-1605881158342.jpeg', '799187-1605881158339.png', 'oghi-absolyut-18', 1, '2020-11-20 18:05:58'),
(19, 10, '1105749-1605961186176.jpeg', '49253-1605961186174.png', 'wyborowa-vodka-19', 0, '2020-11-21 16:19:46'),
(20, 4, '903256-1606301578202.jpeg', '1192789-1606301578201.png', 'martell-cognac-20', 0, '2020-11-25 14:52:58');

-- --------------------------------------------------------

--
-- Структура таблицы `brand_language`
--

CREATE TABLE `brand_language` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `text` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `brand_language`
--

INSERT INTO `brand_language` (`id`, `brand_id`, `lang_id`, `title`, `name`, `text`) VALUES
(29, 15, 1, 'Ararat', 'ARARAT (YEREVAN BRANDY COMPANY)', '<p><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">The biography of the first Armenian ARARAT brandy is a history of generosity, passion, and craftsmanship. The culture of winemaking in Armenia goes back centuries, and the Armenian brandy that is famous around the world today appeared at the end of the 19th century. More Armenian brandy production was started in 1887 in Yerevan by the merchant and philanthropist Nerses Tairyan</span><br></p>'),
(30, 15, 2, 'Արարատ', 'Արարատ Կոնյակ', '<p><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">ARARAT առաջին հայկական կոնյակի կենսագրությունը շռայլության, կրքի ու վարպետության պատմություն է: Գինեգործության մշակույթը Հայաստանում հաշվվում է ոչ մեկ հազարամայակ, սակայն այսօր աշխարհում հայտնի հայկական կոնյակը լույս աշխարհ է եկել միայն XIX դարի վերջին: Ընդամենը քառորդ դար հետո հայկական կոնյակը հասավ միջազգային ճանաչման, իսկ նրա արտադրությունը ձեռք բերեց իրապես կայսերական թափ: Նույնիսկ հեղափոխակ</span><br></p>'),
(33, 17, 1, 'Glenlivet', 'Glenlivet', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</span><br></p>'),
(34, 17, 2, 'Glenlivet', 'Glenlivet', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Հայտնի է, որ ընթերցողը, կարդալով հասկանալի տեքստ, չի կարողանա կենտրոնանալ տեքստի ձևավորման վրա: Lorem Ipsum օգտագործելը բացատրվում է նրանով, որ այն բաշխում է բառերը քիչ թե շատ իրականի նման, ի տարբերություն «Բովանդակություն, բովանդակություն» սովորական կրկննության, ինչը ընթերցողի համար հասկանալի է: Շատ համակարգչային տպագրական ծրագրեր և ինտերնետային էջերի խմբագրիչներ այն օգտագործում են որպես իրենց ստանդարտ տեքստային մոդել, և հետևապես, ինտերնետում Lorem Ipsum-ի որոնման արդյունքում կարելի է հայտնաբերել էջեր, որոնք դեռ նոր են կերտվում: Ժամանակի ընթացքում ձևավորվել են Lorem Ipsum-ի տարբեր վերսիաներ` երբեմն ներառելով պատահական տեքստեր, երբեմն էլ հատուկ իմաստ (հումոր և նմանատիպ բովանդակություն):</span><br></p>'),
(35, 18, 1, 'Absolute Vodka', 'Absolute', '<p><strong style=\"margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><br></p>'),
(36, 18, 2, 'Օղի Աբսոլյւտ', 'Աբսոլյւտ', '<h2 style=\"margin-bottom: 10px; padding: 0px; font-weight: 400; font-family: DauphinPlain; font-size: 24px; line-height: 24px;\">Ինչո՞ւ ենք այն օգտագործում</h2><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Հայտնի է, որ ընթերցողը, կարդալով հասկանալի տեքստ, չի կարողանա կենտրոնանալ տեքստի ձևավորման վրա: Lorem Ipsum օգտագործելը բացատրվում է նրանով, որ այն բաշխում է բառերը քիչ թե շատ իրականի նման, ի տարբերություն «Բովանդակություն, բովանդակություն» սովորական կրկննության, ինչը ընթերցողի համար հասկանալի է: Շատ համակարգչային տպագրական ծրագրեր և ինտերնետային էջերի խմբագրիչներ այն օգտագործում են որպես իրենց ստանդարտ տեքստային մոդել, և հետևապես, ինտերնետում Lorem Ipsum-ի որոնման արդյունքում կարելի է հայտնաբերել էջեր, որոնք դեռ նոր են կերտվում: Ժամանակի ընթացքում ձևավորվել են Lorem Ipsum-ի տարբեր վերսիաներ` երբեմն ներառելով պատահական տեքստեր, երբեմն էլ հատուկ իմաստ (հումոր և նմանատիպ բովանդակություն):</p>'),
(37, 19, 1, 'Wyborowa vodka', 'Wyborowa', '<p>Wyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodka<br></p>'),
(38, 19, 2, 'Wyborowa vodka', 'Wyborowa', '<p>Wyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodkaWyborowa vodka<br></p>'),
(39, 20, 1, 'MARTELL Cognac', 'MARTELL ', '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</span><br></p>'),
(40, 20, 2, 'MARTELL Cognac', 'MARTELL ', '<h2 style=\"margin-bottom: 10px; padding: 0px; font-weight: 400; font-family: DauphinPlain; font-size: 24px; line-height: 24px;\">Ինչո՞ւ ենք այն օգտագործում</h2><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Հայտնի է, որ ընթերցողը, կարդալով հասկանալի տեքստ, չի կարողանա կենտրոնանալ տեքստի ձևավորման վրա: Lorem Ipsum օգտագործելը բացատրվում է նրանով, որ այն բաշխում է բառերը քիչ թե շատ իրականի նման, ի տարբերություն «Բովանդակություն, բովանդակություն» սովորական կրկննության, ինչը ընթերցողի համար հասկանալի է: Շատ համակարգչային տպագրական ծրագրեր և ինտերնետային էջերի խմբագրիչներ այն օգտագործում են որպես իրենց ստանդարտ տեքստային մոդել, և հետևապես, ինտերնետում Lorem Ipsum-ի որոնման արդյունքում կարելի է հայտնաբերել էջեր, որոնք դեռ նոր են կերտվում: Ժամանակի ընթացքում ձևավորվել են Lorem Ipsum-ի տարբեր վերսիաներ` երբեմն ներառելով պատահական տեքստեր, երբեմն էլ հատուկ իմաստ (հումոր և նմանատիպ բովանդակություն):</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `ord` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `slug`, `image`, `ord`) VALUES
(3, 'whisky-3', '1282778-1605865659442.svg', 3),
(4, 'brandy-cognac-4', '334531-1605865495865.svg', 0),
(5, 'tequila-5', '1253994-1605865531344.svg', 2),
(10, 'oghi-10', '529415-1605881060039.svg', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `categories_language`
--

CREATE TABLE `categories_language` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories_language`
--

INSERT INTO `categories_language` (`id`, `cat_id`, `lang_id`, `name`) VALUES
(5, 3, 1, 'Whisky'),
(6, 3, 2, 'Վիսկի'),
(7, 4, 1, 'Brandy/Cognac'),
(8, 4, 2, 'Բրենդի/ Կոնյակ'),
(9, 5, 1, 'Tequila'),
(10, 5, 2, 'Տեկիլա'),
(19, 10, 1, 'Vodka'),
(20, 10, 2, 'Օղի');

-- --------------------------------------------------------

--
-- Структура таблицы `cookie`
--

CREATE TABLE `cookie` (
  `id` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cookie`
--

INSERT INTO `cookie` (`id`, `text`, `lang_id`) VALUES
(1, '<h2 style=\"margin-bottom: 10px; padding: 0px; font-weight: 400; font-family: DauphinPlain; font-size: 24px; line-height: 24px;\"><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the</span><br></h2>', 1),
(2, '<h2 style=\"margin-bottom: 10px; padding: 0px; font-weight: 400; font-family: DauphinPlain; font-size: 24px; line-height: 24px;\">Ի՞նչ ծագում ունի այն</h2><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Հակառակ ընդհանուր պատկերացմանը` Lorem Ipsum-ը այդքան էլ պատահական հավաքված տեքստ չէ: Այս տեքստի արմատները հասնում են Ք.ա. 45թ. դասական լատինական գրականություն. այն 2000 տարեկան է: Ռիչարդ ՄքՔլինտոքը` Վիրջինիայի Համպդեն-Սիդնեյ քոլեջում լատիներենի մի դասախոս` ուսումնասիրելով Lorem Ipsum տեքստի ամենատարօրինակ բառերից մեկը` consectetur-ը, և այն որոնելով դասական գրականության մեջ` բացահայտեց դրա իսկական աղբյուրը: Lorem Ipsum-ը ամրագրված է Ք.ա 45թ. Ցիցերոնի \"de Finibus Bonorum et Malorum\" (Չարի և բարու ծայրահեղությունների մասին) ստեղծագործության 1.10.32 և 1.10.33 բաժիններում: Այս գիրքը Վերածննդի ժամանակաշրջանում էթիկայի տեսության հայտնի աշխատություն է եղել: Lorem Ipsum-ի առաջին տողը` \"Lorem ipsum dolor sit amet..\", 1.10.32 բաժնից է:</p>', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `ord` int(11) NOT NULL DEFAULT 0,
  `insert_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `faq`
--

INSERT INTO `faq` (`id`, `ord`, `insert_date`) VALUES
(4, 0, '2020-11-13 13:08:58'),
(5, 2, '2020-11-13 13:40:57'),
(6, 1, '2020-11-13 13:41:51');

-- --------------------------------------------------------

--
-- Структура таблицы `faq_language`
--

CREATE TABLE `faq_language` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `faq_id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `question` text DEFAULT NULL,
  `answer` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `faq_language`
--

INSERT INTO `faq_language` (`id`, `lang_id`, `faq_id`, `name`, `question`, `answer`) VALUES
(7, 1, 4, 'Name en', 'Question en', 'Answer en'),
(8, 2, 4, 'Հաճախ ', '1. Կատարել եմ առցանց պատվեր։ Քանի՞ օրում կստանամ իմ պատվիրված ապրանքը։', 'Առցանց պատվերի դեպքում ապրանքը Ձեզ կհասնի 24 աշխատանքային ժամվա ընթացքում։'),
(9, 1, 5, 'sad', 'asd', 'xzc'),
(10, 2, 5, 'qwe', '3. Ունե՞ք պահեստամասի առաքման ծառայություն։', 'Այո։ Պատվերի ժամանակ կարող եք նշել թե ինչպես եք ցանկանում ստանալ պահեստամասը։'),
(11, 1, 6, 'ad', 'asd', 'asd'),
(12, 2, 6, 'qe', '2. Հնարավո՞ր է արդյոք վերադարձնել կամ փոխանակել արդեն գնված պահեստամասը։', 'Ցանկացած պահեստամաս (բացի էլեկտրոնայիններից) ենթակա է վերադարձի կամ փոխանակման գնման օրվանից հետո 14 օրվա ընթացքում, ՀԴՄ կտրոնի առկայության դեպքում, եթե ապրանքը չի օգտագործվել ու չի վնասվել։');

-- --------------------------------------------------------

--
-- Структура таблицы `favorite`
--

CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `insert_data` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `favorite`
--

INSERT INTO `favorite` (`id`, `guest_id`, `user_id`, `product_id`, `insert_data`) VALUES
(1, NULL, 10, 4, NULL),
(2, NULL, 10, 2, NULL),
(3, 60, NULL, 5, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `filter`
--

CREATE TABLE `filter` (
  `id` int(11) NOT NULL,
  `type` enum('0','1') NOT NULL COMMENT '0 - normal, 1 - color',
  `ord` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `filter`
--

INSERT INTO `filter` (`id`, `type`, `ord`) VALUES
(1, '0', 1),
(2, '0', 2),
(10, '1', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `filter_language`
--

CREATE TABLE `filter_language` (
  `id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `filter_language`
--

INSERT INTO `filter_language` (`id`, `filter_id`, `lang_id`, `name`) VALUES
(1, 1, 1, 'Litrage'),
(2, 1, 2, 'ԾԱՎԱԼ Լ'),
(3, 2, 1, 'Volume'),
(4, 2, 2, 'ԹՆԴՈՒԹՅՈՒՆ'),
(17, 10, 1, 'Country'),
(18, 10, 2, 'Երկիր');

-- --------------------------------------------------------

--
-- Структура таблицы `filter_product`
--

CREATE TABLE `filter_product` (
  `id` int(11) NOT NULL,
  `filter_prop_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `filter_product`
--

INSERT INTO `filter_product` (`id`, `filter_prop_id`, `product_id`) VALUES
(13, 2, 26),
(14, 16, 26),
(15, 1, 27),
(16, 16, 27),
(17, 2, 28),
(18, 16, 28),
(19, 1, 29),
(20, 16, 29),
(21, 1, 30),
(22, 16, 30),
(23, 1, 31),
(24, 16, 31),
(25, 18, 26),
(26, 18, 27),
(27, 18, 28),
(28, 17, 29),
(29, 17, 30),
(30, 17, 31),
(31, 1, 32),
(32, 16, 32),
(33, 17, 32),
(34, 1, 33),
(35, 16, 33),
(36, 17, 33),
(37, 2, 34),
(38, 16, 34),
(39, 17, 34),
(40, 2, 35),
(41, 16, 35),
(42, 17, 35),
(43, 1, 36),
(44, 16, 36),
(45, 18, 36),
(46, 2, 37),
(47, 16, 37),
(48, 17, 37),
(49, 2, 38),
(50, 16, 38),
(51, 17, 38);

-- --------------------------------------------------------

--
-- Структура таблицы `filter_prop`
--

CREATE TABLE `filter_prop` (
  `id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  `ord` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `filter_prop`
--

INSERT INTO `filter_prop` (`id`, `filter_id`, `ord`) VALUES
(1, 1, 0),
(2, 1, 1),
(6, 1, 2),
(10, 2, 0),
(11, 2, 0),
(12, 2, 0),
(14, 2, 0),
(15, 1, 0),
(16, 2, 0),
(17, 10, 0),
(18, 10, 0),
(19, 10, 0),
(20, 10, 0),
(21, 10, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `filter_prop_language`
--

CREATE TABLE `filter_prop_language` (
  `id` int(11) NOT NULL,
  `filter_prop_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `filter_prop_language`
--

INSERT INTO `filter_prop_language` (`id`, `filter_prop_id`, `lang_id`, `name`) VALUES
(1, 1, 1, '0.5 Litre'),
(2, 1, 2, '0.5 Լիտր'),
(3, 2, 1, '0.7 Litre'),
(4, 2, 2, '0.7 Լիտր'),
(9, 6, 1, '1 Litre'),
(10, 6, 2, '1 Լիտր'),
(17, 10, 1, '80%'),
(18, 10, 2, '80%'),
(19, 11, 1, '12%'),
(20, 11, 2, '12%'),
(21, 12, 1, '8%'),
(22, 12, 2, '8%'),
(25, 14, 1, 'asd'),
(26, 14, 2, 'www'),
(27, 15, 1, '3.5 Litre'),
(28, 15, 2, '3.5 Լիտր'),
(29, 16, 1, '40%'),
(30, 16, 2, '40%'),
(31, 17, 1, 'Sweden'),
(32, 17, 2, 'Շվեդիա'),
(33, 18, 1, 'Armenia'),
(34, 18, 2, 'Հայաստան'),
(35, 19, 1, 'Poland'),
(36, 19, 2, 'Լեհաստան'),
(37, 20, 1, 'Russia'),
(38, 20, 2, 'Ռուսաստան'),
(39, 21, 1, 'USA'),
(40, 21, 2, 'ԱՄՆ');

-- --------------------------------------------------------

--
-- Структура таблицы `guest`
--

CREATE TABLE `guest` (
  `id` int(11) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `guest`
--

INSERT INTO `guest` (`id`, `token`) VALUES
(58, '2689abfa22ebad34d98db4225f188a3686d0e95c'),
(59, 'f4756a114df3c9a3f38700c6f4e0c2c330094900'),
(60, 'f16b1cc1218198a1fbb51ef372dbdd3f7b2fae5c'),
(61, '3b96e7f806fe6a640098baccb8b9b0041e68aebb'),
(62, '0359a56e6abf63aad6ee56ab50cc9cbfe235c3d1'),
(63, '27073879ca94f9aba073ef1297012dc9c613e495'),
(64, '2b9b0bec06cf2281c9607ff9c38f6c8b603e2098'),
(65, 'f073f5b4bd2ef3d11ece82de60435f769d5b2043'),
(66, 'cab29001a9745918b1c04c2c9dca579f9bada030'),
(67, '97e9bc1b401c18dfc14544eadf9de0a0c2533e1b'),
(68, '1c5f71e9eea085f9d26291244d9cb38cb9092878'),
(69, '5f08bfdaeb9496d0f40728519ea4890eb9e727e7'),
(70, '7bec84d6e70f1fc318928b328d9598add306035b'),
(71, '9261c2ac0a6c72858efef18cdb54755355f43353'),
(72, 'a9ff9e80cd37da875fcd28e7e470025265f2b91f'),
(73, '2ad967f716d8aaf13248010ee093afea8236806d'),
(74, 'b68df6a940bb28a9018cbacfc294dcf7fd6c28d9'),
(75, '10ffb6e5e35c7e8255564dd97d54d506f2edc0b6'),
(76, 'a5203a0e2896100ec12e6a028469098e35bab0f7'),
(77, '838cdb40b00de7d5df765e6287b20af07ae651e4'),
(78, '4f6dcea12d4d661bbbe0d1678211d2d1c96970ce'),
(79, '29ddc29ebb28e64c50eecc4d7559f141df3eb02d'),
(80, '355444b568537beb4d4b5cb11080bb5da13645e7'),
(81, '70776e6e4ed7bb09525a9ed607aed998f7656e11'),
(82, '26cb9b627e8b9b8bc1182642b1ac00b08bbb3913'),
(83, '2bb1bae1a0795a9511a60c214ffc9a51a1253acd'),
(84, '95196ee74fc64fa526c3b38580cce1ac0e36b5ee'),
(85, 'f4bcc32af5c4db28f3912ddddacf9090139eb61a'),
(86, 'da791c768f8c264ba8983ead361eda4556ceace2'),
(87, 'fd23fa8f069a562f2b68a5b7f413d288ac7cae57'),
(88, '9994bf9f4c011f2b583b97ac2687310ae8582f13'),
(89, '4e6d06194f4fd11a37742606e1a9c5cc834f373b'),
(90, '98b32c3bf0be6fc8980bd828f6d9cd39d8b590b7'),
(91, '2a47d75f0e0aa4467e5d536a0941965a9133511e'),
(92, '4db847ec2f74050d313a1203b7a89c2d7826b55c'),
(93, 'a3c481b43f2297f5a0ce73038aa120d214c96ae4'),
(94, '61c1b655bfed78d5798c180d058dd505bbc870ef'),
(95, '3590f6b7ed691fad467014eddd22a7c1649c9c16'),
(96, '2fb03ee69ab9edc485b1d8518299c35daba5698a'),
(97, '3d3d7295a66c280b75631639d38ba2862667c95c'),
(98, '08fedd77c1d95c6b77ed964010b8fc26468934be'),
(99, '63e42463e2035d051e56287b31795c83ae737633'),
(100, '883b2edc3f74208b815eae43d8841d7eee7996fc'),
(101, 'ec639722164107692540affe7fae8e62ae6d3cae'),
(102, 'fa2bd04d321d902ea1a2415a3af5037a9e61127c'),
(103, '179dcadea899d9c3551df9dd25cf98ea3afdca95'),
(104, 'da46e11cb615bbab9a98b2696917a4231b76f252'),
(105, '255256948bdd5108daf56cc6730a64f9edec78a0'),
(106, '818205967a789038de07471484c857fc8a0cf053'),
(107, 'eb086f1add0e1b3d170088e9abef9a6539cfbded'),
(108, 'a6886be4f98f8216bbba939ac3cc285654f8f6db'),
(109, '8d4fde5d49ae0f2e05b10946ba6b0d9dd2dfa343'),
(110, '638770e2bc8a2cf1cd7438c7acdbdab6a619b153'),
(111, 'd9dad7b43b4482953d020f5f2331be62882e47e8'),
(112, 'fd32567985fc30b33d798f3a040cbc514eb06b82'),
(113, '1187fc3e6e4b68325850089db3a88496d515fd23'),
(114, 'b12ab2686ca62b0d9f5d9c2de6efa3a93fe97db8'),
(115, '992be1c58589717cdc9d9f3e0fbf633f32fccf1b'),
(116, '9ccbbde964c33c3fb531f83a62003e3d0a644210'),
(117, '089e0fb1898aad1ac5cf8f37ee9130b9a8bdba2c'),
(118, '13cc4de15f3a2be2347a320deb216942839be845'),
(119, 'd1dd8470aad2bab21e37019f2632a30e98c75e73'),
(120, 'f246e606a2311be55f862d84263ea0c7ecf6c7e0'),
(121, '79d773ac629e3d1d8abddddbf4215d5b5b5280c8'),
(122, '89f5f94747d4334d53bc18c8d9cb3654a8908507'),
(123, 'efa3597aecd53cd06dd5768ee6107dbc3e3a7579'),
(124, '91426a764657a347906a9a22b01635c977f7f24d'),
(125, '42700e1820afa5906cba5176c0e6e60837181b71'),
(126, '473ff926ddef7c54820003582ae061f476dcea6d'),
(127, '955888ca1510da94272910441c0d0f28a02ce277'),
(128, 'a7906ca85c5886ab6c3a3e6c0c387fa1fddddbd9'),
(129, 'e045950645c6bbf0befb59faa2759e6b6058292d'),
(130, '1c842e6b4c903014732974774cf2bff7517d15ba');

-- --------------------------------------------------------

--
-- Структура таблицы `home_slide`
--

CREATE TABLE `home_slide` (
  `id` int(11) NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `ord` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `home_slide`
--

INSERT INTO `home_slide` (`id`, `image`, `url`, `ord`) VALUES
(14, '644740-1604927459410.jpeg', 'http://realmadrid.am', 0),
(15, '1100641-1604927608382.png', 'wwweeee', 1),
(16, '1274300-1605103191453.jpeg', 'http://realmadrid.am', 3),
(18, '397426-1605097716484.jpeg', 'http://realmadrid.am', 2),
(19, '1034311-1605367239152.jpeg', 'http://realmadrid.am', 4),
(20, '340123-1605872880646.jpeg', 'http://realmadrid.am', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `home_slide_language`
--

CREATE TABLE `home_slide_language` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `home_slide_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `button_text` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `home_slide_language`
--

INSERT INTO `home_slide_language` (`id`, `lang_id`, `home_slide_id`, `title`, `button_text`) VALUES
(3, 1, 14, 'English', 'asd'),
(4, 2, 14, 'Armenian', 'asd'),
(5, 1, 15, 'English Name', 'Button Tex en'),
(6, 2, 15, 'Armenian Name', 'Button Tex arm'),
(7, 1, 16, 'Eh Namessssssa', 'asd'),
(8, 2, 16, 'Arm Name33', 'asd'),
(11, 1, 18, 'qqq', 'asd'),
(12, 2, 18, 'www', 'asd'),
(13, 1, 19, 'wewqe qwe wqewq ', 'asd'),
(14, 2, 19, 'wq ewqeqw', 'asd'),
(15, 1, 20, 'slide title en', 'Button text en'),
(16, 2, 20, 'slide title am', 'Button text am');

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `lang_code` varchar(50) DEFAULT NULL,
  `lang_name` varchar(50) DEFAULT NULL,
  `lang_img` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`id`, `lang_code`, `lang_name`, `lang_img`) VALUES
(1, 'en', 'English', NULL),
(2, 'am', 'Armenian', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `loyality_status`
--

CREATE TABLE `loyality_status` (
  `id` int(11) NOT NULL,
  `from_money` int(11) NOT NULL,
  `ord` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `loyality_status`
--

INSERT INTO `loyality_status` (`id`, `from_money`, `ord`) VALUES
(1, 0, 0),
(2, 500000, 0),
(3, 1000000, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `loyality_status_language`
--

CREATE TABLE `loyality_status_language` (
  `id` int(11) NOT NULL,
  `loyality_status_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `loyality_status_language`
--

INSERT INTO `loyality_status_language` (`id`, `loyality_status_id`, `lang_id`, `name`) VALUES
(1, 1, 1, 'junior'),
(2, 1, 2, 'sksnak'),
(3, 2, 1, 'middle'),
(4, 2, 2, 'mijin'),
(5, 3, 1, 'senior'),
(6, 3, 2, 'senyor');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `insert_data` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `popular_products`
--

CREATE TABLE `popular_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `popular_products`
--

INSERT INTO `popular_products` (`id`, `product_id`) VALUES
(3, 19),
(5, 28),
(6, 26),
(7, 32),
(8, 34),
(9, 35),
(10, 38);

-- --------------------------------------------------------

--
-- Структура таблицы `privacy`
--

CREATE TABLE `privacy` (
  `id` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `privacy`
--

INSERT INTO `privacy` (`id`, `text`, `lang_id`) VALUES
(1, '<h2 style=\"margin-bottom: 10px; padding: 0px; font-weight: 400; font-family: DauphinPlain; font-size: 24px; line-height: 24px;\">sa dsa dasdasրտեղի՞ց վերցնել նման տեքստ</h2><h2 style=\"margin-bottom: 10px; padding: 0px; font-weight: 400; font-family: DauphinPlain; font-size: 24px; line-height: 24px;\"><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Կան Lorem Ipsum-ի շատ տարբերակներ, սակայն շատերը աղավաղվել են տարաբնույթ, երբեմն նույնիսկ լատիներենից շատ հեռու և անհավանական բառերի և հումորի ավելացման արդյունքում: Եթե ուզում եք օգտագործել Lorem Ipsum, ապա երևի չեք ցանկանա, որ այն պարունակի ինչ-որ թաքնված հումոր տեքստի միջնամասում: Ինտերնետում բոլոր Lorem Ipsum արտադրիչները հակված են կրկնել նույն տեքստը մինչև ցանկալի ծավալի լրացումը. այնինչ իսկականը այս արտադրիչն է: Այն օգտագործում է լատիներենի շուրջ 200 բառ` դրանք համադրելով նախադասության հնարավոր շարադասությունների հետ, որպեսզի արտադրի ճշմարիտ Lorem Ipsum: Արտադրված Lorem Ipsum-ը, արդյունքում չունի կրկնություններ, հումորային բովանդակություն կամ այլ անիրական բառեր:</p></h2>', 1),
(2, '<h2 style=\"margin-bottom: 10px; padding: 0px; font-weight: 400; font-family: DauphinPlain; font-size: 24px; line-height: 24px;\">Որտեղի՞ց Որտեղի՞ց&nbsp;Որտեղի՞ց վերցնել նման տեքստ</h2><h2 style=\"margin-bottom: 10px; padding: 0px; font-weight: 400; font-family: DauphinPlain; font-size: 24px; line-height: 24px;\"><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Կան Lorem Ipsum-ի շատ տարբերակներ, սակայն շատերը աղավաղվել են տարաբնույթ, երբեմն նույնիսկ լատիներենից շատ հեռու և անհավանական բառերի և հումորի ավելացման արդյունքում: Եթե ուզում եք օգտագործել Lorem Ipsum, ապա երևի չեք ցանկանա, որ այն պարունակի ինչ-որ թաքնված հումոր տեքստի միջնամասում: Ինտերնետում բոլոր Lorem Ipsum արտադրիչները հակված են կրկնել նույն տեքստը մինչև ցանկալի ծավալի լրացումը. այնինչ իսկականը այս արտադրիչն է: Այն օգտագործում է լատիներենի շուրջ 200 բառ` դրանք համադրելով նախադասության հնարավոր շարադասությունների հետ, որպեսզի արտադրի ճշմարիտ Lorem Ipsum: Արտադրված Lorem Ipsum-ը, արդյունքում չունի կրկնություններ, հումորային բովանդակություն կամ այլ անիրական բառեր:</p></h2>', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `price` decimal(12,2) DEFAULT NULL,
  `price_before_sale` decimal(12,2) DEFAULT NULL,
  `sale_percent` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `cover_image` varchar(200) DEFAULT NULL,
  `hover_image` varchar(200) DEFAULT NULL,
  `insert_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `brand_id`, `slug`, `price`, `price_before_sale`, `sale_percent`, `image`, `cover_image`, `hover_image`, `insert_date`) VALUES
(26, 15, 'erebuni-50-years-26', '850000.00', '1200000.00', 15, '273x760-4.png', '166589-1606297408560.jpeg', '49426-1606297408557.jpeg', '2020-11-25 13:43:28'),
(27, 15, 'ararat-apricot-27', '9850.00', NULL, NULL, '45632-1606297631201.jpeg', '370654-1606297631204.jpeg', '852358-1606297631203.jpeg', '2020-11-25 13:47:11'),
(28, 15, 'ararat-nairi-28', '38000.00', NULL, NULL, '650269-1606298056598.jpeg', '532493-1606298056603.jpeg', '643255-1606298056601.jpeg', '2020-11-25 13:54:16'),
(29, 18, 'absolute-lime-29', '6650.00', NULL, NULL, '782101-1606298932775.jpeg', '552038-1606298932793.jpeg', '864436-1606298932778.jpeg', '2020-11-25 14:08:52'),
(30, 18, 'absolut-grapefruit-30', '8000.00', NULL, NULL, '593814-1606299221014.jpeg', '1216121-1606299221020.png', '1184004-1606299221016.jpeg', '2020-11-25 14:13:41'),
(31, 19, 'wyborowa-wodka-31', '6100.00', NULL, NULL, '567803-1606299420551.jpeg', '837563-1606299420556.jpeg', '844285-1606299420552.jpeg', '2020-11-25 14:17:00'),
(32, 18, 'absolut-elyx-32', '16850.00', NULL, NULL, '875876-1606300356134.jpeg', '692387-1606300356137.jpeg', '936348-1606300356135.jpeg', '2020-11-25 14:32:36'),
(33, 15, 'absolut-mandrin-33', '8400.00', NULL, NULL, '1276331-1606300556145.jpeg', '498406-1606300556150.jpeg', '610983-1606300556146.jpeg', '2020-11-25 14:35:56'),
(34, 20, 'martell-vs-single-distillery-34', '19150.00', NULL, NULL, '138538-1606301735379.jpeg', '669551-1606301735383.jpeg', '330971-1606301735380.jpeg', '2020-11-25 14:55:35'),
(35, 20, 'martell-x-o-35', '115000.00', NULL, NULL, '452660-1606301954122.jpeg', '948300-1606301954128.jpeg', '874906-1606301954125.jpeg', '2020-11-25 14:59:14'),
(36, 18, 'absolut-vanilia-36', '6650.00', NULL, NULL, '386423-1606302213600.jpeg', '817223-1606302213603.jpeg', '660417-1606302213601.jpeg', '2020-11-25 15:03:33'),
(37, 18, 'absolut-kurant-37', '6700.00', NULL, NULL, '717873-1606302380144.jpeg', '901599-1606302380148.jpeg', '536930-1606302380147.jpeg', '2020-11-25 15:06:20'),
(38, 18, 'absolut-raspberri-38', '7000.00', NULL, NULL, '313029-1606302658459.jpeg', '800763-1606302658461.jpeg', '1037801-1606302658460.jpeg', '2020-11-25 15:10:58');

-- --------------------------------------------------------

--
-- Структура таблицы `product_language`
--

CREATE TABLE `product_language` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_language`
--

INSERT INTO `product_language` (`id`, `lang_id`, `product_id`, `name`, `description`) VALUES
(29, 1, 26, 'EREBUNI 50 Years Old', '<p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">The new brandy of Yerevan Brandy Company – Erebuni 50 complements the ultra-premium collection of ARARAT, launched back in 2015 with Erebuni 30. Taking its name from the ancient Erebuni Fortress, which became the heart of Armenia, Erebuni collection is the quintessence of ARARAT brandies.</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\"></p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">This most aged brandy in ARARAT range is a bright, rich and unique chapter in the 130-year-old history of Armenian brandy making.</p>'),
(30, 2, 26, 'EREBUNI 50 years', '<p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">Երևանի կոնյակի գործարանի նոր Էրեբունի 50 կոնյակը համալրում է ARARAT-ի ուլտրապրեմիալ հավաքածուն, որի սկիզբը 2015 թվականին դրվել է Էրեբունի 30 կոնյակի թողարկմամբ: Կոչված լինելով հնագույն ամրոցի պատվին, որը դարձել է Հայաստանի սիրտը՝ Էրեբունի հավաքածուն արտացոլում է ARARAT կոնյակների առանցքն ու էությունը:</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\"></p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">Այն ARARAT-ի ներկայիս շարքի ամենահնացված կոնյակն է՝ վառ, հագեցած, անկրկնելի էջ հայկական կոնյակագործության 130-ամյա պատմության մեջ:</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">&nbsp;</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">Էրեբունի 50 արտահայտիչ կուպաժն ավելի քան 50 հազվագյուտ սպիրտների ներդաշնակ համադրություն է, որոնցից ամենահինը՝ Երևանի կոնյակի գործարանի գանձարանի հազվագյուտ նմուշը, 104 տարեկան է: Արևով օծված Էրեբունի 50-ը ARARAT շարքի գագաթնակետն է, քանզի այս կուպաժի սպիտներից յուրաքանչյուրն արտադրված է արևով ողողված և հաճախ «կյանքի օրրան» անվանվող Արարատյան դաշտավայրում աճող էնդեմիկ հայկական խաղողից:</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">&nbsp;</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">Էրեբունի 50-ը ներկայացված է բացառիկ դեկանտերով, որը ստեղծվել է ֆրանսիացի դիզայներ Ֆիլիպ Սեի կողմից:</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">&nbsp;</p><p><br></p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify; color: rgb(148, 148, 148) !important;\">«Շքեղ: Բացառիկ: Կատարյալ: Էրեբունի 50-ը անվերջ բացահայտումների սկիզբ է: Այս կոնյակի հարուստ համը նրբագեղ արաբեսկ է, որի ողջ գեղեցկությունը կարող են գնահատել վայելքի իրական գիտակները: Հազվագյուտ սպիրտների ազնվական համադրությունը դարձնում է այս կուպաժն հիրավի բացառիկ և պարգևում է հագեցած սաթե գույն կարմիր փայտի և ոսկու շողքով: Այն ունի անկրկնելի համահոտային փունջ, որտեղ նկատելի են ծիրանաչրի, սալորի և մեխակի երագները: Մետաքսյա պարուրող համը երանգավորված է բոված պնդուկի և շոկոլադի ալիքներով: Երկար հետհամի ժամանակ նկատելի է ժամանակի դրած կնիքը՝ հին կաղնու տտիպությունը», - պատմում է Երևանի կոնյակի գործարանի արտադրության տնօրեն Համլետ Անտոնյանը՝ Էրեբունի 50-ի հեղինակը:</p>'),
(31, 1, 27, 'ARARAT Apricot', '<p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Character</span><br style=\"box-sizing: inherit; background-color: unset !important;\">ARARAT Apricot, based on 6-year-old ARARAT brandy and apricot flavor, has a rich and intense aroma with a soft and fresh palate. The undertones of ripe fruits tinged with notes of licorice-vanilla linger in the smooth flavor of the drink and carry-on with a sweet finish of fresh apricots.</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Moment</span><br style=\"box-sizing: inherit; background-color: unset !important;\">Enjoy ARARAT Apricot cooled or with an ice cube. Accompany the tasting with marzipan or almond-based refined desserts to fully reveal the bright taste of the product. Or savour the vibrant and fresh ARARAT Apricot cocktails to create new unique #ARARATMOMENTS for you and your beloved ones.</p><p><br></p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify; color: rgb(148, 148, 148) !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Legend</span><br style=\"box-sizing: inherit; background-color: unset !important;\">The union of two traditional symbols of Armenia, legendary brandy ARARAT and apricot, gives life to the innovative product ARARAT Apricot: embodiment of the generous Armenian land. The masters of Yerevan Brandy Company introduce the new bright taste of the legendary Armenian brandy ARARAT by keeping the heritage and savoir-faire of brandy production in ARARAT Apricot</p>'),
(32, 2, 27, 'ARARAT Apricot', '<p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Բնութագիր</span><br style=\"box-sizing: inherit; background-color: unset !important;\">ARARAT Apricot խմիչքը, 6 տարի հնացման ARARAT կոնյակի հիմքով և ծիրանի համով, օժտված է նուրբ և կատարյալ ներդաշնակ բույրով: Հասուն մրգերի նրբերանգները, ընդգծված վանիլի և մատուտակի ելևէջներով, շարունակվում են թարմ ծիրանի հաճելի նրբահամով և քաղցր հետհամով:</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Պահ</span><br style=\"box-sizing: inherit; background-color: unset !important;\">Վայելեք ARARAT Apricot խմիչքը սառեցված բաժակներով կամ ավելացնելով սառույցի կտոր՝ զուգակցելով մարցիպանի կամ նուշի հիմքով պատրաստված նրբահամ աղանդերով՝ լիովին բացահայտելու խմիչքի փափուկ և մեղմ համը: ARARAT Apricot-ի հիմքով պատրաստվող թարմ և վառ կոկտեյլները կստեղծեն նոր հիշարժան պահեր #ARARATMOMENTS ձեր և ձեր մտերիմների համար:</p><p><br></p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify; color: rgb(148, 148, 148) !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Լեգենդ</span><br style=\"box-sizing: inherit; background-color: unset !important;\">այաստանի երկու իրական խորհրդանիշների՝ լեգենդար ARARAT կոնյակի և ծիրանի միաձուլումից ծնունդ է առնում նորարական ARARAT Apricot-ը՝ հայկական հողի առատաձեռնության մարմնավորումը: Պահպանելով Երևանի կոնյակի գործարանի վարպետների փորձը և հայկական կոնյակի ստեղծման դարավոր ավանդույթները՝ ARARAT Apricot-ը ներկայացնում է լեգենդար ARARAT հայկական կոնյակի նոր, վառ համը</p>'),
(33, 1, 28, 'ARARAT Nairi', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Character:</span><span style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; text-align: justify;\">&nbsp;the twenty year old ARARAT Nairi is the oldest and deepest in the ARARAT range. Its noble, multi-faceted bouquet is perfectly balanced with spicy hints of pine nuts, the sweetness of thick fresh honey and light peppercorn.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Moment:</span><span style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; text-align: justify;\">&nbsp;the celebration of victory, achieving cherished goals, these are the moments ARARAT Nairi brandy was created for. This sophisticated blend perfectly emphasizes the significance of the achievement and is a perfect gift for a true victor.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Legend:</span><span style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; text-align: justify;\">&nbsp;ARARAT Nairi brandy is named after the powerful people of Nairi, who inhabited the Kingdom of Urartu, an area which today is in Armenia. This superb blend is considered to be the pinnacle of Markar Sedrakyan’s craftmanship.</span><br></p>'),
(34, 2, 28, 'ARARAT Nairi', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\"><span style=\"box-sizing: inherit; color: rgb(148, 148, 148) !important; font-weight: 300 !important; background-color: unset !important;\">Բնութագիր</span></span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">Քսանամյա ARARAT «Նաիրի» կոնյակը տեսականու ամենահնեցվածն է: Նրա ազնվագույն, բարդ ու բազմաշերտ փնջում կատարելապես հավասարակշռված են մայրընկույզի սուր երանգները, մեղրի քաղցրությունն ու թեթև նրբահամ տաքդեղի հատիկը:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\"><span style=\"box-sizing: inherit; color: rgb(148, 148, 148) !important; font-weight: 300 !important; background-color: unset !important;\">Պահ</span></span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">Հաղթանակների, նշանակալի ձեռքբերումների ու հասած նպատակների տոնումը կատարյալ առիթ է ARARAT «Նաիրի» կոնյակի համար: Այս նրբաճաշակ խմիչքը հրաշալիորեն ընդգծում է պահի հանդիսավորությունն ու գեղեցիկ նվեր կդառնա` փառաբանելու անձնական հաջողությունները և ձեռքբերումները:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\"><span style=\"box-sizing: inherit; color: rgb(148, 148, 148) !important; font-weight: 300 !important; background-color: unset !important;\">Լեգենդ</span></span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">ARARAT «Նաիրի» կոնյակն իր անվանումը ստացել է Ուրարտու թագավորությունում բնակեցված Նաիրի հզոր ժողովրդի պատվին, որի տարածքում է գտնվում ժամանակակից Հայաստանը: Այս սքանչելի կոնյակը իրավամբ համարվում է Մարգար Սեդրակյանի վարպետության գագաթնակետը:</span><br></p>'),
(35, 1, 29, 'ABSOLUTE Lime', NULL),
(36, 2, 29, 'ABSOLUTE Lime', '<p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">Absolut Lime is the latest addition to the core range of flavors. An instant classic, this citrus sibling arrives almost 30 years after the launch of Absolut Citron. With its fresh, balanced taste of natural Lime flavor and iconic frosted bottle, Absolut Lime perfectly complements the range as a key ingredient in many of today’s classic drinks with a refreshing twist.</p><p><br></p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify; color: rgb(148, 148, 148) !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">What does true taste of lime taste like?</span><br style=\"box-sizing: inherit; background-color: unset !important;\">Absolut Lime is made exclusively from natural ingredients, and unlike some other flavored vodkas, it doesn’t contain any added sugar. To put it short and simple: Absolut Lime is smooth, rich and very fresh with a distinct note of freshly pressed lime and a slightly sweet and fruity finish.</p>'),
(37, 1, 30, 'ABSOLUT Grapefruit', NULL),
(38, 2, 30, 'ABSOLUT Grapefruit', NULL),
(39, 1, 31, 'WYBOROWA WODKA', '<p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">The purity of the rye grain combined with our traditional distillation technique perfected for close to a century, along with pristine water from our own well, is what gives Wyborowa its exceptional smoothness.</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">Wyborowa has grainy, fruity and slightly flowery notes, along with a long, exceptionally smooth finish. In creating vodka Wyborowa collaborates with local agricultural distilleries, where passionate people use time-honored methods to create raw spirit from pure rye grain. The grain is steamed, fermented and distilled and the spirit goes through what’s known as multi-column rectification – a process that cleans impurities and adds the final character to the product.<br style=\"box-sizing: inherit; background-color: unset !important;\"><br style=\"box-sizing: inherit; background-color: unset !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">CHILLED DRINKS WARM HEARTS</span><br style=\"box-sizing: inherit; background-color: unset !important;\">Did you know that Wyborowa is exceptionally versatile?<br style=\"box-sizing: inherit; background-color: unset !important;\">Its smooth flavor makes it the perfect match to pretty much anything – you don’t need many ingredients to create a great shot or cocktail.<br style=\"box-sizing: inherit; background-color: unset !important;\"><br style=\"box-sizing: inherit; display: inline; background-color: unset !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">START EVERY OCCASION WITH A TOAST</span></p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">1. Chill the bottle before you do anything else<br style=\"box-sizing: inherit; display: inline; background-color: unset !important;\">2. Prepare shot glasses for you and your friends<br style=\"box-sizing: inherit; background-color: unset !important;\">3. ​​All together, raise your glasses and say... NAZ-DROH-VEE-YA !!!</p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\"><br style=\"box-sizing: inherit; background-color: unset !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">THE NEAT SHOT</span><br style=\"box-sizing: inherit; background-color: unset !important;\">The Polish Way</p><p><br></p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify; color: rgb(148, 148, 148) !important;\"><em style=\"box-sizing: inherit; background-color: unset !important;\">How to serve:</em><br style=\"box-sizing: inherit; background-color: unset !important;\">Wyborowa on its own? Perfection.<br style=\"box-sizing: inherit; background-color: unset !important;\">Simply chill your Wyborowa in the freezer, and then pour 4cl into a chilled shot glass. Because simple is sophisticated.&nbsp;<br style=\"box-sizing: inherit; background-color: unset !important;\"><em style=\"box-sizing: inherit; display: inline; background-color: unset !important;\">Ingredients:</em><br style=\"box-sizing: inherit; background-color: unset !important;\">1 shot of Wyborowa – that’s it!</p>'),
(40, 2, 31, 'WYBOROWA WODKA', NULL),
(41, 1, 32, 'ABSOLUT Elyx', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Nose:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;pure, rich, notes of fresh bread, grains and aniseed. Delicate hints of spice.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Taste:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;well-balanced in the mouth, silky texture, notes of aniseed, hazelnut and grains as well as traces of delicate spiciness.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Finish:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;the delicate hints of spice and fresh hazelnuts persist. Pure, warm and sweet.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Serving suggestion:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;Absolut Elyx can be drunk straight, iced, or in cocktails where it reveals all its organoleptic qualities.</span><br></p>'),
(42, 2, 32, 'ABSOLUT Elyx', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Բույրը`</span><span style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; text-align: justify;\">&nbsp;մաքուր, հարուստ, թարմ հացի, հացահատիկի և անիսոնի երանգներ: Նուրբ համեմունքային երանգներ:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Համը`</span><span style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; text-align: justify;\">&nbsp;համադրված, մետաքսյա փափկություն, անիսոնի, պնդուկի և հացահատիկի նրբերանգներ, ինչպես նաև նուրբ համեմունքային երանգներ:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Հետհամային զգացողություն`</span><span style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; text-align: justify;\">համեմունքների և թարմ պնդուկի նրբահամ: Մաքուր, ջերմացնող և քաղցր:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Մատուցման եղանակը`</span><span style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; text-align: justify;\">&nbsp;ABSOLUT Elyx օղին կարող է մատուցվել սառույցով, առանց սառույցի կամ կոկտեյլների մեջ, որտեղ այն բացահայտում է իր բոլոր զգայորոշման հատկությունները:</span><br></p>'),
(43, 1, 33, ' ABSOLUT Mandrin', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Color:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;crystal clear.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Aroma:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;a complex, fruity character of mandarin and orange mixed with a note of orange peel.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Taste:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;smooth and mellow with a rich fruitiness.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Finish:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;long, fruity and tasty.</span><br></p>'),
(44, 2, 33, ' ABSOLUT Mandrin', NULL),
(45, 1, 34, 'MARTELL VS Single Distillery', '<p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; color: rgb(148, 148, 148) !important;\">Martell VS Single Distillery combines spirits from a single distillation source in France’s Cognac region for a richer and more intense expression of the Martell distillation style. Martell is the only great cognac house to double distill exclusively clear wines, from which all sediments have been removed, in order to preserve the authentic fruity aromas of the grapes and reveal their incredible subtlety. These are hallmarks of the Martell style.</p><p><br></p><p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify; color: rgb(148, 148, 148) !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Colour:</span>&nbsp;rich, clear gold.<br style=\"box-sizing: inherit; background-color: unset !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Aroma:</span>&nbsp;intense aromas of plum, apricot and candied lemon.<br style=\"box-sizing: inherit; background-color: unset !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Taste:</span>&nbsp;the luscious fruity notes associated with Martell are taken to new heights with this supremely smooth blend.<br style=\"box-sizing: inherit; display: inline; background-color: unset !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Serving tips:</span>&nbsp;a great cognac to enjoy in long drinks or cocktails.</p>'),
(46, 2, 34, 'MARTELL VS Single Distillery', NULL),
(47, 1, 35, 'MARTELL X. O.', '<p style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify; color: rgb(148, 148, 148) !important;\"><span style=\"box-sizing: inherit; color: rgb(128, 128, 128); background-color: unset !important;\"><em style=\"box-sizing: inherit; color: rgb(148, 148, 148) !important; background-color: unset !important;\">With its distinctive, arch-shaped bottle, Martell XO is a powerful symbol of inspiration - a testimony to Jean Martell’s visionary spirit. Combining the elegance of the Borderies with the power of the Grande Champagne terroirs, Martell is an outstanding XO bearing the hallmark of the Martell style.</em><br style=\"box-sizing: inherit; color: rgb(148, 148, 148) !important; background-color: unset !important;\"><br style=\"box-sizing: inherit; color: rgb(148, 148, 148) !important; background-color: unset !important;\">A spicy, rich crescendo starting with the mellow elegance of the Borderies, followed by the intensity and finesse typical of Grande Champagne eaux-de-vie.<br style=\"box-sizing: inherit; color: rgb(148, 148, 148) !important; background-color: unset !important;\"><br style=\"box-sizing: inherit; color: rgb(148, 148, 148) !important; background-color: unset !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Colour:</span>&nbsp;golden amber with dark copper and mahogany highlights.<br style=\"box-sizing: inherit; color: rgb(148, 148, 148) !important; background-color: unset !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Nose:</span>&nbsp;ground spice (black pepper, coriander) and red berries. Rich fruit flavours: fig chutney and compote, almonds and walnuts. Beeswax and sandalwood.<br style=\"box-sizing: inherit; color: rgb(148, 148, 148) !important; background-color: unset !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Palate:</span>&nbsp;rounded and fruity on the palate (notes of fig and walnut) followed by the characteristic power and finesse of eaux-de-vie from Grande Champagne. A long and silken finish.<br style=\"box-sizing: inherit; display: inline; color: rgb(148, 148, 148) !important; background-color: unset !important;\"><span style=\"box-sizing: inherit; font-weight: bolder !important; color: rgb(130, 130, 130) !important; background-color: unset !important;\">Serving suggestion:</span>&nbsp;ideal for special occasions. Best enjoyed neat or with a splash of water.</span></p><p><a class=\"less_btn\" style=\"box-sizing: inherit; background-color: rgb(255, 255, 255); color: inherit; text-decoration: none; touch-action: manipulation; cursor: pointer; outline-width: 0px; font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify; font-weight: 300 !important;\">&nbsp;</a><br></p>'),
(48, 2, 35, 'MARTELL X. O.', '<p><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">Իր աչքի ընկնող, կամարաձև շշով Martell XO-ն հանդիսանում է Ժան Մարտելլի ստեղծագործ ոգու ոգեշնչման հզոր խորհրդանիշը: Համադրելով Բորդերի շրջանի էլեգանտությունը Գրան Շամպան շրջանի ուժի հետ՝ Martell-ը հայտնի XO է, որը Մարտելլի ոճի այցեքարտն է:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">Համեմունքային, հարուստ կրեշչենդո, որը սկսվում է Բորդերի շրջանի նուրբ էլեգանտությամբ, որին հաջորդում է Գրան Շամպան շրջանի կոնյակներին բնորոշ ինտենսիվությունը և նրբահամությունը:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Գույնը`&nbsp;</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">ոսկեգույն սաթի, մուգ պղնձագույն և կարմրափայտ ծառի երանգներ:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Բույրը</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">` ինտենսիվ և համահունչ, նարնջի և բուրումնավետ խոտաբույսերի նրբերանգ:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Համը</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">` աղացած համեմունքների /սև պղպեղի, գինձի/ և կարմիր հատապտուղների համ: Հարուստ մրգային երանգներ՝ թզի չատնի և մրգաջուր, նուշ և ընկույզ: Մեղրամոմ և սանդալի փայտ:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Համը</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">` ավարտուն և մրգային /թզի և ընկույզի երանգներ/, ինչին հաջորդում է Գրն Շամպան շրջանի կոնյակների արտասովոր ուժը և նրբահամությունը երկարատև և թավշյա հետհամային զգացողությամբ:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Մատուցման եղանակը</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">`հիանալի է հատուկ առիթների ժամանակ: Ըմպել առանց խառնուրդի կամ շատ քիչ քանակությամբ ջրով:</span><br></p>'),
(49, 1, 36, 'ABSOLUT Vanilia', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Color:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;crystal clear.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Aroma:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;rich, robust and complex. It has a distinct character of natural vanilla, notes of butterscotch and hints of chocolate.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Taste:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;smooth and mellow with a rich character of vanilla and a slight note of natural sweetness.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Finish:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;long, rich and smooth.</span><br></p>'),
(50, 2, 36, 'ABSOLUT Vanilia', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Գույնը`&nbsp;</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">բյուրեղյա մաքուր:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Բույրը</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">` հարուստ, ուժեղ և բարդ: Բնական վանիլինի, իրիսի և շոկոլադի նրբերանգներ:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Համը</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">` փափուկ և հյութալի, վանիլինի և բնական քաղցրության նուրբ երանգներ:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">​Հետհամային զգացողություն</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">`երկարատև, հարուստ և փափուկ:</span><br></p>'),
(51, 1, 37, 'ABSOLUT Kurant', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Color:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;crystal clear.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Aroma:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;berry and fruity with notes of fresh natural black currant leaves.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Taste:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;rich, smooth and berry notes with a hint of tartness.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Finish:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;long and smooth.</span><br></p>'),
(52, 2, 37, 'ABSOLUT Kurant', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Գույնը`&nbsp;</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">բյուրեղյա մաքուր:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Բույրը</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">` հատապտղային, մրգային, սև հաղարջի տերևների թարմության երանգներ:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Համը</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">` հարուստ, փափուկ, հատապտղային, փոքր-ինչ թթվային:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Հետհամային զգացողություն</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">`երկարատև, փափուկ:</span><br></p>'),
(53, 1, 38, 'ABSOLUT Raspberri', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Color:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;crystal clear.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Aroma:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;rich and intense, revealing the fresh and fruity character of ripened raspberries.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Taste:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;smooth, tasty and mellow with notes of sun ripened raspberries.</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Finish:</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">&nbsp;long and berry.</span><br></p>'),
(54, 2, 38, 'ABSOLUT Raspberri', '<p><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Գույնը`&nbsp;</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">բյուրեղյա մաքուր:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Բույրը</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">` հարուստ, ինտենսիվ, հասած ազնվամորու թարմ և մրգային երանգներ:|</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Համը</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">` փափուկ, համեղ և հյութալի, արևի ջերմությամբ հասած ազնվամորու երանգներով:</span><br style=\"box-sizing: inherit; color: rgb(148, 148, 148); font-size: 13px; font-family: Roboto, sans-serif; display: inline; text-align: justify;\"><span style=\"box-sizing: inherit; font-size: 13px; font-family: Roboto, sans-serif; text-align: justify; font-weight: bolder !important; color: rgb(130, 130, 130) !important;\">Հետհամային զգացողություն</span><span style=\"color: rgb(148, 148, 148); font-family: Roboto, sans-serif; font-size: 13px; text-align: justify;\">`երկարատև, հատապտղային:</span><br></p>');

-- --------------------------------------------------------

--
-- Структура таблицы `terms`
--

CREATE TABLE `terms` (
  `id` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `terms`
--

INSERT INTO `terms` (`id`, `text`, `lang_id`) VALUES
(1, '<p><strong style=\"margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><br></p>', 1),
(2, '<h2 style=\"margin-bottom: 10px; padding: 0px; font-weight: 400; font-family: DauphinPlain; font-size: 24px; line-height: 24px;\">Ինչո՞ւ ենք այն օգտագործում</h2><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Հայտնի է, որ ընթերցողը, կարդալով հասկանալի տեքստ, չի կարողանա կենտրոնանալ տեքստի ձևավորման վրա: Lorem Ipsum օգտագործելը բացատրվում է նրանով, որ այն բաշխում է բառերը քիչ թե շատ իրականի նման, ի տարբերություն «Բովանդակություն, բովանդակություն» սովորական կրկննության, ինչը ընթերցողի համար հասկանալի է: Շատ համակարգչային տպագրական ծրագրեր և ինտերնետային էջերի խմբագրիչներ այն օգտագործում են որպես իրենց ստանդարտ տեքստային մոդել, և հետևապես, ինտերնետում Lorem Ipsum-ի որոնման արդյունքում կարելի է հայտնաբերել էջեր, որոնք դեռ նոր են կերտվում: Ժամանակի ընթացքում ձևավորվել են Lorem Ipsum-ի տարբեր վերսիաներ` երբեմն ներառելով պատահական տեքստեր, երբեմն էլ հատուկ իմաստ (հումոր և նմանատիպ բովանդակություն):</p>', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `translates`
--

CREATE TABLE `translates` (
  `id` int(11) NOT NULL,
  `key_name` varchar(100) DEFAULT NULL,
  `ord` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `translates`
--

INSERT INTO `translates` (`id`, `key_name`, `ord`) VALUES
(1, 'log_in', 0),
(2, 'sign_up', 0),
(3, 'search', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `translates_language`
--

CREATE TABLE `translates_language` (
  `id` int(11) NOT NULL,
  `translates_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `translates_language`
--

INSERT INTO `translates_language` (`id`, `translates_id`, `lang_id`, `name`) VALUES
(1, 1, 1, 'Login'),
(2, 1, 2, 'Մուտք'),
(3, 2, 1, 'Sign Up'),
(4, 2, 2, 'Գրանցում'),
(5, 3, 1, 'Search'),
(6, 3, 2, 'Փնտրել');

-- --------------------------------------------------------

--
-- Структура таблицы `users_db`
--

CREATE TABLE `users_db` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `phone` int(20) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 - normal, 1 - blocked',
  `password` varchar(100) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `inset_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_db`
--

INSERT INTO `users_db` (`id`, `name`, `last_name`, `email`, `birth_date`, `phone`, `status`, `password`, `token`, `last_login_date`, `inset_date`) VALUES
(43, 'Grigor', 'Sadoyan', 'sadoyangrigor@gmail.com', '1993-11-08', 374943837, '0', '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'be71899de596b55e44a1c9ba24f90860d61fbc94', NULL, '2020-11-18 15:53:04');

-- --------------------------------------------------------

--
-- Структура таблицы `user_loyality`
--

CREATE TABLE `user_loyality` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `money` int(11) NOT NULL,
  `loyality_status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_loyality`
--

INSERT INTO `user_loyality` (`id`, `user_id`, `money`, `loyality_status_id`) VALUES
(3, 43, 0, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `additional_filters`
--
ALTER TABLE `additional_filters`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `additional_filters_language`
--
ALTER TABLE `additional_filters_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blog_language`
--
ALTER TABLE `blog_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_id` (`blog_id`);

--
-- Индексы таблицы `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_id` (`cat_id`);

--
-- Индексы таблицы `brand_language`
--
ALTER TABLE `brand_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brand_id` (`brand_id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories_language`
--
ALTER TABLE `categories_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_id` (`cat_id`);

--
-- Индексы таблицы `cookie`
--
ALTER TABLE `cookie`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `faq_language`
--
ALTER TABLE `faq_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faq_id` (`faq_id`);

--
-- Индексы таблицы `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `filter`
--
ALTER TABLE `filter`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `filter_language`
--
ALTER TABLE `filter_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `filter_id` (`filter_id`);

--
-- Индексы таблицы `filter_product`
--
ALTER TABLE `filter_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `filter_prop`
--
ALTER TABLE `filter_prop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `filter_id` (`filter_id`);

--
-- Индексы таблицы `filter_prop_language`
--
ALTER TABLE `filter_prop_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `filter_prop_id` (`filter_prop_id`);

--
-- Индексы таблицы `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `home_slide`
--
ALTER TABLE `home_slide`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `home_slide_language`
--
ALTER TABLE `home_slide_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `home_slide_id` (`home_slide_id`);

--
-- Индексы таблицы `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `loyality_status`
--
ALTER TABLE `loyality_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `loyality_status_language`
--
ALTER TABLE `loyality_status_language`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `popular_products`
--
ALTER TABLE `popular_products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `privacy`
--
ALTER TABLE `privacy`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_language`
--
ALTER TABLE `product_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `translates`
--
ALTER TABLE `translates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `translates_language`
--
ALTER TABLE `translates_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `translates_id` (`translates_id`);

--
-- Индексы таблицы `users_db`
--
ALTER TABLE `users_db`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_loyality`
--
ALTER TABLE `user_loyality`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `additional_filters`
--
ALTER TABLE `additional_filters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `additional_filters_language`
--
ALTER TABLE `additional_filters_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT для таблицы `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `blog_language`
--
ALTER TABLE `blog_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `brand_language`
--
ALTER TABLE `brand_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `categories_language`
--
ALTER TABLE `categories_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT для таблицы `cookie`
--
ALTER TABLE `cookie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `faq_language`
--
ALTER TABLE `faq_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `filter`
--
ALTER TABLE `filter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `filter_language`
--
ALTER TABLE `filter_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `filter_product`
--
ALTER TABLE `filter_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT для таблицы `filter_prop`
--
ALTER TABLE `filter_prop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `filter_prop_language`
--
ALTER TABLE `filter_prop_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT для таблицы `guest`
--
ALTER TABLE `guest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT для таблицы `home_slide`
--
ALTER TABLE `home_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `home_slide_language`
--
ALTER TABLE `home_slide_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `loyality_status`
--
ALTER TABLE `loyality_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `loyality_status_language`
--
ALTER TABLE `loyality_status_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `popular_products`
--
ALTER TABLE `popular_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `privacy`
--
ALTER TABLE `privacy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `product_language`
--
ALTER TABLE `product_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT для таблицы `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `translates`
--
ALTER TABLE `translates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `translates_language`
--
ALTER TABLE `translates_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `users_db`
--
ALTER TABLE `users_db`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT для таблицы `user_loyality`
--
ALTER TABLE `user_loyality`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `additional_filters_language`
--
ALTER TABLE `additional_filters_language`
  ADD CONSTRAINT `additional_filters_language_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `blog_language`
--
ALTER TABLE `blog_language`
  ADD CONSTRAINT `blog_language_ibfk_1` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `brand`
--
ALTER TABLE `brand`
  ADD CONSTRAINT `brand_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `brand_language`
--
ALTER TABLE `brand_language`
  ADD CONSTRAINT `brand_language_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `categories_language`
--
ALTER TABLE `categories_language`
  ADD CONSTRAINT `categories_language_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `faq_language`
--
ALTER TABLE `faq_language`
  ADD CONSTRAINT `faq_language_ibfk_1` FOREIGN KEY (`faq_id`) REFERENCES `faq` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `filter_language`
--
ALTER TABLE `filter_language`
  ADD CONSTRAINT `filter_language_ibfk_1` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `filter_product`
--
ALTER TABLE `filter_product`
  ADD CONSTRAINT `filter_product_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `filter_prop`
--
ALTER TABLE `filter_prop`
  ADD CONSTRAINT `filter_prop_ibfk_1` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `filter_prop_language`
--
ALTER TABLE `filter_prop_language`
  ADD CONSTRAINT `filter_prop_language_ibfk_1` FOREIGN KEY (`filter_prop_id`) REFERENCES `filter_prop` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `home_slide_language`
--
ALTER TABLE `home_slide_language`
  ADD CONSTRAINT `home_slide_language_ibfk_1` FOREIGN KEY (`home_slide_id`) REFERENCES `home_slide` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_language`
--
ALTER TABLE `product_language`
  ADD CONSTRAINT `product_language_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `translates_language`
--
ALTER TABLE `translates_language`
  ADD CONSTRAINT `translates_language_ibfk_1` FOREIGN KEY (`translates_id`) REFERENCES `translates` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
