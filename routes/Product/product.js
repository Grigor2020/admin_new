var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var brandsQuery = require('../../model/dbQuerys/Brands/brands');
var productQuery = require('../../model/dbQuerys/Product/product');
var filtersQuery = require('../../model/dbQuerys/Filters/filters');
var litrageQuery = require('../../model/dbQuerys/Litrage/litrage');
var multer = require('multer');
var staticMethods = require('../../model/staticMethods')
var statics = require('../../static')
var fs = require('fs')
var upload = multer({
    fileSize:1024 * 1024,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.mimetype == "video/mp4") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }//size of u file
});

upload.storage = multer.diskStorage({
    destination: '../'+statics.API_FOLDER_NAME+'/public/images/product',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});
var cpUpload = upload.fields([
    { name: 'image', maxCount: 1 },
    { name: 'cover_image', maxCount: 1 },
    { name: 'hover_image', maxCount: 1 },
    ]);

router.get('/', function(req, res, next) {
    var page = req.query.page;
    Promise.all([
        // brandsQuery.getAllBrandNames(),
        productQuery.getProductCount(),
        productQuery.getAllProducts(page)
    ])
        .then(function ([productCount,products]) {
            var pagesCount = Math.ceil(parseInt(productCount) / staticMethods.pageProductCount);
            console.log('pagesCount............................',pagesCount)
            console.log('thisPage............................',page)
            console.log('products............................',products)
            for(let i = 0; i < products.length; i++){
                products[i].img = statics.API_URL+'/images/product/'+products[i].image;
                delete products[i].image;
            }
            // console.log('products',products)
            res.render('Product/product', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                // brands : brands,
                data : products,
                pageCount :  pagesCount,
                thisPage : parseInt(page)
            })
        })
        .catch(function (error) {
            console.log('..............',error)
        })
});

router.get('/add', function(req, res, next) {
    Promise.all([
        // litrageQuery.getAllLitrages(),
        brandsQuery.getAllBrandNames(),
        filtersQuery.getAllFiltersByLangId(2),
        filtersQuery.getAllProperties(2)
    ])
        .then(function ([
                            // litrage,
                            brands,
                            filters,
                            filterProps
                        ]) {
            var filter = sortingFilters(filters,filterProps);
            // console.log('brands',brands)
            res.render('Product/addProduct', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                brands : brands,
                // litrages : litrages,
                litrages : staticMethods.litrages(),
                filters : filter,
            })
        })
        .catch(function (error) {
            console.log('..............',error)
        })
});

router.post('/add',cpUpload, function(req, res, next) {
    // console.log('..................',typeof req.body.image_top !== "undefined" ? req.body.image_top : 0);
    // console.log('req.body',req.body.image_top);
    // console.log('typeof ssreq.body',req.body.ap_name.length)
    let dinamicData = JSON.parse(req.body.res_data)
    let staticFiltersData = JSON.parse(req.body.static_filters)
    // console.log('staticFiltersData',staticFiltersData)


    query.insert2vPromise('product',
        {
            image : req.files.image[0].filename,
            cover_image : req.files.cover_image[0].filename,
            hover_image : req.files.hover_image[0].filename,
            price : req.body.price,
            image_top : typeof req.body.image_top !== "undefined" ? req.body.image_top : '0',
            price_before_sale : req.body.price_before_sale == '' ? null : parseFloat(req.body.price_before_sale),
            sale_percent : req.body.sale_percent == '' ? null : parseFloat(req.body.sale_percent),
            brand_id : req.body.brand_id,
            litrage_id : req.body.litrage_id,
        }
        )
        .then(function (data) {
            let insertId = data.insertId;
            let fields = ['lang_id','product_id','name','description','seo_title','seo_desc']
            let post = [];
            for(let i = 0; i < dinamicData.length;i++){
                post.push([
                    ''+dinamicData[i].lang_id,
                    ''+insertId,
                    dinamicData[i].name == '' ? null : dinamicData[i].name,
                    dinamicData[i].description == '' ? null : dinamicData[i].description,
                    dinamicData[i].seo_title == '' ? null : dinamicData[i].seo_title,
                    dinamicData[i].seo_desc == '' ? null : dinamicData[i].seo_desc
                ])
            }
            let newSlug = staticMethods.convertStringToSlug(dinamicData[1].name);

            let additionalFiltersPost = [];
            let filtersPost = [];
            // console.log('req.body.ap_name',req.body.ap_name)

            for (let i = 0; i < req.body.ap_name.length; i++){
                var unique;
                let lang_id;
                if (i % 2 == 0) {
                    lang_id = 1;
                    unique = staticMethods.generateRandomIntegers(3)
                }else{
                    unique = additionalFiltersPost[i-1][4]
                    lang_id = 2;
                }
                additionalFiltersPost.push([insertId,lang_id,req.body.ap_name[i],req.body.ap_val[i],unique])
            }
            // console.log('additionalFiltersPost',additionalFiltersPost)
            for(let i = 0; i < staticFiltersData.length; i++){
                filtersPost.push([staticFiltersData[i],insertId])
            }
            Promise.all([
                query.insertLoopPromise('product_language',fields,post),
                query.update2vPromise('product',{where:{id:insertId},values:{slug : newSlug+'-'+insertId}}),
                query.insertLoopPromise('additional_filters_language',['product_id','lang_id','name','val','group_row_id'],additionalFiltersPost),
                query.insertLoopPromise('filter_product',['filter_prop_id','product_id'],filtersPost)
            ])

                .then(function ([insertLoopRes,updateRes]) {
                    res.redirect('/product?page=1');
                })
                .catch(function (error) {
                    console.log('error 2',error)
                })
        })
        .catch(function (error) {
            console.log('error 1',error)
        })
});

router.get('/update/:id', function(req, res, next) {
    Promise.all([
        // litrageQuery.getAllLitrages(),
        brandsQuery.getAllBrandNames(),
        productQuery.getProductById(req.params.id),
        filtersQuery.getAllFiltersByLangId(2),
        filtersQuery.getAllProperties(2),
        filtersQuery.getAllAdditionalProperties(req.params.id),
    ])
        .then(function (
            [
                // litrages,
                brands,
                data,
                filters,
                filterProps,
                additionalFilterProps
            ]) {
            var filter = sortingFilters(filters,filterProps);
            for(let i = 0; i < data.length; i++){
                let productStaticFilters = data[i].filter_prop_id.split('||')
                let langIds = data[i].lang_id.split('||')
                let name = data[i].name.split('||')
                let description = data[i].description.split('||')
                let seo_title = data[i].seo_title === null ? ['',''] : data[i].seo_title.split('||')
                let seo_desc = data[i].seo_desc === null ? ['',''] :  data[i].seo_desc.split('||')
                let obj = []
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        langId : langIds[j],
                        description : description.length === 1 || description[j] === "" ? description[0] : description[j],
                        name :  name.length === 1 || name[j] === "" ? name[0] : name[j],
                        seo_title :  seo_title.length === 1 || seo_title[j] === "" ? seo_title[0] : seo_title[j],
                        seo_desc :  seo_desc.length === 1 || seo_desc[j] === "" ? seo_desc[0] : seo_desc[j],
                    })
                }
                data[i].language = obj;
                data[i].productStaticFilters = productStaticFilters;
            }

            console.log('data[0]',data[0])
            // console.log('litrages',litrages)

            res.render('Product/editProduct', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                brands : brands,
                // litrages : litrages,
                litrages : staticMethods.litrages(),
                filters : filter,
                additionalFilterProps : additionalFilterProps,
                data : data[0]
            })


        })
        .catch(function (error) {
            console.log('..............',error)
        })
});


router.post('/update/:id',cpUpload, function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)
    let staticFiltersData = JSON.parse(req.body.static_filters)
    console.log('req.body',req.body)
    console.log('dinamicData',dinamicData)
    console.log('staticFiltersData',staticFiltersData)
    console.log('req.files',req.files)
    // return false;
    let updateQuerys = [];
    query.findByMultyNamePromise('product',{id:req.params.id},['image','image','cover_image','hover_image','slug'])
        .then(function (productResult) {
            let oldImage = productResult[0].image;
            let oldHoverImage = productResult[0].hover_image;
            let oldCoverImage = productResult[0].cover_image;
            if(typeof req.files['image'] !== "undefined"){
                if(typeof req.files['image'] !== "undefined") {
                    fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/product/'+oldImage+'',function (err) {
                        updateQuerys.push(query.update2vPromise('product',{where:{id:req.params.id},values:{image:req.files['image'][0].filename}}))
                    });
                }
            }
            if(typeof req.files['hover_image'] !== "undefined") {
                if (req.files['hover_image'][0].filename.length > 0) {
                    fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/product/' + oldHoverImage + '', function (err) {
                        updateQuerys.push(query.update2vPromise('product', {
                            where: {id: req.params.id},
                            values: {hover_image: req.files['hover_image'][0].filename}
                        }))
                    });
                }
            }
            if(typeof req.files['cover_image'] !== "undefined") {
                if (req.files['cover_image'][0].filename.length > 0) {
                    fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/product/' + oldCoverImage + '', function (err) {
                        updateQuerys.push(query.update2vPromise('product', {
                            where: {id: req.params.id},
                            values: {cover_image: req.files['cover_image'][0].filename}
                        }))
                    });
                }
            }

            for(let i = 0; i < dinamicData.length; i++){
                // console.log('req.params.id',req.params.id)
                updateQuerys.push(
                    query.update2vPromise('product_language',
                        {
                            where:{lang_id:''+dinamicData[i].lang_id,product_id :req.params.id},
                            values:{
                                name:dinamicData[i].name,
                                description:dinamicData[i].description,
                                seo_title : dinamicData[i].seo_title == '' ? null : dinamicData[i].seo_title,
                                seo_desc : dinamicData[i].seo_desc == '' ? null : dinamicData[i].seo_desc
                            }
                        }
                    )
                )
            }
            let newSlug = staticMethods.convertStringToSlug(dinamicData[1].name)
            updateQuerys.push(query.update2vPromise('product',
                {
                    where:{id :req.params.id},
                    values:{
                        slug:newSlug+'-'+req.params.id,
                        brand_id : req.body.brand_id,
                        litrage_id : req.body.litrage_id,
                        price : parseFloat(req.body.price),
                        image_top : typeof req.body.image_top !== "undefined" ? req.body.image_top : '0',
                        price_before_sale : req.body.price_before_sale == '' ? null : parseFloat(req.body.price_before_sale),
                        sale_percent : req.body.sale_percent == '' ? null : parseFloat(req.body.sale_percent)
                    }
                }));
            console.log('updateQuerys',updateQuerys)
            Promise.all(updateQuerys)
                .then(function ([data]) {

                    console.log('data',data)
                    // delete static filters
                    if(staticFiltersData.length > 0){
                        query.deletesPromise('filter_product',{product_id:req.params.id}).then(function () {
                            var loopFields = ['filter_prop_id','product_id']
                            var loopPost = []
                            for(var j = 0; j < staticFiltersData.length; j++){
                                loopPost.push([staticFiltersData[j],req.params.id])
                            }
                            query.insertLoopPromise('filter_product',loopFields,loopPost).then(function (data) {
                                res.redirect('/product?page=1');
                                res.end();
                            })
                        })
                    }



                })
                .catch(function (error) {
                    res.json({error:error});
                    res.end();
                })
        })
        .catch(function (error) {
            console.log('........error',error)
        })
});


// router.get('/filters/:id', function(req, res, next) {
//     Promise.all([
//         productQuery.getProductById(req.params.id),
//         filtersQuery.getAllFiltersByLangId(2),
//         filtersQuery.getAllProperties(2)
//     ])
//         .then(function ([data,filters,filterProps]) {
//             var filter = sortingFilters(filters,filterProps);
//             for(let i = 0; i < data.length; i++){
//                 let langIds = data[i].lang_id.split('||')
//                 let name = data[i].name.split('||')
//                 let description = data[i].description.split('||')
//                 let obj = []
//                 for(let j = 0; j < langIds.length; j++){
//                     obj.push({
//                         langId : langIds[j],
//                         description : description.length === 1 || description[j] === "" ? description[0] : description[j],
//                         name :  name.length === 1 || name[j] === "" ? name[0] : name[j],
//                     })
//                 }
//                 data[i].language = obj;
//             }
//             res.render('Product/productFilters', {
//                 siteData  : req.siteData ,
//                 userInfo : req.userInfo,
//                 language : req.language,
//                 data : data[0],
//                 filters : filter,
//             })
//         })
//         .catch(function (error) {
//             console.log('..............',error)
//         })
// });


router.post('/set-favourite', function(req, res, next) {
    let productId = req.body.productId;
    console.log('productId',productId)
    productQuery.getPopularProductById(productId)
        .then(function (data) {
            console.log('data',data)
            if(data.length == 0){
                productQuery.getCountPopularProduct(productId)
                    .then(function (dataCount) {
                        console.log('dataCount',dataCount)
                        if(dataCount[0].rowCount < 9){
                            //insert new popular product
                            query.insert2vPromise('popular_products',{product_id : productId})
                                .then(function (insertData) {
                                    res.json({error:false,msg:'added'})
                                })
                                .catch(function () {
                                    // error inserting
                                    res.json({error:true,msg:'333'})
                                })
                        }else{
                            // 9 items are full
                            res.json({error:true,msg:'111'})
                        }
                    })
                    .catch(function (error) {
                        res.json({error:true,msg:'222'})
                        console.log('..............error 2',error)
                    })
            }else{
                // deleting row
                query.deletesPromise('popular_products',{product_id : productId})
                    .then(function (deleteData) {
                        res.json({error:false,msg:'deleted'})
                    })
                    .catch(function (error) {
                        // error deleting
                        res.json({error:true,msg:'333'})
                    })
            }
        })
        .catch(function (error) {
            console.log('..............error 1',error)
        })
});

router.post('/set-stoke', function(req, res, next) {
    let productId = req.body.productId;

    query.findByMultyNamePromise('product',{id : productId},['id','in_stock'])
        .then(function (data){
            console.log('klklklklklkllk',data)

            var updateVsl = '0';
            if(data[0].in_stock == '0'){
                updateVsl = '1';
            }
            console.log('updateVsl',updateVsl)

            query.update2vPromise('product',{where : {id : productId},values:{in_stock : updateVsl}})
                .then(function (result) {
                    res.json({error:false, msg:updateVsl})
                })
                .catch(function (error){
                    console.log('..............error 2',error)
                })
        })
        .catch(function (error){
            console.log('..............error 1',error)
        })
});

router.post('/delete-additional-filter', function(req, res, next) {
    const product_id = req.body.product_id;
    const group_row_id = req.body.group_row_id;

    query.deletesPromise('additional_filters_language', {product_id : product_id, group_row_id : group_row_id},)
        .then(function (data){
            res.json({error:false})
        })
        .catch(function (error){
            res.json({error:true})
        })
});



router.post('/remove/', function(req, res, next) {
    query.findByMultyNamePromise('product',{id:req.body.id},['image','cover_image','hover_image'])
        .then(function (result) {
            console.log('result',result)
            fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/product/'+result[0].image+'',function (err) {});
            fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/product/'+result[0].cover_image+'',function (err) {});
            fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/product/'+result[0].hover_image+'',function (err) {});
            query.deletesPromise('product',{id:req.body.id})
                .then(function (resultDel) {
                    res.json({error:false})
                })
                .catch(function (error) {
                    res.json({error:true,msg:error.toString()})
                })
        })
        .catch(function (error) {
            res.json({error:true,msg:error.toString()})
        })


});

module.exports = router;


function sortingFilters(filters,filterProps) {
    let result = [];
    for(let i = 0; i < filters.length; i++){
        var obj_1 = {
            filterId : filters[i].id,
            filterName : filters[i].name,
        };
        var arr = [];
        for(let j = 0; j < filterProps.length; j++){
            if(filterProps[j].filter_id == filters[i].id){
                arr.push(
                    {
                        filterPropId : filterProps[j].id,
                        filterPropName : filterProps[j].name
                    }
                )
            }
        }
        obj_1.filterProperties = arr
        result.push(obj_1)
    }
    return result;
}