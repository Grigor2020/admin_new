var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var brandsQuery = require('../../model/dbQuerys/Brands/brands');
var categoriesQuery = require('../../model/dbQuerys/Categories/categories');
var multer = require('multer');
var staticMethods = require('../../model/staticMethods')
var statics = require('../../static')
var fs = require('fs')
var upload = multer({
    fileSize:1024 * 1024,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.mimetype == "video/mp4") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }//size of u file
});


upload.storage = multer.diskStorage({
    destination: '../'+statics.API_FOLDER_NAME+'/public/images/brands',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});
var cpUpload = upload.fields([{ name: 'image', maxCount: 1 },{ name: 'brand_logo', maxCount: 1 }]);


router.get('/', function(req, res, next) {
    Promise.all([
        brandsQuery.getAllBrands(),
        categoriesQuery.getAllCategoriesByLangId(2)
    ])
        .then(function ([data,categories]) {
            // console.log('data',data)
            for(let i = 0; i < data.length; i++){
                data[i].img = statics.API_URL+'/images/brands/'+data[i].brand_logo;
                delete data[i].brand_logo
            }
            // console.log('data',data)
            res.render('Brand/brand', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data,
                categories : categories,
            })
        })
        .catch(function (error) {
            console.log('..............',error)
        })
});

router.get('/add', function(req, res, next) {
    categoriesQuery.getAllCategoriesByLangId(2)
        .then(function (categories) {
            res.render('Brand/addBrand', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                categories : categories
            })
        })
        .catch(function (error) {
            console.log('..............',error)
        })
});

router.get('/update/:id', function(req, res, next) {
    Promise.all([
        categoriesQuery.getAllCategoriesByLangId(2),
        brandsQuery.getBrand(req.params.id)
    ])
        .then(function ([categories,data]) {
            console.log('data',data)
            for(let i = 0; i < data.length; i++){
                let langIds = data[i].lang_id.split('||')
                let name = data[i].name.split('||')
                let title = data[i].title.split('||')
                let text = data[i].text.split('||')
                let obj = []
                console.log('text',text)
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        langId : langIds[j],
                        title : title.length === 1 || title[j] === "" ? title[0] : title[j],
                        name :  name.length === 1 || name[j] === "" ? name[0] : name[j],
                        text : text.length === 1 || text[j] === "" ? text[0] : text[j],
                    })
                }
                data[i].language = obj;
                // delete data[i].lang_id;
                // delete data[i].name;
                // delete data[i].text;
            }
            // console.log('data',data[0].language)
            res.render('Brand/brandUpdate', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data[0],
                categories : categories,
            })
        })
        .catch(function (error) {

        })
});

router.post('/add',cpUpload, function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)
    query.insert2vPromise('brand',{image : req.files.image[0].filename,cat_id : req.body.cat_id,brand_logo : req.files.brand_logo[0].filename})
        .then(function (data) {
            let insertId = data.insertId;
            let fields = ['lang_id','brand_id','title','name','text']
            let post = [];
            for(let i = 0; i < dinamicData.length;i++){
                post.push([''+dinamicData[i].lang_id,''+insertId,''+dinamicData[i].title,''+dinamicData[i].name,''+dinamicData[i].text])
            }
            let newSlug = staticMethods.convertStringToSlug(dinamicData[1].title)
            query.findByMultyNamePromise('brand', {slug : newSlug}, ['id'])
                .then(function (slugData) {
                    var gSlug;
                    if(slugData.length > 0){
                        gSlug = newSlug+'-'+insertId
                    }else{
                        gSlug = newSlug
                    }
                    Promise.all([
                        query.insertLoopPromise('brand_language',fields,post),
                        query.updateV3Promise('brand',{where:{id:insertId},values:{slug : gSlug}})
                    ])
                        .then(function ([insertLoopRes,updateRes]) {
                            res.redirect('/brand');
                        })
                        .catch(function (error) {
                            console.log('error 2',error)
                        })
                })
                .catch(function (error) {
                    console.log('error 3',error)
                })

        })
        .catch(function (error) {
            console.log('error 1',error)
        })
});

router.post('/ord', function(req, res, next) {
    staticMethods.sortData('brand',req.body.ords, function () {
        res.json({error:false})
        res.end();
    })
});


router.post('/update/:id',cpUpload, function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)

    let updateQuerys = [];
    // staticMethods.convertStringToSlug
    query.findByMultyNamePromise('brand',{id:req.params.id},['image','brand_logo','slug'])
        .then(function (brandResult) {
                console.log('req.files',req.files)
                // console.log('oldImage',oldImage)
                if( typeof req.files['image'] !== "undefined") {
                    let oldImage = brandResult[0].image;
                    fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/brands/'+oldImage+'',function (err) {
                        updateQuerys.push(query.update2vPromise('brand',{where:{id:req.params.id},values:{image:req.files['image'][0].filename}}))
                    });
                }
                if(typeof req.files['brand_logo'] !== "undefined") {
                    let old_brand_logo = brandResult[0].brand_logo;
                    fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/brands/'+old_brand_logo+'',function (err) {
                        updateQuerys.push(query.update2vPromise('brand',{where:{id:req.params.id},values:{brand_logo:req.files['brand_logo'][0].filename}}))
                    });
                }
            for(let i = 0; i < dinamicData.length; i++){

                updateQuerys.push(
                    query.update2vPromise('brand_language',
                        {
                            where:{lang_id:''+dinamicData[i].lang_id,brand_id :req.params.id},
                            values:{title:dinamicData[i].title,name:dinamicData[i].name,text:dinamicData[i].text}
                        }
                    )
                )
            }

            let newSlug = staticMethods.convertStringToSlug(dinamicData[1].title)
            query.findByMultyNamePromise('brand', {slug : newSlug}, ['id'])
                .then(function (slugData) {
                    var gSlug;
                    if(slugData.length > 0){
                        gSlug = newSlug+'-'+req.params.id
                    }else{
                        gSlug = newSlug
                    }
                    updateQuerys.push(query.update2vPromise('brand',{where:{id :req.params.id}, values:{slug:gSlug,cat_id : req.body.cat_id}}));
                    // console.log('updateQuerys',updateQuerys)
                    Promise.all(updateQuerys)
                        .then(function (data) {
                            res.redirect('/brand');
                            res.end();
                        })
                        .catch(function (error) {
                            res.json({error:error});
                            res.end();
                        })
                })
                .catch(function (error) {
                    console.log('error 3',error)
                })


        })
        .catch(function (error) {
            console.log('........error',error)
        })
});

router.post('/remove', function(req, res, next) {
    console.log('req.body',req.body)
    let id = req.body.id;
    query.findByMultyNamePromise('brand',{id:req.body.id},['image','brand_logo'])
        .then(function (result) {
            console.log('result',result)
            fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/brands/'+result[0].image+'',function (err) {});
            fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/brands/'+result[0].brand_logo+'',function (err) {});
            query.deletesPromise('brand',{id:req.body.id})
                .then(function (resultDel) {
                    res.json({error:false})
                })
                .catch(function (error) {
                    res.json({error:true,msg:error.toString()})
                })
        })
        .catch(function (error) {
            res.json({error:true,msg:error.toString()})
        })
});
module.exports = router;