var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var homeSlideQuery = require('../../model/dbQuerys/HomeSlide/homeSlide');
var sortTable = require('../../model/staticMethods')
var multer = require('multer');
var statics = require('../../static')
var fs = require('fs')
var upload = multer({
    fileSize:1024 * 1024,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }//size of u file
});

upload.storage = multer.diskStorage({
    destination: '../'+statics.API_FOLDER_NAME+'/public/images/home/slide',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});
var cpUpload = upload.fields([{ name: 'image', maxCount: 1 }]);


router.get('/slide', function(req, res, next) {
    homeSlideQuery.findAllSlides()
        .then(function (data) {
            for(let i = 0; i < data.length; i++){
                let langIds = data[i].slideLangId.split('||')
                let slideTitles = data[i].slideTitle.split('||')
                let obj = []
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        langId : langIds[j],
                        title : slideTitles[j]
                    })
                }
                data[i].title = obj;
                data[i].image = statics.API_URL+'/images/home/slide/'+data[i].slideImage;

                delete data[i].slideLangId;
                delete data[i].slideTitle;
                delete data[i].slideImage;
            }
            res.render('SiteContent/Home/slide', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data,
                title: ''
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })


});
router.get('/login-slider', function(req, res, next) {
    homeSlideQuery.findAllLoginSlides()
        .then(function (data) {
            console.log('1111data',data)
            for(let i = 0; i < data.length; i++){
                let langIds = data[i].langId.split('||')
                let name = data[i].name.split('||')
                let obj = []
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        langId : langIds[j],
                        name : name[j]
                    })
                }
                data[i].language = obj;
                data[i].image = statics.API_URL+'/images/home/slide/'+data[i].image;

                delete data[i].langId;
                delete data[i].name;
            }
            console.log('2222222data',data[0].language)
            res.render('SiteContent/LoginPopup/login_slide', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data,
                title: ''
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })


});

router.get('/login-slider/edit/:slide_id', function(req, res, next) {
    homeSlideQuery.findLoginSlides(req.params.slide_id)
        .then(function (data) {
            console.log('11111111111111111data', data)
            for(let i = 0; i < data.length; i++){
                let lang_id = data[i].lang_id.split('||')
                let name = data[i].name.split('||')
                console.log('namem',name)
                let obj = []
                for(let j = 0; j < lang_id.length; j++){
                    obj.push({
                        langId : lang_id[j],
                        name : name.length===1 ? name[0]  : name[j]
                    })
                }
                data[i].language = obj

                delete data[i].lang_id;
                delete data[i].name;
            }

            console.log('2222222data',data)
            res.render('SiteContent/LoginPopup/login_slide_edit', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data[0],
                title: ''
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })


});

router.post('/login-slider/edit/:id',cpUpload, function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)
    let updateQuerys = [];
    var updatePost = {
        where: {id:req.params.id},
        values:{}
    };
    console.log('........req.files',req.files)
    console.log('........req.body',req.body)
    if(typeof req.files['image'] !== "undefined"){
        query.findByMultyNamePromise('login_slide',{id:req.params.id},['image'])
            .then(function (imageResults) {
                let oldImage = imageResults[0].image;

                if( req.files['image'][0].filename.length > 0) {
                    fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/home/slide/'+oldImage+'',function (err) {
                        updatePost.values.image = req.files['image'][0].filename;
                        updateQuerys.push(query.update2vPromise('login_slide',updatePost))
                    });
                }

            })
            .catch(function (error) {
                console.log('........error',error)
            })
    }


    for(let i = 0; i < dinamicData.length; i++){
        updateQuerys.push(
            query.update2vPromise('login_slide_language',
                {
                    where:{lang_id:dinamicData[i].lang_id,login_slide_id:req.params.id},
                    values:{name:dinamicData[i].name}}
            )
        )
    }

    Promise.all(updateQuerys)
        .then(function (data) {
            res.redirect('/site-content/home/login-slider');
            res.end();
        })
        .catch(function (error) {
            res.json({error:error});
            res.end();
        })
});
router.get('/slide/add', function(req, res, next) {
    res.render('SiteContent/Home/addSlide', {
        siteData  : req.siteData ,
        userInfo : req.userInfo,
        language : req.language,
    });
});
router.post('/slide/add',cpUpload, function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)
    query.insert2vPromise('home_slide',{image : req.files.image[0].filename,url : req.body.url})
        .then(function (data) {
            let insertId = data.insertId;
            let fields = ['lang_id','home_slide_id','title','button_text']
            let post = [];
            for(let i = 0; i < dinamicData.length;i++){
                post.push([''+dinamicData[i].lang_id,''+insertId,''+dinamicData[i].title,''+dinamicData[i].button_text])
            }
            query.insertLoopPromise('home_slide_language',fields,post)
                .then(function (result) {
                    res.redirect('/site-content/home/slide');
                })
                .catch(function (error) {
                    console.log('error 2',error)
                })
        })
        .catch(function (error) {
            console.log('error 1',error)
        })
});

router.get('/slide/edit/:id', function(req, res, next) {
    homeSlideQuery.findSlideById(req.params.id)
        .then(function (data) {
            for(let i = 0; i < data.length; i++){
                let langIds = data[i].slideLangId.split('||')
                let slideTitles = data[i].slideTitle.split('||')
                let slideButtonText = data[i].button_text.split('||')
                let obj = []
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        langId : langIds[j],
                        title : slideTitles[j],
                        button_text : slideButtonText.length === 1 || slideButtonText[j] === "" ? slideButtonText[0] : slideButtonText[j],
                    })
                }
                data[i].language = obj;
                delete data[i].slideLangId;
                delete data[i].slideTitle;
                delete data[i].button_text;
            }
            res.render('SiteContent/Home/slideItem', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data[0],
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});

router.post('/slide/edit/:id',cpUpload, function(req, res, next) {
    console.log('........req.body',req.body)
    let dinamicData = JSON.parse(req.body.res_data)
    let updateQuerys = [];
    var updatePost = {
        where: {id:req.params.id},
        values:{url : req.body.url}
    };
    if(typeof req.files['image'] !== "undefined"){
        query.findByMultyNamePromise('home_slide',{id:req.params.id},['image'])
            .then(function (imageResults) {
                console.log('imageResults',imageResults)
                let oldImage = imageResults[0].image;

                if( req.files['image'][0].filename.length > 0) {
                    fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/home/slide/'+oldImage+'',function (err) {
                        updatePost.values.image = req.files['image'][0].filename;
                    });
                }

            })
            .catch(function (error) {
                console.log('........error',error)
            })
    }
    updateQuerys.push(query.update2vPromise('home_slide',updatePost))

    for(let i = 0; i < dinamicData.length; i++){
        updateQuerys.push(
            query.update2vPromise('home_slide_language',
                {
                    where:{lang_id:dinamicData[i].lang_id,home_slide_id:req.params.id},
                    values:{title:dinamicData[i].title,button_text:dinamicData[i].button_text}}
                )
        )
    }

    Promise.all(updateQuerys)
        .then(function (data) {
            res.redirect('/site-content/home/slide');
            res.end();
        })
        .catch(function (error) {
            res.json({error:error});
            res.end();
        })
});
router.post('/slide/ord', function(req, res, next) {
    console.log('req.body.ords',req.body.ords)
    sortTable.sortData('home_slide',req.body.ords, function () {
        res.json({error:false})
        res.end();
    })
});

router.post('/login-slider/ord', function(req, res, next) {
    console.log('req.body.ords',req.body.ords)
    sortTable.sortData('login_slide',req.body.ords, function () {
        res.json({error:false})
        res.end();
    })
});

router.post('/slide/remove', function(req, res, next) {
    console.log('req.body',req.body)
    let id = req.body.id;
    query.findByMultyNamePromise('home_slide',{id:req.body.id},['image'])
        .then(function (result) {
            console.log('result',result)
            fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/home/slide/'+result[0].image+'',function (err) {});
            query.deletesPromise('home_slide',{id:req.body.id})
                .then(function (resultDel) {
                    res.json({error:false})
                })
                .catch(function (error) {
                    res.json({error:true,msg:error.toString()})
                })
        })
        .catch(function (error) {
            res.json({error:true,msg:error.toString()})
        })
});
module.exports = router;