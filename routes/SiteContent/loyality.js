var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var loyalityQuery = require('../../model/dbQuerys/Loyality/loyality');
var sortTable = require('../../model/staticMethods')
var multer = require('multer');
var statics = require('../../static')


router.get('/', function(req, res, next) {
    loyalityQuery.findAllLoyalities()
        .then(function (data) {
            console.log('data',data)
            res.render('Loyalities/loyalities', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data,
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});
router.get('/add', function(req, res, next) {
    res.render('Loyalities/addLoyalities', {
        siteData  : req.siteData ,
        userInfo : req.userInfo,
        language : req.language
    });
});

router.post('/add', function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)
    query.insert2vPromise('loyality_status',{from_money:req.body.from_money})
        .then(function (data) {
            let insertId = data.insertId;
            let fields = ['lang_id','loyality_status_id','name'];
            let post = [];
            for(let i = 0; i < dinamicData.length;i++){
                post.push([''+dinamicData[i].lang_id,''+insertId,''+dinamicData[i].name])
            }
            Promise.all([
                query.insertLoopPromise('loyality_status_language',fields,post),
            ])

                .then(function ([insertLoopRes,updateRes]) {
                    res.redirect('/site-content/loyality');
                })
                .catch(function (error) {
                    console.log('error 2',error)
                })
        })
        .catch(function (error) {
            console.log('error 1',error)
        })
});
router.get('/update/:id', function(req, res, next) {
    loyalityQuery.findLoyalitieById(req.params.id)
        .then(function (data) {
            for(let i = 0; i < data.length; i++){
                let langIds = data[i].lang_id.split('||')
                let name = data[i].name.split('||')
                let obj = []
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        name : name[j],
                        langId : langIds[j]
                    })
                }
                data[i].language = obj;
                // delete data[i].lang_id;
                // delete data[i].name;
                // delete data[i].text;
            }
            console.log('data',data[0])
            res.render('Loyalities/editLoyalities', {
                siteData  : req.siteData,
                userInfo : req.userInfo,
                language : req.language,
                data: data[0]
            });
        })
        .catch(function (error) {
            console.log('error 1',error)
        })

});

router.post('/update/:id', function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)
    let updateQuerys = [];
    var x = {
        where:{id :req.params.id},
        values:{from_money:parseInt(req.body.from_money)}
    };
    // console.log('.....................',x)
    updateQuerys.push(
        query.update2vPromise('loyality_status',
        x
        )
    );
    for(let i = 0; i < dinamicData.length; i++){
        updateQuerys.push(
            query.update2vPromise('loyality_status_language',
                {
                    where:{lang_id:''+dinamicData[i].lang_id,loyality_status_id :req.params.id},
                    values:{name:dinamicData[i].name}
                }
            )
        )
    }
    Promise.all(updateQuerys)
        .then(function (data) {
            res.redirect('/loyality');
            res.end();
        })
        .catch(function (error) {
            res.json({error:error});
            res.end();
        })
});

module.exports = router;