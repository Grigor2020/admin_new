var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var faqQuery = require('../../model/dbQuerys/Faq/faq');
var staticMethods = require('../../model/staticMethods');
var statics = require('../../static')

router.get('/', function(req, res, next) {
    faqQuery.getAllFaq()
        .then(function (data) {
            res.render('Faq/faq', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data,
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});

router.get('/add', function(req, res, next) {
    res.render('Faq/addFaq', {
        siteData  : req.siteData ,
        userInfo : req.userInfo,
        language : req.language,
    });
});

router.post('/add', function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)
    query.insert2vPromise('faq',{ord:0})
        .then(function (data) {
            let insertId = data.insertId;
            let fields = ['lang_id','faq_id','name','answer','question'];
            let post = [];
            for(let i = 0; i < dinamicData.length;i++){
                post.push([''+dinamicData[i].lang_id,''+insertId,''+dinamicData[i].name,''+dinamicData[i].answer,''+dinamicData[i].question])
            }
            Promise.all([
                query.insertLoopPromise('faq_language',fields,post),
            ])

                .then(function ([insertLoopRes,updateRes]) {
                    res.redirect('/faq');
                })
                .catch(function (error) {
                    console.log('error 2',error)
                })
        })
        .catch(function (error) {
            console.log('error 1',error)
        })
});

router.get('/update/:id', function(req, res, next) {
    faqQuery.getFaq(req.params.id)
        .then(function (data) {
            for(let i = 0; i < data.length; i++){
                let langIds = data[i].lang_id.split('||')
                let name = data[i].name.split('||')
                let answer = data[i].answer.split('||')
                let question = data[i].question.split('||')
                let obj = []
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        langId : langIds[j],
                        name : name[j],
                        answer : answer[j],
                        question : question[j],
                    })
                }
                data[i].language = obj;
            }
            console.log('data[i].language',data[0].language)
            res.render('Faq/editFaq', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data[0],
            })
        })
        .catch(function (error) {
            console.log('error 1',error)
        })
});


router.post('/update/:id', function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)
    console.log('dinamicData',dinamicData)


    let updateQuerys = [];
    for(let i = 0; i < dinamicData.length; i++){
        updateQuerys.push(
            query.updateV3Promise('faq_language',
                {
                    where:{lang_id:''+dinamicData[i].lang_id,faq_id :req.params.id},
                    values:{name:dinamicData[i].name,answer:dinamicData[i].answer,question:dinamicData[i].question}
                }
            )
        )
    }
    Promise.all(updateQuerys)
        .then(function (data) {
            res.redirect('/faq');
            res.end();
        })
        .catch(function (error) {
            res.json({error:error});
            res.end();
        })
});
router.post('/remove', function(req, res, next) {
    query.deletesPromise('faq',{id:req.body.id})
        .then(function (resultDel) {
            res.json({error:false})
        })
        .catch(function (error) {
            res.json({error:true,msg:error.toString()})
        })
});

router.post('/ord', function(req, res, next) {
    staticMethods.sortData('faq',req.body.ords, function () {
        res.json({error:false})
        res.end();
    })
});

module.exports = router;