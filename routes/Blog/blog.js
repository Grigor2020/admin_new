var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var blogQuery = require('../../model/dbQuerys/Blog/blog');
var multer = require('multer');
var staticMethods = require('../../model/staticMethods')
var statics = require('../../static')

var fs = require('fs')
var upload = multer({
    fileSize:1024 * 1024,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.mimetype == "video/mp4") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }//size of u file
});

upload.storage = multer.diskStorage({
    destination: '../'+statics.API_FOLDER_NAME+'/public/images/blog',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + new Date().getTime()+"."+type[1];
        cb(null, fullName);
    }
});
var cpUpload = upload.fields([{ name: 'image', maxCount: 1 }]);
var uploadAreaImages = upload.fields([{ name: 'images', maxCount: 50 }]);

router.get('/', function(req, res, next) {
    blogQuery.getAllBlog()
        .then(function (data) {
            for(let i = 0; i < data.length; i++){
                data[i].img = statics.API_URL+'/images/blog/'+data[i].image;
                delete data[i].image
            }
            console.log('data',data)
            res.render('Blog/blog', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data,
            })
        })
        .catch(function (error) {
            console.log('..............',error)
        })
});

router.get('/add', function(req, res, next) {
    res.render('Blog/addBlog', {
        siteData  : req.siteData ,
        userInfo : req.userInfo,
        language : req.language,
    })
});

router.post('/add',cpUpload, function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)
    var deletableImages = [];
    query.findByMultyNamePromise('blog_images', {group_id : req.body.group_id}, ['name'])
        .then(function (data) {
            for(var i = 0; i < data.length; i++){
                var urlImage = statics.API_URL+"/images/blog/"+data[i].name

                var engFind = true;
                var armFind = true;
                if(!dinamicData[0].text.includes(urlImage)){
                    engFind = false
                }
                if(!dinamicData[1].text.includes(urlImage)){
                    armFind = false
                }
                if(!armFind && !engFind){
                    if(deletableImages.indexOf(urlImage) === -1){
                        deletableImages.push(urlImage);
                        fs.unlink('../api/public/images/blog/'+data[i].name+'',function (err) {

                        });
                    }
                }
            }
            query.deletesPromise('blog_images',{group_id : req.body.group_id})
                .then(function (data) {
                    query.insert2vPromise('blog',{image : req.files.image[0].filename})
                        .then(function (data) {
                            let insertId = data.insertId;
                            let fields = ['lang_id','blog_id','title','text']
                            let post = [];
                            for(let i = 0; i < dinamicData.length;i++){
                                post.push([''+dinamicData[i].lang_id,''+insertId,''+dinamicData[i].title,''+dinamicData[i].text])
                            }
                            let newSlug = staticMethods.convertStringToSlug(dinamicData[1].title)
                            Promise.all([
                                query.insertLoopPromise('blog_language',fields,post),
                                query.update2vPromise('blog',{where:{id:insertId},values:{slug : newSlug+'-'+insertId}})
                            ])
                                .then(function ([insertLoopRes,updateRes]) {
                                    res.redirect('/blog');
                                })
                                .catch(function (error) {
                                    console.log('error 4',error)
                                })
                        })
                        .catch(function (error) {
                            console.log('error 3',error)
                        })
                })
                .catch(function (error) {
                    console.log('error 2',error)
                })
        })
        .catch(function (error) {
            console.log('error 1',error)
        })
});


router.post('/add-blog-images/:group_id',uploadAreaImages, function(req, res, next) {
    console.log('req.files.images',)
    if(typeof req.files.images !== "undefined") {
        var imageUrls = [];
        let fields = ['name', 'group_id']
        let post = [];
        // var token = staticMethods.generateRandomIntegers(4);

        for (let i = 0; i < req.files.images.length; i++) {
            post.push(['' + req.files.images[i].filename, req.params.group_id])
            imageUrls.push(statics.API_URL + '/images/blog/' + req.files.images[i].filename)
        }
        query.insertLoopPromise('blog_images', fields, post)
            .then(function () {
                res.json({error: false, imageUrls: imageUrls, token: req.params.group_id})
            })
            .catch(function () {
                res.json({error: true, imageUrls: []})
            })
    }else{
        res.json({error: true, imageUrls: []})
    }
});


// router.post('/area-images/add',uploadAreaImages, function(req, res, next) {
//     console.log('req.body',req.body)
//     console.log('req.files',req.files)
//     let raw =req.body.upload;
//     console.log('raw...............',raw)
//     // let base64Image = raw.split(';base64,');
//     // console.log('base64Image...............',base64Image)
//     // var type = base64Image[0].split('/')
//     let random = Math.round(Math.random()*1299999);
//     let fullName = random + '-' + Date.now();
//     // console.log('fullName...............',fullName)
//     function decodeBase64Image(dataString) {
//         // console.log('dataString',dataString)
//         // var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
//         //     response = {};
//         //
//         // if (matches.length !== 3) {
//         //     return new Error('Invalid input string');
//         // }
//         //
//         // response.type = matches[1];
//         // response.data = Buffer.from(matches[2], 'base64');
//
//         return dataString
//     }
//
//     var imageBuffer = decodeBase64Image(raw);
//     console.log('imageBuffer',imageBuffer);
//     fs.writeFile(fullName+'.jpeg', imageBuffer.data, function(err,asd) { console.log('err..........',err) });
//     // let dinamicData = JSON.parse(req.body.res_data)
//     // query.insert2vPromise('blog',{image : req.files.image[0].filename})
//     //     .then(function (data) {
//     //         let insertId = data.insertId;
//     //         let fields = ['lang_id','blog_id','title','text']
//     //         let post = [];
//     //         for(let i = 0; i < dinamicData.length;i++){
//     //             post.push([''+dinamicData[i].lang_id,''+insertId,''+dinamicData[i].title,''+dinamicData[i].text])
//     //         }
//     //         let newSlug = staticMethods.convertStringToSlug(dinamicData[1].title)
//     //         Promise.all([
//     //             query.insertLoopPromise('blog_language',fields,post),
//     //             query.update2vPromise('blog',{where:{id:insertId},values:{slug : newSlug+'-'+insertId}})
//     //         ])
//     //
//     //             .then(function ([insertLoopRes,updateRes]) {
//     //                 res.redirect('/blog');
//     //             })
//     //             .catch(function (error) {
//     //                 console.log('error 2',error)
//     //             })
//     //     })
//     //     .catch(function (error) {
//     //         console.log('error 1',error)
//     //     })
// });

router.get('/update/:id', function(req, res, next) {
    blogQuery.getBlog(req.params.id)
        .then(function (data) {
            for(let i = 0; i < data.length; i++){
                let langIds = data[i].lang_id.split('||')
                let title = data[i].title.split('||')
                let text = data[i].text.split('||')
                let obj = []
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        langId : langIds[j],
                        title : title[j],
                        text : text[j],
                    })
                }
                data[i].language = obj;
            }
            res.render('Blog/editBlog', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data[0],
            })
        })
        .catch(function (error) {

        })
});

router.post('/update/:id',cpUpload, function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)
    var deletableImages = [];
    query.findByMultyNamePromise('blog_images', {group_id : req.body.group_id}, ['name'])
        .then(function (data) {
            for(var i = 0; i < data.length; i++){
                var urlImage = statics.API_URL+"/images/blog/"+data[i].name

                var engFind = true;
                var armFind = true;
                if(!dinamicData[0].text.includes(urlImage)){
                    engFind = false
                }
                if(!dinamicData[1].text.includes(urlImage)){
                    armFind = false
                }
                if(!armFind && !engFind){
                    if(deletableImages.indexOf(urlImage) === -1){
                        deletableImages.push(urlImage);
                        fs.unlink('../api/public/images/blog/'+data[i].name+'',function (err) {

                        });
                    }
                }
            }
            query.deletesPromise('blog_images',{group_id : req.body.group_id})
                .then(function (data) {
                    let updateQuerys = [];
                    query.findByMultyNamePromise('blog',{id:req.params.id},['image','slug'])
                        .then(function (blogResult) {
                            if(typeof req.files['image'] !== "undefined"){
                                let oldImage = blogResult[0].image;
                                if( req.files['image'][0].filename.length > 0) {
                                    fs.unlink('../api/public/images/blog/'+oldImage+'',function (err) {
                                        updateQuerys.push(query.update2vPromise('blog',{where:{id:req.params.id},values:{image:req.files['image'][0].filename}}))
                                    });
                                }
                            }
                            for(let i = 0; i < dinamicData.length; i++){
                                updateQuerys.push(
                                    query.update2vPromise('blog_language',
                                        {
                                            where:{lang_id:''+dinamicData[i].lang_id,blog_id :req.params.id},
                                            values:{title:dinamicData[i].title,text:dinamicData[i].text}
                                        }
                                    )
                                )
                            }
                            let newSlug = staticMethods.convertStringToSlug(dinamicData[1].title)
                            updateQuerys.push(query.update2vPromise('blog',{where:{id :req.params.id}, values:{slug:newSlug+'-'+req.params.id}}));
                            Promise.all(updateQuerys)
                                .then(function (data) {
                                    res.redirect('/blog');
                                    res.end();
                                })
                                .catch(function (error) {
                                    res.json({error:error});
                                    res.end();
                                })
                        })
                        .catch(function (error) {
                            console.log('........error',error)
                        })
                })
                .catch(function (error) {
                    console.log('error 2',error)
                })
        })
        .catch(function (error) {
            console.log('error 1',error)
        })
});

router.post('/remove', function(req, res, next) {
    console.log('req.body',req.body)
    let id = req.body.id;
    Promise.all([
        query.findByMultyNamePromise('blog',{id:req.body.id},['image']),
        query.findByMultyNamePromise('blog_language',{blog_id:req.body.id},['text'])
    ])
        .then(function ([result,languageResult]) {
            // console.log('result',result)
            // console.log('languageResult',languageResult)
            for(let i = 0; i < languageResult.length; i++){
                let string = languageResult[i].text;
                let matching = string.match(/src\=("|')([^"']+)/g);
                let images = [];
                for (let j = 0; j < matching.length; j++) {
                    let x = matching[j].replace(/src\=(\'|\")/, '');
                    let y = x.split('/')
                    let z = y[y.length - 1]
                    if(images.indexOf(z) === -1){
                        fs.unlink('../api/public/images/blog/'+z+'',function (err) {});
                        images.push(z)
                    }
                }
            }
            fs.unlink('../api/public/images/blog/'+result[0].image+'',function (err) {});
            query.deletesPromise('blog',{id:req.body.id})
                .then(function (resultDel) {
                    res.json({error:false})
                })
                .catch(function (error) {
                    res.json({error:true,msg:error.toString()})
                })
        })
        .catch(function (error) {
            res.json({error:true,msg:error.toString()})
        })
});

router.post('/ord', function(req, res, next) {
    staticMethods.sortData('blog',req.body.ords, function () {
        res.json({error:false})
        res.end();
    })
});

router.post('/set-main', function(req, res, next) {
    let blog_id = req.body.blog_id;
    query.findByMultyNamePromise('blog_top',{blog_id:blog_id},['id'])
        .then(function (data) {
            if(data.length === 0){
                query.findAllPromise('blog_top',null)
                    .then(function (count) {
                        var allCount = count.length;
                        if(allCount < 4){
                            query.insert2vPromise('blog_top',{blog_id:blog_id})
                                .then(function (){
                                    res.json({error:false,msg:'added'})
                                })
                        }else {
                            res.json({error:true,msg:'111'})
                        }
                    })
                    .catch(function (error) {
                        res.json({error:true,msg:error.toString()})
                    })
            }else{
                query.deletesPromise('blog_top',{id:data[0].id})
                    .then(function (){
                        res.json({error:false,msg:'deleted'})
                    })
            }
        })
        .catch(function (error) {
            res.json({error:true,msg:error.toString()})
        })
});


module.exports = router;