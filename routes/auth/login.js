var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys')
var sha1 = require('sha1');


router.get('/', function(req, res, next) {
    res.render('Auth/login', { title: 'Pernod Ricard',failedLogin:false });
});


router.post('/', function(req, res, next) {
    if(req.body.login !== "" && req.body.password !== ""){
        var password = sha1(req.body.password);
        query.adminLogin({login: req.body.login, password: password}, function (result) {
            if(result.length === 1){
                console.log('result',result)
                res.cookie('clin', result[0].token);
                // res.redirect('/');
                res.json({error:false})
            }else{
                res.json({error:true})
                res.end();
            }
        })
    }else{
        res.json({error:true})
        res.end();
    }
});


router.get('/logout', function(req, res, next) {
    res.clearCookie("clin");
    res.redirect('/');
    res.end();
});


module.exports = router;
