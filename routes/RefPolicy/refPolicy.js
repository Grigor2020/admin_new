var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var statics = require('../../static')

router.get('/', function(req, res, next) {
    query.findAllPromise('refPolicy',null)
        .then(function (data) {
            res.render('RefPolicy/refPolicy', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data,
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});

router.post('/', function(req, res, next) {
    let dinamicData = JSON.parse(req.body.res_data)

    let updateQuerys = [];
    for(let i = 0; i < dinamicData.length; i++){
        updateQuerys.push(query.updateStaticsV3Promise('refPolicy',dinamicData[i].lang_id,dinamicData[i].text))
    }

    Promise.all(updateQuerys)
        .then(function (data) {
            res.redirect('/ref-policy');
            res.end();
        })
        .catch(function (error) {
            res.json({error:error});
            res.end();
        })
});
module.exports = router;