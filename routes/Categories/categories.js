var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var categoriesQuery = require('../../model/dbQuerys/Categories/categories');
var staticMethods = require('../../model/staticMethods');
var statics = require('../../static');
var multer = require('multer');
const fs = require('fs');
// const fileUpload = require('express-fileupload');
//
//
// var svgUpload = fileUpload({
//     tempFileDir : '../'+statics.API_FOLDER_NAME+'/public/images/categories'
// });

var upload = multer({
    fileSize:1024 * 1024,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.mimetype == "video/mp4") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }//size of u file
});


upload.storage = multer.diskStorage({
    destination: '../'+statics.API_FOLDER_NAME+'/public/images/categories',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});
var cpUpload = upload.fields([{ name: 'image', maxCount: 1 },{ name: 'effect_image', maxCount: 1 }]);

router.get('/', function(req, res, next) {
    categoriesQuery.getAllCategories()
        .then(function (data) {
            console.log('data',data)
            res.render('Categories/categories', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data,
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});

router.get('/update/:id', function(req, res, next) {
    const catId = req.params.id;
    categoriesQuery.getCategorieById(catId)
        .then(function (data) {
            for(let i = 0; i < data.length; i++){
                let langIds = data[i].lang_id.split('||')
                let name = data[i].name.split('||')
                let obj = []
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        langId : langIds[j],
                        name :  name.length === 1 || name[j] === "" ? name[0] : name[j]
                    })
                }
                data[i].language = obj;
                // delete data[i].lang_id;
                // delete data[i].name;
                // delete data[i].text;
            }
            console.log('data',data[0])
            res.render('Categories/editCategories', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data : data[0],
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});

router.post('/update/:id',cpUpload, function(req, res, next) {
    console.log('req.files.image',typeof req.files)
    // Use the mv() method to place the file somewhere on your server
    // if(typeof req.files.image.name !== 'undefined'){
    //     let image = req.files.image;
    //     let type = req.files.image.name.split(".");
    //     let random = Math.round(Math.random()*1299999);
    //     var fullName = random + '-' + Date.now()+"."+type[1];
    //     image.mv('../api/public/images/categories/'+fullName, );
    // }

    // end upload svg

    // console.log('req.body.res_data',req.body.res_data)
    let dinamicData = JSON.parse(req.body.res_data)
    // console.log('req.files["image"]',req.files['image'])
    let updateQuerys = [];
    // staticMethods.convertStringToSlug
    query.findByMultyNamePromise('categories',{id:req.params.id},['image','slug'])
        .then(function (categoriesResult) {
            if(typeof req.files.image !== 'undefined'){
                let oldImage = categoriesResult[0].image;
                fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/categories/'+oldImage+'',function (err) {
                    updateQuerys.push(query.update2vPromise('categories',{where:{id:req.params.id},values:{image:req.files.image[0].filename}}))
                });
            }
            if(typeof req.files.effect_image !== 'undefined'){
                let oldImage = categoriesResult[0].effect_image;
                fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/categories/'+oldImage+'',function (err) {
                    updateQuerys.push(query.update2vPromise('categories',{where:{id:req.params.id},values:{effect_image:req.files.effect_image[0].filename}}))
                });
            }
            for(let i = 0; i < dinamicData.length; i++){
                // console.log('req.params.id',req.params.id)
                updateQuerys.push(
                    query.update2vPromise('categories_language',
                        {
                            where:{lang_id:''+dinamicData[i].lang_id,cat_id :req.params.id},
                            values:{name:dinamicData[i].name}
                        }
                    )
                )
            }

            let newSlug = staticMethods.convertStringToSlug(dinamicData[1].name)
            updateQuerys.push(query.update2vPromise('categories',{where:{id :req.params.id}, values:{slug:newSlug+'-'+req.params.id,cat_id : req.body.cat_id}}));
            // console.log('updateQuerys',updateQuerys)
            Promise.all(updateQuerys)
                .then(function (data) {
                    res.redirect('/categories');
                    res.end();
                })
                .catch(function (error) {
                    res.json({error:error});
                    res.end();
                })
        })
        .catch(function (error) {
            console.log('........error',error)
        })
});

router.get('/add', function(req, res, next) {
    res.render('Categories/addCategories', {
        siteData  : req.siteData ,
        userInfo : req.userInfo,
        language : req.language
    });
});

router.post('/add',cpUpload, function(req, res, next) {
    // console.log('req.files',req.files);
    // console.log('req.files.image.name',typeof req.files.image.name);

    // Use the mv() method to place the file somewhere on your server
    // let image = req.files.image;
    // let type = req.files.image.name.split(".");
    // let random = Math.round(Math.random()*1299999);
    // let fullName = random + '-' + Date.now()+"."+type[1];
    // image.mv('../api/public/images/categories/'+fullName, );
    // end upload svg

    let dinamicData = JSON.parse(req.body.res_data)
    query.insert2vPromise('categories',{image:req.files.image[0].filename, effect_image:req.files.effect_image[0].filename})
        .then(function (data) {
            const insertId = data.insertId;
            let fields = ['lang_id','cat_id','name']
            let post = [];
            for(let i = 0; i < dinamicData.length;i++){
                post.push([''+dinamicData[i].lang_id,''+insertId,''+dinamicData[i].name])
            }
            let newSlug = staticMethods.convertStringToSlug(dinamicData[1].name)
            Promise.all([
                query.insertLoopPromise('categories_language',fields,post),
                query.update2vPromise('categories',{where:{id:insertId},values:{slug : newSlug+'-'+insertId}})
            ])
                .then(function ([insertLoopRes,updateRes]) {
                    res.redirect('/categories');
                })
                .catch(function (error) {
                    console.log('error 2',error)
                })
        })
        .catch(function (error) {
            console.log('error 1',error)
        })
});


router.post('/ord', function(req, res, next) {
    staticMethods.sortData('categories',req.body.ords, function () {
        res.json({error:false})
        res.end();
    })
});

router.post('/remove', function(req, res, next) {
    let id = req.body.id;
    query.findByMultyNamePromise('categories',{id:req.body.id},['image'])
        .then(function (result) {
            console.log('result',result)
            fs.unlink('../'+statics.API_FOLDER_NAME+'/public/images/categories/'+result[0].image+'',function (err) {});
            query.deletesPromise('categories',{id:id})
                .then(function () {
                    res.json({error:false})
                })
                .catch(function (error) {
                    res.json({error:true,msg:error.toString()})
                })
        })
        .catch(function (error) {
            res.json({error:true,msg:error.toString()})
        })
});
module.exports = router;