var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var litrageQuery = require('../../model/dbQuerys/Litrage/litrage');
var categoriesQuery = require('../../model/dbQuerys/Categories/categories');
var staticMethods = require('../../model/staticMethods')
var statics = require('../../static')


router.get('/', function(req, res, next) {
    Promise.all([
        litrageQuery.getAllLitrages(),
    ])
        .then(function ([data]) {
            // console.log('data',data)
            res.render('Litrage/litrage', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                data : data
            })
        })
        .catch(function (error) {
            console.log('..............',error)
        })
});

router.post('/add', function(req, res, next) {
    if(typeof req.body.name !== "undefined" && req.body.name !== ""){
        query.insert2vPromise('litrages',{name : req.body.name})
            .then(function () {
                res.json({error:false})
            })
            .catch(function () {
                res.json({error:true})
            })
    }else{
        res.json({error:true})
    }
});
router.post('/edit', function(req, res, next) {
    if(typeof req.body.name !== "undefined" && req.body.name !== ""&& typeof req.body.id !== "undefined" && req.body.id !== ""){
        query.update2vPromise('litrages',{where : {id : req.body.id}, values : {name : req.body.name}})
            .then(function () {
                res.json({error:false})
            })
            .catch(function () {
                res.json({error:true})
            })
    }else{
        res.json({error:true})
    }
});
router.post('/remove', function(req, res, next) {
    if(typeof req.body.id !== "undefined" && req.body.id !== ""){
        query.deletesPromise('litrages',{id: req.body.id})
            .then(function () {
                res.json({error:false})
            })
            .catch(function () {
                res.json({error:true})
            })
    }else{
        res.json({error:true})
    }
});

router.post('/ord', function(req, res, next) {
    staticMethods.sortData('litrages',req.body.ords, function () {
        res.json({error:false})
        res.end();
    })
});

module.exports = router;