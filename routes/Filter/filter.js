var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var filtersQuery = require('../../model/dbQuerys/Filters/filters');
var staticMethods = require('../../model/staticMethods');
var statics = require('../../static')

router.get('/', function(req, res, next) {
    filtersQuery.getAllFilters()
        .then(function (data) {
            console.log('data...',data)
            for(let i = 0; i < data.length; i++){
                let langIds = data[i].lang_id.split('||')
                let name = data[i].name.split('||')
                let obj = []
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        langId : langIds[j],
                        name : name[j]
                    })
                }
                data[i].language = obj;
            }
            console.log('data',data)
            res.render('Filter/filter', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data:data
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});


router.post('/add', function(req, res, next) {
    console.log('req.body',req.body)
    let dinamicData = JSON.parse(req.body.res_data)
    console.log('dinamicData',dinamicData)
    query.insert2vPromise('filter',{type : req.body.type})
        .then(function (data) {
            let insertId = data.insertId;
            let fields = ['lang_id','filter_id','name']
            let post = [];
            for(let i = 0; i < dinamicData.length;i++){
                post.push([''+dinamicData[i].lang_id,''+insertId,''+dinamicData[i].name])
            }
            Promise.all([
                query.insertLoopPromise('filter_language',fields,post),
            ])

                .then(function ([insertLoopRes]) {
                    res.redirect('/filter');
                })
                .catch(function (error) {
                    console.log('error 2',error)
                })
        })
        .catch(function (error) {
            console.log('error 1',error)
        })
});
router.post('/update', function(req, res, next) {
    console.log('req.body',req.body)
    let dinamicData = JSON.parse(req.body.__res_datas)
    console.log('dinamicData',dinamicData)
    let updateQuerys = [];
    for(let i = 0; i < dinamicData.length; i++){
        updateQuerys.push(
            query.update2vPromise('filter_language',
                {
                    where:{lang_id:dinamicData[i].lang_id,filter_id:dinamicData[i].filter_id},
                    values:{name:dinamicData[i].name}
                }
            )
        )
    }
    Promise.all(updateQuerys)
        .then(function (data) {
            res.redirect('/filter');
            res.end();
        })
        .catch(function (error) {
            res.json({error:error});
            res.end();
        })
});

router.post('/ord', function(req, res, next) {
    staticMethods.sortData('filter',req.body.ords, function () {
        res.json({error:false})
        res.end();
    })
});

router.post('/remove', function(req, res, next) {

            query.deletesPromise('filter',{id:req.body.id})
                .then(function (resultDel) {
                    res.json({error:false})
                })
                .catch(function (error) {
                    res.json({error:true,msg:error.toString()})
                })
});
module.exports = router;