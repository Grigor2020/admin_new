var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var filtersQuery = require('../../model/dbQuerys/Filters/filters');
var staticMethods = require('../../model/staticMethods');
var statics = require('../../static')

router.get('/', function(req, res, next) {
    query.getAllTranslates()
        .then(function (data) {
            // console.log('data...',data)
            for(let i = 0; i < data.length; i++){
                let langIds = data[i].lang_id.split('||')
                let name = data[i].name.split('||')
                let obj = []
                for(let j = 0; j < langIds.length; j++){
                    obj.push({
                        id : data[i].id,
                        key_name : data[i].key_name,
                        langId : langIds[j],
                        name : name.length === 1 ? name[0] : name[j]
                    })
                }
                data[i].language = obj;
            }
            // console.log('data',data[0].language)
            res.render('Translate/translate', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                data:data
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});


router.post('/add', function(req, res, next) {
    console.log('req.body',req.body)
    let dinamicData = JSON.parse(req.body.res_data)
    console.log('dinamicData',dinamicData)
    let keyName = dinamicData[0].key_name;
    query.findByMultyNamePromise('translates',{key_name : keyName},['id'])
        .then(function (checkFinder) {
            if(checkFinder.length == 0){
                query.insert2vPromise('translates',{key_name : keyName})
                    .then(function (trInsert) {
                        let fields = ['translates_id ','lang_id','name']
                        let post = [];
                        for(let i = 0; i < dinamicData.length;i++){
                            post.push([trInsert.insertId,''+dinamicData[i].lang_id,''+dinamicData[i].name])
                        }
                        Promise.all([
                            query.insertLoopPromise('translates_language',fields,post),
                        ])
                            .then(function ([insertLoopRes]) {
                                res.json({error : false})
                            })
                            .catch(function (error) {
                                console.log('error 2',error)
                            })
                    })
                    .catch(function (error) {
                        console.log('error 1',error)
                    })
            }else{
                res.json({error : true})
            }
        })
        .catch(function (error) {
            console.log('error 0',error)
        })
});


router.post('/remove', function(req, res, next) {
    query.deletesPromise('translates',{id:req.body.id})
        .then(function (resultDel) {
            res.json({error:false})
        })
        .catch(function (error) {
            res.json({error:true,msg:error.toString()})
        })
});
module.exports = router;