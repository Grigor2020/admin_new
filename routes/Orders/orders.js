var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var ordersQuery = require('../../model/dbQuerys/Ordes/orders');
var statics = require('../../static')
var Update = require('../../objects/Update');
var staticMethods = require('../../model/staticMethods')

router.get('/', function(req, res, next) {
    var page = req.query.page;
    Promise.all([
        ordersQuery.findOrders(page),
        ordersQuery.findOrdersCount()
    ])
        .then(function ([orders,ordersCount]) {
            var pagesCount = Math.ceil(parseInt(ordersCount) / staticMethods.pageProductCount);
            console.log('orders............................',orders)
            console.log('thisPage............................',page)




            for(var i = 0; i < orders.length; i++){
                var data = new Date(orders[i].insert_data);
                orders[i].date = data.getDay()+'/'+parseInt(data.getMonth())+1+'/'+data.getFullYear()
                orders[i].time = data.getHours()+':'+data.getMinutes()
            }
            // console.log('orders............................',orders)
            res.render('Orders/orders', {
                siteData  : req.siteData ,
                language : req.language,
                orders : orders,
                pageCount :  pagesCount,
                thisPage : parseInt(page)
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});

router.get('/info/:id', function(req, res, next) {
    var id = req.params.id;
    Promise.all([
        ordersQuery.findOrderById(id),
        ordersQuery.findOrderProducts(id),
    ])
        .then(function ([order,products]) {
            console.log('order............................',order)
            if(order[0].admin_view === null){
                console.log('nmulllllll')
                query.update2vPromise('orders',{where :{ id : id}, values:{admin_view : 1}})
            }
            console.log('order............................',order)
            // console.log('products............................',products)
            // for(var i = 0; i < order.length; i++){
            //     order[i].bankPaymentInitResponse = JSON.parse(order[i].bankPaymentInitResponse)
            //     order[i].bankPaymentDetailResponse = JSON.parse(order[i].bankPaymentDetailResponse)
            // }
            res.render('Orders/orderItem', {
                siteData  : req.siteData ,
                language : req.language,
                order :  order[0],
                products :  products,
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});

router.post('/change-order-status', function(req, res, next) {
    if(typeof req.body.status !== "undefined") {
        var status = req.body.status;
        var orderId = req.body.id;
        if (status !== '99') {
            query.update2vPromise('orders',{where : {id:orderId},values : {order_status : status}})
                .then(function (data) {
                    res.json({error:false})
                })
                .catch(function (error) {
                    res.json({error:true})
                })
        }
    }
});



module.exports = router;