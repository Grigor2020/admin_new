var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var statics = require('../../static')
var Update = require('../../objects/Update');

router.get('/', function(req, res, next) {
    console.log('req.cookies',req.cookies)
    query.findAllPromise('settings',null)
        .then(function (data) {
            res.render('Settings/settings', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                data : data[0],
                errorField : typeof req.cookies.errorField === "undefined" ? null : req.cookies.errorField,
                errorMsg : typeof req.cookies.errorMsg === "undefined" ? null : req.cookies.errorMsg
            });
        })
        .catch(function (error) {
            console.log('error 2',error)
        })
});

router.post('/', async function(req, res) {
    // let checkInfo = await checkSettingsInfo(req.body)
    // if(!checkInfo.error){
    //     let mUpdate = new Update();
    //     await mUpdate.setUpdateObject(req.body)
    //     await mUpdate.setWhereObject({id:1})
    //     await mUpdate.setTable("settings");
    //     let data = await mUpdate.update()
    //         // if(data.error){
    //         //     res.cookie('error','0');
    //             res.redirect('/settings');
    //         // }
    // }else{
    //     res.cookie('errorField',checkInfo.errorField);
    //     res.cookie('errorMsg',checkInfo.errorMsg);
    //     res.redirect('/settings');
    // }

    query.update2vPromise('settings',{where :{id:1},values : {
            site_name : req.body.site_name,
            admin_email : req.body.admin_email,
            public_email : req.body.public_email,
            fb_link : req.body.fb_link,
            public_phone : req.body.public_phone,
            public_fax : req.body.public_fax
        }})
        .then(function (data){
            res.redirect('/settings');
        })
        .catch(function (error) {
            res.redirect('/settings');
        })

});

module.exports = router;

async function checkSettingsInfo(data) {
    let obj = {
        error : false,
        errorMsg : "",
        errorField : ""
    }

    if(typeof data.site_name !== "undefined"){
        if(data.site_name.length > 250){
            obj.error = true;
            obj.errorField = 'site_name';
            obj.errorMsg = 'long_data';
        }
    }
    if(typeof data.admin_email !== "undefined"){
        if(data.admin_email.length > 250){
            obj.error = true;
            obj.errorField = 'admin_email';
            obj.errorMsg = 'long_data';
        }
    }
    if(typeof data.public_email !== "undefined"){
        if(data.public_email.length > 250){
            obj.error = true;
            obj.errorField = 'admin_email';
            obj.errorMsg = 'long_data';
        }
    }
    return obj
}