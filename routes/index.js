var express = require('express');
var router = express.Router();
var query = require('../model/dbQuerys/querys');



/* GET home page. */
router.get('/', function(req, res, next) {
    Promise.all([
        query.getSoltItemsCount(),
        query.getAllProdCount(),
        query.getAllUsersCount(),
    ])
        .then(function ([soldItemsCount,allProdCount,allUsersCount]) {
            console.log('soldItemsCount',soldItemsCount)
            console.log('allProdCount',allProdCount)
            console.log('allUsersCount',allUsersCount)

            res.render('index', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                soldItemsCount : soldItemsCount,
                allProdCount : allProdCount,
                allUsersCount : allUsersCount,
                title: 'Proshcall Admin'
            });
        })
        .catch(function (error) {
            console.log('error',error)
            res.render('index', {
                siteData  : req.siteData ,
                userInfo : req.userInfo,
                language : req.language,
                soldItemsCount : 0,
                allProdCount : 0,
                allUsersCount : 0,
                title: 'Proshcall Admin'
            });
        })



});

module.exports = router;
