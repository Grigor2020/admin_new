
$(document).ready(function () {



    /* for lang */
    // $(document).on("change",".onoffswitch-checkbox",function() {
    //     if($(this).prop("checked")){
    //         $('.forrus').css('display','none');
    //         $('.foreng').css('display','block');
    //     }else{
    //         $('.forrus').css('display','block');
    //         $('.foreng').css('display','none');
    //     }
    // });

    $(".lang_colums").click(function () {
        $(".lang_colums").removeClass('active');
        $(this).addClass('active');
        $('.foren').css({'display':'none'})
        $('.foram').css({'display':'none'})
        $('.for'+$(this).attr('data-name')+'').css({
            'display':'block'
        })
    });

    // $("._radio_lang").change(function() {
    //     var langIndex = $(this).attr("data-name");
    //     $("._change_lang").each(function () {
    //         if(langIndex==$(this).attr("data-lnag")){
    //             $(this).css('display','block');
    //         }else{
    //             $(this).css('display','none');
    //         }
    //     })
    // });
    /* for lang end */

    /* cookie notification */
    var cok = getCookie('not');
    if(cok == 'up'){
        $('#update_not').css('display','block');
        $('#save_not').css('display','none');
        $('.save_done').css('top','0');
        deleteCookie('not');
        setTimeout(function(){
            $('.save_done').css('top','-120px');
        },2000)
    }

    if(cok == 'save'){
        $('#save_not').css('display','block');
        $('#update_not').css('display','none');
        $('.save_done').css('top','0');
        deleteCookie('not');
        setTimeout(function(){
            $('.save_done').css('top','-120px');
        },2000)
    }
    /* cookie notification */

    $('.setsize').click(function () {
        elementChild = $(this).children('i');
        elementChild.toggleClass('fa-minus');
        elementChild.toggleClass('fa-plus');
        element = $(this).parent('div').parent('div').next('div.box_edit');
        element.slideToggle();
    });

    // $( "._foto_block" ).click(function() {
    //   $(this).children(".img_file" )[0].click();
    // });
    $( ".foto_block img" ).click(function() {
        $(this).next(".img_file" )[0].click();
    });
    $( ".foto_blocks img" ).click(function() {
        $(this).next(".img_file" )[0].click();
    });

    // $( "._chose_file" ).click(function() {
    //     $(this).next('.foto_block').children(".img_file" )[0].click();
    // });


    function renderImage(file,x){
        var reader = new FileReader();
        reader.onload = function(event){
                the_url = event.target.result;
                $(x).prev('img').css('display','block');
                $(x).prev('img').attr('src',the_url);
        }
        reader.readAsDataURL(file);
    }

    $( ".img_file" ).change(function() {
        $(this).parent('div.foto_block').removeClass('forempty');
        $(this).parent('div.foto_block').addClass('forfull');
        renderImage(this.files[0],this)
    });

    /*********** Absolute img ************/
        // $( "._foto_block" ).click(function() {
        //     $(this).children(".img_file" )[0].click();
        // });


        function renderImageAb(file,x){
            var reader = new FileReader();
            reader.onload = function(event){
                the_url = event.target.result;
                $(x).prev('img').css('display','block');
                $(x).prev('img').attr('src',the_url);
            }
            reader.readAsDataURL(file);
        }

        $( ".ab_img_f" ).change(function() {
            $(this).parent('div.foto_block').removeClass('forempty');
            $(this).parent('div.foto_block').addClass('forfull');
            renderImageAb(this.files[0],this)
        });
    /***********************/

    /****************************************/
        // $( "._video_block" ).click(function() {
        //     $(this).children(".video_file" )[0].click();
        // });


        $( "._chose_video" ).click(function() {
            $(this).next('.video_block').children(".video_file" )[0].click();
            console.log('asd')
        });

        function renderVideo(file,x){
            var reader = new FileReader();
            reader.onload = function(event){
                the_url = event.target.result;
                $(x).next('video').css('display','block');
                $(x).next('video').attr('src',the_url);
                //$(x).prev('video').children('source').attr('src',the_url);
            }
            reader.readAsDataURL(file);
        }

        $( ".video_file" ).change(function() {
            $(this).parent('div.video_block').removeClass('forempty');
            $(this).parent('div.video_block').addClass('forfull');
            renderVideo(this.files[0],this)
        });
    /****************************************/

    //$(".action_delete").click(function(){
    $(document).on("click",".action_delete",function() {
        if(!confirm("Do you really want to delete?")){return false;}
        var self = $(this);

        var datGet = $(this).data('get');
        var datPage = $(this).data('page');
        var url = "";
        if(typeof datPage != "undefined" && datPage != ""){
            url = "/"+datGet+"/"+datPage+"/delete/";
        }else{
            url = "/"+datGet+"/delete/";
        }

        var id = $(this).data('id');
        var body = "id="+id+"";
        requestPost(url,body,function(){
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(!result.error){
                    self.parent('td').parent('tr').fadeOut();
                }else{

                }
            }
        })
    });

    // function openSub(e){
    //     $('.menu_b_row_content').removeClass('active_menu_b_row_content');
    //     $('.menu_cilds').removeClass('active_menu_cilds');
    //     console.log('e',e)
    // }

    // $('.menu_b_row').click(function () {
    //     $('.menu_b_row_content').removeClass('active_menu_b_row_content');
    //     $('.menu_cilds').removeClass('active_menu_cilds');
    //     var allrows = $('.menu_b_row');
    //     var rowsWithChild = [];
    //     for(var i = 0; i < allrows.length; i++){
    //         if($(allrows[i]).find('div.menu_cilds').length !== 0){
    //             rowsWithChild.push($(allrows[i]));
    //         }
    //     }
    //     console.log('rowsWithChild',rowsWithChild)
    // })

    $('.h_username').click(function () {
        if($(this).next('.active_h_userlist').length > 0){
            $(this).next('.h_userlist').removeClass('active_h_userlist');
        }else{
            $('.h_userlist').removeClass('active_h_userlist');
            $(this).next('.h_userlist').addClass('active_h_userlist');
        }
    });


    class MyUploadAdapter {
        constructor( loader ) {
            // CKEditor 5's FileLoader instance.
            this.loader = loader;

            // URL where to send files.
            this.url = '/news/area-images/add';
        }

        // Starts the upload process.
        upload() {
            return new Promise( ( resolve, reject ) => {
                this._initRequest();
                this._initListeners( resolve, reject );
                this._sendRequest();
            } );
        }

        // Aborts the upload process.
        abort() {
            if ( this.xhr ) {
                this.xhr.abort();
            }
        }

        // Example implementation using XMLHttpRequest.
        _initRequest() {
            const xhr = this.xhr = new XMLHttpRequest();

            xhr.open( 'POST', this.url, true );
            xhr.responseType = 'json';
        }

        // Initializes XMLHttpRequest listeners.
        _initListeners( resolve, reject ) {
            const xhr = this.xhr;
            const loader = this.loader;
            const genericErrorText = 'Couldn\'t upload file:' + ` ${ loader.file.name }.`;

            xhr.addEventListener( 'error', () => reject( genericErrorText ) );
            xhr.addEventListener( 'abort', () => reject() );
            xhr.addEventListener( 'load', () => {
                const response = xhr.response;

                if ( !response || response.error ) {
                    return reject( response && response.error ? response.error.message : genericErrorText );
                }

                // If the upload is successful, resolve the upload promise with an object containing
                // at least the "default" URL, pointing to the image on the server.
                resolve( {
                    default: response.url
                } );
            } );

            if ( xhr.upload ) {
                xhr.upload.addEventListener( 'progress', evt => {
                    if ( evt.lengthComputable ) {
                        loader.uploadTotal = evt.total;
                        loader.uploaded = evt.loaded;
                    }
                } );
            }
            console.log('xhr.upload',xhr.upload)
        }

        // Prepares the data and sends the request.
        _sendRequest() {
            const data = new FormData();
            console.log(' this.loader.file', this.loader.file)
            data.append( 'upload', this.loader.file );

            this.xhr.send( data );
        }
    }

    function MyCustomUploadAdapterPlugin( editor ) {
        editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
            return new MyUploadAdapter( loader );
        };
    }


// CK EDITOR
//     ClassicEditor
//         .create( document.querySelector( '.__text_1' ), {
//             extraPlugins: [ MyCustomUploadAdapterPlugin ],
//             allowedContent:true,
//             toolbar: {
//                 items: [
//                     'heading',
//                     '|',
//                     'bold',
//                     'italic',
//                     'link',
//                     'bulletedList',
//                     'numberedList',
//                     '|',
//                     'indent',
//                     'outdent',
//                     '|',
//                     'imageUpload',
//                     'blockQuote',
//                     'insertTable',
//                     'mediaEmbed',
//                     'undo',
//                     'redo'
//                 ]
//             },
//             language: 'en',
//             image: {
//                 toolbar: [
//                     'imageTextAlternative',
//                     'imageStyle:full',
//                     'imageStyle:side'
//                 ]
//             },
//             table: {
//                 contentToolbar: [
//                     'tableColumn',
//                     'tableRow',
//                     'mergeTableCells'
//                 ]
//             },
//             licenseKey: '',
//
//         } )
//         .then( editor => {
//             window.editor_1 = editor;
//         } )
//     ClassicEditor
//         .create( document.querySelector( '.__text_2' ), {
//             extraPlugins: [ MyCustomUploadAdapterPlugin ],
//             allowedContent:true,
//             toolbar: {
//                 items: [
//                     'heading',
//                     '|',
//                     'bold',
//                     'italic',
//                     'link',
//                     'bulletedList',
//                     'numberedList',
//                     '|',
//                     'indent',
//                     'outdent',
//                     '|',
//                     'imageUpload',
//                     'blockQuote',
//                     'insertTable',
//                     'mediaEmbed',
//                     'undo',
//                     'redo'
//                 ]
//             },
//             language: 'en',
//             image: {
//                 toolbar: [
//                     'imageTextAlternative',
//                     'imageStyle:full',
//                     'imageStyle:side'
//                 ]
//             },
//             table: {
//                 contentToolbar: [
//                     'tableColumn',
//                     'tableRow',
//                     'mergeTableCells'
//                 ]
//             },
//             licenseKey: '',
//
//         } )
//         .then( editor => {
//             window.editor_2 = editor;
//         } )
//
//         .catch( error => {
//             console.error( 'Oops, something went wrong!' );
//             console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
//             console.warn( 'Build id: k2i30chx32nf-8o65j7c6blw0' );
//             console.error( error );
//         } );
//

});



function openPassModal(){
    document.getElementById('modal_c_password').style.display = 'flex';
}

function autoHeightAnimate(element, time) {
    if (element.is(':animated')) {
        return false;
    }
    var curHeight = element.height(),
    autoHeight = element.css('height', 'auto').height();
    element.height(curHeight);
    element.stop().animate({height: autoHeight}, parseInt(time));
}



function sendPostAjax(url,params,callback){
    $.ajax({
        type: "POST",
        url: url,
        data: {params: params},
        dataType: "json",
        success: function (result) {
           callback(result);
        },
    });
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
function deleteCookie(name) {
    document.cookie = name + "=" + "; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/";
}

function setCook(name,value){
    document.cookie = name + "=" +value;
}

function requestPost(url,body,callback){
    var oAjaxReq = new XMLHttpRequest();
    oAjaxReq.open("post", url, true);
    oAjaxReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    oAjaxReq.send(body);
    oAjaxReq.onreadystatechange = callback;
}
