var querys = require('../model/dbQuerys/querys')

class Update {
    constructor() {
        this.table = "";
        this.updateObject = {};
        this.whereObject = {};
        this.result = {
            error:false,
            errorMsg : ""
        }
        this.data = null;
    }

    setUpdateObject(object) {
        let updateVal = {};
        Object.entries(object).forEach(([key, value]) => {
            if(value !== "") {
                updateVal[key] = value
            }else{
                updateVal[key] = null
            }
        });
        this.updateObject = updateVal
        return true
    }

    setWhereObject(object){
        let updateVal = {};
        Object.entries(object).forEach(([key, value]) => {
            if(value !== "")
                updateVal[key] = value
        });
        this.whereObject = updateVal
        return true
    }

    setTable(table) {
        this.table = table
        return true
    }

    async update() {
        await querys.update2vPromiseG(this.table, {where: this.whereObject, values: this.updateObject})
        // if (typeof data === "undefined") {
        //     this.result.error = true
        // }
        // return this.result
    }
}

module.exports = Update;