var statics = require('./static');
var query = require('./model/dbQuerys/querys')

const getNewOrdersCount = function(req, res, next) {
    query.getAllNewOrdersCount()
        .then(function (data) {
            req.siteData.newOrdersCount = data
            next();
        })
        .catch(function (error) {
            req.siteData.newOrdersCount = 0
            console.log('error',error)
            next();
        })
}

module.exports.getNewOrdersCount = getNewOrdersCount;
