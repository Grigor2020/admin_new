const db = require('../../connection');
var log = require("../../../logs/log");
var staticMethods = require('../../../model/staticMethods')
var statics = require('../../../static')

var findOrders = function(page){
    var pagination = staticMethods.detectPagination(page);
    console.log('pagination',pagination)
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `orders`.`id`,`orders`.`order_status` as order_status,`orders`.`admin_view`,`orders`.`total_price`,`orders`.`insert_data`,`orders`.`payment_type`,`orders`.`public_id`,' +
            ' `order_payment`.`status` as payment_status ' +
            ' FROM `orders` ' +
            ' JOIN `order_payment` ON `order_payment`.`order_id` = `orders`.`id` ' +
            ' ORDER BY `orders`.`id` DESC LIMIT '+pagination.start+','+pagination.limit+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var findOrderById = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `orders`.`id`,`orders`.`order_status` as order_status,`orders`.`admin_view`,`orders`.`user_id`,`orders`.`guest_id`,`orders`.`total_price`,`orders`.`insert_data`,`orders`.`payment_type`,`orders`.`public_id`,`orders`.`private`,' +
            ' `orders`.`comment`,`orders`.`delivery_date`,`orders`.`delivery_time`,`orders`.`insert_data`,`orders`.`public_id`,' +
            ' `order_info`.`name` as buyerName,`order_info`.`last_name` as buyerLastName,`order_info`.`email` as buyerEmail,`order_info`.`phone` as buyerPhone,`order_info`.`address` as buyerAddress,' +
            ' `cities_lang`.`name` as cityName,' +
            ' `users_db`.`name` as userName,`users_db`.`last_name` as userLastName,`users_db`.`email` as userEmail,`users_db`.`phone` as userPhone,' +
            ' `order_payment`.`status` as payment_status,`order_payment`.`payment_id` as bankPaymentID,`order_payment`.`paymentInitResponse` as bankPaymentInitResponse,`order_payment`.`paymentDetailResponse` as bankPaymentDetailResponse ' +
            ' FROM `orders` ' +
            ' JOIN `order_info` ON `order_info`.`order_id` = `orders`.`id` ' +
            ' JOIN `cities_lang` ON `order_info`.`cities_id` = `cities_lang`.`cities_id` AND `cities_lang`.`lang_id` = 1 ' +
            ' JOIN `order_payment` ON `order_payment`.`order_id` = `orders`.`id` ' +
            ' LEFT JOIN `users_db` ON `users_db`.`id` = `orders`.`user_id` ' +
            ' WHERE `orders`.`id` = '+id+'';
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                console.log('111111111111')
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var findOrderProducts = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `order_product`.`quantity`,' +
            ' `product`.`id` as prodId,`product`.`price` as price,`product`.`price_before_sale` as priceBeforeSale,`product`.`sale_percent` as salePercent, ' +
            ' `product`.`in_stock` as prodInStock, ' +
            ' `litrages`.`name` as litrage, ' +
            ' `product_language`.`name` as name, ' +
            " CONCAT('"+statics.API_URL+"','/images/product/',`product`.`image`) as prodImage" +
            ' FROM `order_product` ' +
            ' LEFT JOIN `product` ON `product`.`id` = `order_product`.`product_id` ' +
            ' LEFT JOIN `litrages` ON `product`.`litrage_id` = `litrages`.`id` ' +
            ' LEFT JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = 1 ' +
            ' WHERE `order_product`.`order_id` = '+id+'';
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                console.log('22222222222222')
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var findOrdersCount = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT  COUNT(`orders`.`id`) as ordersRow " +
            " FROM `orders`";
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows[0].ordersRow)
            }
        })
    })
}

module.exports.findOrders = findOrders;
module.exports.findOrdersCount = findOrdersCount;
module.exports.findOrderById = findOrderById;
module.exports.findOrderProducts = findOrderProducts;