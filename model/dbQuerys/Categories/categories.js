const db = require('../../connection');
var log = require("../../../logs/log");
let statics = require('../../../static');

var getAllCategories = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `categories`.`id`,' +
            ' CONCAT("'+statics.API_URL+'","/images/categories/",`categories`.`image`) as image,' +
            ' CONCAT("'+statics.API_URL+'","/images/categories/",`categories`.`effect_image`) as effect_image,' +
            ' `categories_language`.`name`,`categories_language`.`lang_id`' +
            ' FROM `categories` ' +
            ' JOIN `categories_language` ON `categories_language`.`cat_id` = `categories`.`id` AND `categories_language`.`lang_id` = "2"' +
            ' ORDER BY `categories`.`ord` ASC';
        // console.log('....................prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var getCategorieById = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `categories`.`id`,' +
            ' CONCAT("'+statics.API_URL+'","/images/categories/",`categories`.`image`) as image,' +
            ' CONCAT("'+statics.API_URL+'","/images/categories/",`categories`.`effect_image`) as effect_image,' +
            ' GROUP_CONCAT(DISTINCT `categories_language`.`lang_id` SEPARATOR "||") AS lang_id, ' +
            ' GROUP_CONCAT(DISTINCT `categories_language`.`name` SEPARATOR "||") AS name ' +
            ' FROM `categories` ' +
            ' JOIN `categories_language` ON `categories_language`.`cat_id` = `categories`.`id`' +
            ' WHERE `categories`.`id` = '+id+'';
        // console.log('....................prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var getAllCategoriesByLangId = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `categories`.`id`,`categories_language`.`name`' +
            ' FROM `categories` ' +
            ' JOIN `categories_language` ON `categories_language`.`cat_id` = `categories`.`id` AND `categories_language`.`lang_id` = "'+id+'"';
        // console.log('....................prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


module.exports.getAllCategories = getAllCategories;
module.exports.getAllCategoriesByLangId = getAllCategoriesByLangId;
module.exports.getCategorieById = getCategorieById;