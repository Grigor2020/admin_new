const db = require('../../connection');
var log = require("../../../logs/log");
var static = require("../../../static");


var findAllSlides = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT ' +
            ' `home_slide`.`image` as slideImage,`home_slide`.`id` as slideId, ' +
            " GROUP_CONCAT(DISTINCT `home_slide_language`.`lang_id` SEPARATOR '||') AS slideLangId, " +
            " GROUP_CONCAT(DISTINCT `home_slide_language`.`title` SEPARATOR '||') AS slideTitle " +
            ' FROM `home_slide` ' +
            ' JOIN `home_slide_language` ON `home_slide_language`.`home_slide_id` = `home_slide`.`id` ' +
            ' GROUP BY `home_slide`.`id` ORDER BY `home_slide`.`ord` ASC';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var findAllLoginSlides = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
        'SELECT ' +
        ' `login_slide`.`image` as image,`login_slide`.`id` as id, ' +
        " GROUP_CONCAT(DISTINCT `login_slide_language`.`lang_id` SEPARATOR '||') AS langId, " +
        " GROUP_CONCAT(DISTINCT `login_slide_language`.`name` SEPARATOR '||') AS name " +
        ' FROM `login_slide` ' +
        ' JOIN `login_slide_language` ON `login_slide_language`.`login_slide_id` = `login_slide`.`id` ' +
        ' GROUP BY `login_slide`.`id` ORDER BY `login_slide`.`ord` ASC';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var findSlideById = function(slideId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT ' +
            ' `home_slide`.`image` as slideImage,`home_slide`.`id` as slideId,`home_slide`.`url` as url, ' +
            " GROUP_CONCAT(DISTINCT `home_slide_language`.`lang_id` SEPARATOR '||') AS slideLangId, " +
            " GROUP_CONCAT(DISTINCT `home_slide_language`.`title` SEPARATOR '||') AS slideTitle, " +
            " GROUP_CONCAT(DISTINCT `home_slide_language`.`button_text` SEPARATOR '||') AS button_text " +
            ' FROM `home_slide` ' +
            ' JOIN `home_slide_language` ON `home_slide_language`.`home_slide_id` = `home_slide`.`id` ' +
            ' WHERE `home_slide`.`id` = '+db.escape(slideId)+' GROUP BY `home_slide`.`id` ';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var findLoginSlides = function(slideId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT ' +
            ' `login_slide`.`id` as id,`login_slide_language`.`login_slide_id`, ' +
            " CONCAT('"+static.API_URL+"','/images/home/slide/',`login_slide`.`image`) as image,"+
            " GROUP_CONCAT(DISTINCT `login_slide_language`.`lang_id` ORDER BY `login_slide_language`.`id` ASC SEPARATOR '||') AS lang_id, " +
            " GROUP_CONCAT(DISTINCT `login_slide_language`.`name`  ORDER BY `login_slide_language`.`id` ASC SEPARATOR '||') AS name " +
            ' FROM `login_slide` ' +
            ' JOIN `login_slide_language` ON `login_slide_language`.`login_slide_id` = `login_slide`.`id` '+
            ' WHERE `login_slide`.`id` = '+db.escape(slideId);
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

module.exports.findAllSlides = findAllSlides;
module.exports.findSlideById = findSlideById;
module.exports.findAllLoginSlides = findAllLoginSlides;
module.exports.findLoginSlides = findLoginSlides;
