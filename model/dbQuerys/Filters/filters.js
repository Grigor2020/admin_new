const db = require('../../connection');
var log = require("../../../logs/log");


var getAllFilters = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `filter`.`id`,' +
            ' GROUP_CONCAT(DISTINCT `filter_language`.`name` ORDER BY `filter_language`.`id`  SEPARATOR "||") AS name, ' +
            ' GROUP_CONCAT(DISTINCT `filter_language`.`lang_id` ORDER BY `filter_language`.`id`  SEPARATOR "||") AS lang_id ' +
            ' FROM `filter` ' +
            ' JOIN `filter_language` ON `filter_language`.`filter_id` = `filter`.`id`' +
            ' GROUP BY `filter`.`id` ORDER BY `filter`.`ord` ASC';
        console.log('....................prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var getAllFiltersByLangId = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `filter`.`id`,`filter_language`.`name`' +
            ' FROM `filter` ' +
            ' JOIN `filter_language` ON `filter_language`.`filter_id` = `filter`.`id` AND `filter_language`.`lang_id` = "'+id+'"';
        // console.log('....................prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var getAllFilterProperties = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `filter_prop`.`id`,`filter_prop`.`filter_id`,' +
            ' GROUP_CONCAT(DISTINCT `filter_prop_language`.`name` SEPARATOR "||") AS name, ' +
            ' GROUP_CONCAT(DISTINCT `filter_prop_language`.`lang_id` SEPARATOR "||") AS lang_id ' +
            ' FROM `filter_prop` ' +
            ' JOIN `filter_prop_language` ON `filter_prop_language`.`filter_prop_id` = `filter_prop`.`id`' +
            ' WHERE `filter_prop`.`filter_id` = "'+id+'"' +
            ' GROUP BY `filter_prop`.`id`' +
            ' ORDER BY `filter_prop`.`ord` ASC ';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var getAllProperties = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `filter_prop`.`id`,`filter_prop`.`filter_id`,`filter_prop_language`.`name`' +
            ' FROM `filter_prop` ' +
            ' JOIN `filter_prop_language` ON `filter_prop_language`.`filter_prop_id` = `filter_prop`.`id` AND `filter_prop_language`.`lang_id` = "2"';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var getFilterInfo = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `filter`.`id`,`filter_language`.`name` ' +
            ' FROM `filter` ' +
            ' JOIN `filter_language` ON `filter_language`.`filter_id` = `filter`.`id` AND `filter_language`.`lang_id` = "2"' +
            ' WHERE `filter`.`id` = "'+id+'"';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var getAllAdditionalProperties = function(productId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `additional_filters_language`.`id`, `additional_filters_language`.`product_id`, ' +
            ' GROUP_CONCAT(DISTINCT `additional_filters_language`.`group_row_id` SEPARATOR "||") AS group_row_id, ' +
            ' GROUP_CONCAT(DISTINCT `additional_filters_language`.`name` SEPARATOR "||") AS name, ' +
            ' GROUP_CONCAT(DISTINCT `additional_filters_language`.`val` SEPARATOR "||") AS val, ' +
            ' GROUP_CONCAT(DISTINCT `additional_filters_language`.`lang_id` SEPARATOR "||") AS lang_id ' +
            ' FROM `additional_filters_language` ' +
            ' WHERE `additional_filters_language`.`product_id` = "'+productId+'" GROUP BY `additional_filters_language`.`id`';
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

module.exports.getAllFilterProperties = getAllFilterProperties;
module.exports.getAllFilters = getAllFilters;
module.exports.getFilterInfo = getFilterInfo;
module.exports.getAllProperties = getAllProperties;
module.exports.getAllFiltersByLangId = getAllFiltersByLangId;
module.exports.getAllAdditionalProperties = getAllAdditionalProperties;