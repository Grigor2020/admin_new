const db = require('../connection');
var log = require("../../logs/log");

var adminLogin = function (params,cb) {
    let prepareSql = "SELECT * FROM admin_login WHERE `log` = '"+ params.login +"' AND `pass` = '"+ params.password +"'";
    db.query(prepareSql, function (err, rows) {
        if (err) throw err;
        cb(rows)
    })
};

/*
 *   findAll => order { null || ( ASC || DESC )}
 */
var findAll = function(table,order,cb){
    let prepareSql = '';
    if(order == null){
        prepareSql = 'SELECT * FROM '+table+'';
    }else{
        prepareSql = 'SELECT * FROM '+table+' ORDER BY id '+ order +'';
    }
    db.query(prepareSql,function (err, rows) {
        if (err) throw err;
        cb(rows)
    });
};


var checkUser = function (token) {
    return new Promise(function (resolve, reject) {
        let prepareSql = "SELECT * FROM `admin_login` WHERE `token` = '"+token+"'";
        db.query(prepareSql, function (error, rows) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
};


/*
 *   findAll => order { null || ( ASC || DESC )}
 */
var findAllSortByOrd = function(table,type_id,order,cb){
    let prepareSql = '';

    let orderString = '';
    if(order == null){
        orderString = '';
    }else{
        orderString = 'ORDER BY ord '+ order +'';
    }
    prepareSql = 'SELECT '+table+'.* , `show_info`.`post_id`, `show_info`.`type_id` , `show_info`.`id` as sid ' +
        ' FROM `'+table+'` ' +
        ' LEFT JOIN `show_info` ON `show_info`.`post_id` = `'+ table+'`.`id` AND `show_info`.`type_id` = '+type_id+' '+
        ' '+orderString;
    db.query(prepareSql,function (err, rows, fields) {
        if (err == null) {
            cb(rows)
        }else{
            cb('9999')
        }

    });
};

var findById = function(table,id,cb) {
    var prepareSql = "SELECT * FROM " + table + " WHERE `id`=" + db.escape(id);
    db.query(prepareSql, function (err, rows, fields) {
        if (err) throw err;
        cb(rows)
    })
}

var insert2v = function(table,post,cb){
    var prepareSql = 'INSERT INTO '+table+' SET ?';
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}


/**
 *
 * @param table
 * @param post {
 *              values: {fildname:value,.....}
 *              where: {fildname:value,.....}
 *          }
 * @param cb
 */

var update = function(table,post,cb){
    var val = [];
    for(var key in post.values){
        val.push("`"+key +"`="+db.escape(post.values[key]));
    }
    var whereVal = [];
    for(var key in post.where){
        whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
    }
    var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

var update2v = function(table,post,cb){
    var val = [];
    for(var key in post.values){
        val.push("`"+key +"`="+db.escape(post.values[key]));
    }
    var whereVal = [];
    for(var key in post.where){
        whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
    }

    var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
    var query = db.query(prepareSql, function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

var update2vPromise = function(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for (var key in post.values) {
            val.push("`" + key + "`=" + db.escape(post.values[key]));
        }
        var whereVal = [];
        for (var key in post.where) {
            whereVal.push("`" + key + "`" + "=" + db.escape(post.where[key]));
        }
        var prepareSql = "UPDATE " + table + " SET " + val.toString() + " WHERE " + whereVal.join(' AND ') + "";
        console.log('prepareSql',prepareSql)
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "update2vPromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(results)
            } else {
                resolve(results)
            }
        });
    })
}

var update2vPromiseG = async function(table,post){
    var val = [];
    for (var key in post.values) {
        val.push("`" + key + "`=" + db.escape(post.values[key]));
    }
    var whereVal = [];
    for (var key in post.where) {
        whereVal.push("`" + key + "`" + "=" + db.escape(post.where[key]));
    }
    var prepareSql = "UPDATE " + table + " SET " + val.toString() + " WHERE " + whereVal.join(' AND ') + "";
    console.log('prepareSql',prepareSql)
    await db.query(prepareSql, post, function (error, results, fields) {
        console.log('error',error)
        if (error) {
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "update2vPromise",
                    table: table,
                    params: post,
                    date: new Date()
                }
            }
            console.log('errorData',errorData)

            log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
            return errorData
        } else {
            console.log('results',results)
            return results
        }
    });
}

var updateTerms = function(langId,text){
    return new Promise(function (resolve, reject) {

        // var prepareSql = "UPDATE " + table + " SET " + val.toString() + " WHERE " + whereVal.join(' AND ') + "";
        var query = db.query("UPDATE terms SET text = ? WHERE lang_id = ?", [ text, langId], function(err, results, fields) {

        // var query = db.query(prepareSql, post, function (error, results, fields) {
            if (err) {
                var errorData = {
                    code: err.code,
                    errno: err.errno,
                    sqlMessage: err.sqlMessage,
                    devData: {
                        queryFunction: "updateTerms",
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(results)
            } else {
                resolve(results)
            }
        });
    })
}

var updateCookies = function(langId,text){
    return new Promise(function (resolve, reject) {

        // var prepareSql = "UPDATE " + table + " SET " + val.toString() + " WHERE " + whereVal.join(' AND ') + "";
        var query = db.query("UPDATE cookie SET text = ? WHERE lang_id = ?", [ text, langId], function(err, results, fields) {

        // var query = db.query(prepareSql, post, function (error, results, fields) {
            if (err) {
                var errorData = {
                    code: err.code,
                    errno: err.errno,
                    sqlMessage: err.sqlMessage,
                    devData: {
                        queryFunction: "updateTerms",
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(results)
            } else {
                resolve(results)
            }
        });
    })
}

var updatePrivacy = function(langId,text){
    return new Promise(function (resolve, reject) {

        // var prepareSql = "UPDATE " + table + " SET " + val.toString() + " WHERE " + whereVal.join(' AND ') + "";
        var query = db.query("UPDATE privacy SET text = ? WHERE lang_id = ?", [ text, langId], function(err, results, fields) {

        // var query = db.query(prepareSql, post, function (error, results, fields) {
            if (err) {
                var errorData = {
                    code: err.code,
                    errno: err.errno,
                    sqlMessage: err.sqlMessage,
                    devData: {
                        queryFunction: "updateTerms",
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(results)
            } else {
                resolve(results)
            }
        });
    })
}
var updateStaticsV3Promise = function(table,langId,text){
    return new Promise(function (resolve, reject) {

        // var prepareSql = "UPDATE " + table + " SET " + val.toString() + " WHERE " + whereVal.join(' AND ') + "";
        var query = db.query("UPDATE "+table+" SET text = ? WHERE lang_id = ?", [ text, langId], function(err, results, fields) {

        // var query = db.query(prepareSql, post, function (error, results, fields) {
            if (err) {
                var errorData = {
                    code: err.code,
                    errno: err.errno,
                    sqlMessage: err.sqlMessage,
                    devData: {
                        queryFunction: "updateStaticsV3Promise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(results)
            } else {
                resolve(results)
            }
        });
    })
}

var updateV3Promise = function(table,post){
    return new Promise(function (resolve, reject) {
        console.log('post',post)
        var filds = [];
        var val = [];
        for(var key in post.values){
            val.push(key+" = ?");
            filds.push(post.values[key]+"")
        }
        var whereVal = [];
        for(var key in post.where){
            whereVal.push(key+" = ?");
            filds.push(post.where[key])
        }
        var prepareSql = "UPDATE "+table+" SET "+val.join(",")+" WHERE "+whereVal.join(" AND ");
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, filds, function(err, results, fields) {
            console.log('err',err)
            if (err) {
                var errorData = {
                    code: err.code,
                    errno: err.errno,
                    sqlMessage: err.sqlMessage,
                    devData: {
                        queryFunction: "updateStaticsV3Promise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(results)
            } else {
                resolve(results)
            }
        });
    })
}

/**
 *
 * @param table
 * @param params {fild_name:value}
 * @param filds [fild1,fild2]
 * @param cb function
 */
var findByMultyName2v = function (table,params,filds,cb) {
    var whereParams = [];
    for(var key in params){
        if(params[key] !== "NULL") {
            whereParams.push("`" + key + "`=" + db.escape(params[key]));
        }else{
            whereParams.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';
    db.query(prepareSql, function (err, rows, fields) {
        if (err){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:err.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,rows)
        }
    });
}

var findByMultyNamePromise = function (table,params,filds) {
    return new Promise(function (resolve, reject) {
        var whereParams = [];
        for (var key in params) {
            if (params[key] !== "NULL") {
                whereParams.push("`" + key + "`=" + db.escape(params[key]));
            } else {
                whereParams.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE ' + whereParams.join(' AND ') + '';
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        });
    });
}
var getDepositForUpdate = function (table,deposit_id,filds) {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `'+table+'`.*,' +
            '`users_db`.`firstName`,`users_db`.`lastName`,`users_db`.`username`,`users_db`.`email`,`users_db`.`e_verify` ' +
            'FROM `'+table +'` ' +
            'JOIN `users_db` ON `users_db`.`id` = `'+table+'`.`user_id` ' +
            'WHERE `'+table+'`.`deposit_id` = '+db.escape(deposit_id)+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        });
    });
}
var findUserPermissions = function (userId) {
    return new Promise(function (resolve, reject) {

        var prepareSql = 'SELECT `places_users_permissions`.`permissions_id`,`places_users_permissions`.`places_users_id` as user_id,' +
            ' `places_users_permissions`.`is_open`,`places_users_permissions`.`read_or_write`,' +
            ' `permissions`.`name`,`permissions`.`display_name`' +
            ' FROM `places_users_permissions`' +
            ' JOIN `permissions` ON `permissions`.`id` = `places_users_permissions`.`permissions_id`' +
            ' WHERE `places_users_permissions`.`places_users_id` = '+db.escape(userId)+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        });
    });
};

var findByUrl = function (table,url,filds,cb) {

    var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE `url_path` LIKE "'+url+'%"';
    db.query(prepareSql, function (err, rows, fields) {
        if (err){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:err.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,rows)
        }
    });
}


/**
 *
 * @param table
 * @param post {fildname:value}
 * @param cb function(){}
 */
var deletes = function(table,post,cb){
    var val = [];
    for(var key in post){
        if(post[key] !== "NULL") {
            val.push("`" + key + "`=" + db.escape(post[key]));
        }else{
            val.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'DELETE FROM ' + table + ' WHERE '+val.join(' AND ')+'';
    db.query(prepareSql,post,function(error,result,fields){
        if (error) throw error;
        cb(result)
    });
}

var findAllPromise = function(table,order){
    return new Promise(function (resolve, reject) {
        var prepareSql = '';
        if(order == null){
            prepareSql = 'SELECT * FROM '+table+'';
        }else{
            prepareSql = 'SELECT * FROM '+table+' ORDER BY id '+ order +'';
        }
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var findAllFaq = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `faq_lang`.`faq_id`,`faq_lang`.`question` ' +
            ' FROM `faq_lang` ' +
            ' WHERE `faq_lang`.`lang_id` = "1"';
        console.log('findAllFaq prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var findAllGames = function(order){
    return new Promise(function (resolve, reject) {
        var orderString = "";
        if(order != null){
            orderString = ' ORDER BY id '+ order +'';
        }
        // var prepareSql = '' +
        //     'SELECT `games`.`image`,`games`.`url`,' +
        //     'GROUP_CONCAT(DISTINCT `games_language`.`name` SEPARATOR "||") AS name, ' +
        //     'GROUP_CONCAT(DISTINCT `games_language`.`lang_id` SEPARATOR "||") AS lang_id ' +
        //     'FROM `games`' +
        //     'JOIN `games_language` ON  `games_language`.`games_id` = `games`.`id`' +
        //     orderString +' ' +
        //     'GROUP BY `games`.`id`';
        var prepareSql = '' +
            'SELECT `games`.`image`,`games`.`id`,`games`.`url`,`games_language`.`name`,`games_language`.`lang_id`' +
            'FROM `games`' +
            'JOIN `games_language` ON  `games_language`.`games_id` = `games`.`id` AND `games_language`.`lang_id` = "1"' +
            orderString;
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var findAllModes = function(order){
    return new Promise(function (resolve, reject) {
        var orderString = "";
        if(order != null){
            orderString = ' ORDER BY id '+ order +'';
        }

        var prepareSql = '' +
            'SELECT `game_mode`.`name`,`game_mode`.`game_id`,`game_mode`.`user_id`,`game_mode`.`from_admin`, ' +
            '`games_language`.`name` as game_name ' +
            'FROM `game_mode` ' +
            'JOIN `games_language` ON  `games_language`.`games_id` = `game_mode`.`game_id` AND `games_language`.`lang_id` = "1" ' +
            orderString;
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var getAllNewOrdersCount = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT COUNT(`orders`.`id`) as newOrders ' +
            'FROM `orders` ' +
            ' WHERE ISNULL(`orders`.`admin_view`)';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows[0].newOrders)
            }
        })
    })
}

var getSoltItemsCount = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT SUM(`order_product`.`quantity`) as quantity ' +
            'FROM `order_product` ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows[0].quantity)
            }
        })
    })
}

var getAllProdCount = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT COUNT(`product`.`id`) as prodQuantity ' +
            'FROM `product` ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows[0].prodQuantity)
            }
        })
    })
}
var getAllUsersCount = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT COUNT(`users_db`.`id`) as usersQuantity ' +
            'FROM `users_db` ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows[0].usersQuantity)
            }
        })
    })
}
var getGameModes = function(gameId){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `game_mode`.`name`,`game_mode`.`id` as game_mode_id ' +
            'FROM `game_mode` ' +
            'WHERE `game_mode`.`game_id` = '+gameId+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var findAllTournaments = function(){
    return new Promise(function (resolve, reject) {

        var prepareSql = '' +
            'SELECT `tournament`.`id`,`tournament`.`name`,`tournament`.`game_id`,' +
            '`tournament`.`members_count`,`tournament`.`entry`,`tournament`.`prize`, ' +
            '`tournament`.`start_datetime`,`tournament`.`check_in_datetime`,' +
            '`games_language`.`name` as game_name ' +
            'FROM `tournament` ' +
            'JOIN `games_language` ON `games_language`.`games_id` = `tournament`.`game_id` AND `games_language`.`lang_id` = "1" ';
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var findAllServers = function(order){
    return new Promise(function (resolve, reject) {
        var orderString = "";
        if(order != null){
            orderString = ' ORDER BY id '+ order +'';
        }
        var prepareSql = '' +
            'SELECT `game_server`.`name`,`game_server`.`game_id`, ' +
            '`games_language`.`name` as game_name ' +
            'FROM `game_server` ' +
            'JOIN `games_language` ON  `games_language`.`games_id` = `game_server`.`game_id` AND `games_language`.`lang_id` = "1" ' +
            orderString;
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

// var getPlaceMaps = function(place_id){
//     return new Promise(function (resolve, reject) {
//         var prepareSql = '' +
//             'SELECT `map`.`name` as map_name,`map_nstatex_group_price`.`name` as group_name,`map_nstatex_group_price`.`color` as group_color ' +
//             'FROM `map` ' +
//             'JOIN `map_nstatex_group_price` ON `map_nstatex_group_price`.`map_id` = `map`.`id` ' +
//             'WHERE `map`.`places_users_id` ='+db.escape(place_id)+''
//         ;
//
//         db.query(prepareSql, function (error, rows, fields) {
//             if (error){
//                 reject(error)
//             }else{
//                 resolve(rows)
//             }
//         })
//     })
// }

var findAllByLangPromise = function(table,order,lang_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '';
        if(order == null){
            prepareSql = 'SELECT * FROM '+table+' WHERE `lang_id` = '+db.escape(lang_id)+'';
        }else{
            prepareSql = 'SELECT * FROM '+table+' WHERE `lang_id` = '+db.escape(lang_id)+' ORDER BY id '+ order +'';
        }

        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var insertLoopPromise = function(table,filds,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO ' + table + ' (' + filds.join(',') + ') VALUES  ?';
        var query = db.query(prepareSql, [post], function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insertLoopPromise",
                        table:table,
                        filds:filds,
                        params:post,
                        date:new Date()
                    }
                };
                reject(error)
            }else{
                resolve(results)
            }
        });
    })
}

function insert2vPromise(table,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO '+table+' SET ?';
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insert2vPromise",
                        table:table,
                        params:post,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/map.txt")
                reject(new Error("Sql error insert2vPromise"))
            }else{
                resolve(results)
            }
        });
    })
}


var findC2CDeposits = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT ' +
            '`deposit_c2c`.`card_number` as c2c_card_number,`deposit_c2c`.`card_number` as c2c_amount, ' +
            '`deposit_c2c`.`admin_check`,`deposit_c2c`.`admin_accept`,`deposit_c2c`.`type`,`deposit_c2c`.`deposit_id`, ' +
            ' `users_db`.`email` as user_email ' +
            'FROM `deposit_c2c` ' +
            'JOIN `users_db` ON `users_db`.`id` = `deposit_c2c`.`user_id` ' +
            'ORDER BY `deposit_c2c`.`id` DESC';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


var findBitcoinDeposits = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT ' +
            '`deposit_bitcoin`.`amount` as bitcoin_amount,`deposit_bitcoin`.`wallet_address` as bitcoin_wallet_address, ' +
            '`deposit_bitcoin`.`admin_check`,`deposit_bitcoin`.`admin_accept`,`deposit_bitcoin`.`type`,`deposit_bitcoin`.`deposit_id`,' +
            ' `users_db`.`email` as user_email ' +
            'FROM `deposit_bitcoin` ' +
            'JOIN `users_db` ON `users_db`.`id` = `deposit_bitcoin`.`user_id` ' +
            ' ORDER BY `deposit_bitcoin`.`id` DESC';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


var findPmDeposits = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT ' +
            '`deposit_pm`.`e_voucher`,`deposit_pm`.`activation_code`, ' +
            '`deposit_pm`.`admin_check`,`deposit_pm`.`admin_accept`, ' +
            '`deposit_pm`.`type`,`deposit_pm`.`type`,`deposit_pm`.`deposit_id`, ' +
            ' `users_db`.`email` as user_email ' +
            'FROM `deposit_pm` ' +
            'JOIN `users_db` ON `users_db`.`id` = `deposit_pm`.`user_id` ' +
            ' ORDER BY `deposit_pm`.`id` DESC';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


var findC2CNewCount = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT COUNT(id) as counts FROM `deposit_c2c` WHERE `deposit_c2c`.`admin_check` = "0"';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var findBitcoinNewCount = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT COUNT(id) as counts FROM `deposit_bitcoin` WHERE `deposit_bitcoin`.`admin_check` = "0"';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var findPmNewCount = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT COUNT(id) as counts FROM `deposit_pm` WHERE `deposit_pm`.`admin_check` = "0"';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var findAllOurMoney = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT SUM(`transactions_history`.`money`) as summary_money ' +
            'FROM `transactions_history` ' +
            'WHERE `transactions_history`.`user_id` = "0"';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var findAllTransactionHistory = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `transactions_history`.* ' +
            'FROM `transactions_history` ' +
            'WHERE `transactions_history`.`user_id` = "0"';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var findTransactionHistory = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `transactions_history`.*, ' +
            'FROM `transactions_history` ' +
            'WHERE `transactions_history`.`user_id` = "0" AND `transactions_history`.`id` = '+id+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var findAllDispute = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `matches_dispute`.*, ' +
            '`users_db`.`firstName`,`users_db`.`lastName`,`users_db`.`username`,`users_db`.`email` ' +
            'FROM `matches_dispute` ' +
            'JOIN `users_db` ON `matches_dispute`.`user_id` = `users_db`.`id`';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var findDisputeById = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `matches_dispute`.*, ' +
            ' `users_db`.`firstName`,`users_db`.`lastName`,`users_db`.`username`,`users_db`.`email`,' +
            ' `matches`.`creator_id`,`matches`.`joiner_id`,`matches`.`creator_result`,`matches`.`joiner_result`,`matches`.`start_date` as match_start_day,' +
            ' `challenge`.`prize`, ' +
            ' `games_language`.`name` as game_name, ' +
            ' `game_mode`.`name` as game_mode_name, ' +
            ' `game_server`.`name` as game_server_name ' +
            ' FROM `matches_dispute` ' +
            ' JOIN `users_db` ON `matches_dispute`.`user_id` = `users_db`.`id` ' +
            ' JOIN `matches` ON `matches`.`id` = `matches_dispute`.`matches_id` ' +
            ' JOIN `challenge` ON `challenge`.`id` = `matches`.`challenge_id` ' +
            ' JOIN `games_language` ON `games_language`.`games_id` = `challenge`.`game_id` AND `games_language`.`lang_id` = "1" ' +
            ' JOIN `game_mode` ON `game_mode`.`id` = `challenge`.`mode_id` ' +
            ' LEFT JOIN `game_server` ON `game_server`.`id` = `challenge`.`server_id` ' +
            ' WHERE `matches_dispute`.`id` = '+id+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


var findWithdraws = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `withdraws`.*, ' +
            '`users_db`.`firstName`,`users_db`.`lastName`,`users_db`.`username`,`users_db`.`email` ' +
            'FROM `withdraws` ' +
            'JOIN `users_db` ON `withdraws`.`user_id` = `users_db`.`id` ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


var findOneWithdraw = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `withdraws`.*, ' +
            '`users_db`.`firstName`,`users_db`.`lastName`,`users_db`.`username`,`users_db`.`email`,`users_db`.`e_verify` ' +
            'FROM `withdraws` ' +
            'JOIN `users_db` ON `withdraws`.`user_id` = `users_db`.`id` ' +
            'WHERE `withdraws`.`id` = '+id+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var deletesPromise = function(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for (var key in post) {
            if (post[key] !== "NULL") {
                val.push("`" + key + "`=" + db.escape(post[key]));
            } else {
                val.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'DELETE FROM ' + table + ' WHERE ' + val.join(' AND ') + '';
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, post, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "deletesPromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}


var getAllTranslates = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `translates`.`id`,`translates`.`key_name`,' +
            ' GROUP_CONCAT(DISTINCT `translates_language`.`name` SEPARATOR "||") AS name, ' +
            ' GROUP_CONCAT(DISTINCT `translates_language`.`lang_id` SEPARATOR "||") AS lang_id ' +
            ' FROM `translates` ' +
            ' JOIN `translates_language` ON `translates_language`.`translates_id` = `translates`.`id` ' +
            ' GROUP BY `translates_language`.`translates_id`';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

module.exports.adminLogin = adminLogin;
module.exports.getAllTranslates = getAllTranslates;
module.exports.findAllTransactionHistory = findAllTransactionHistory;
module.exports.findAllOurMoney = findAllOurMoney;
module.exports.findTransactionHistory = findTransactionHistory;
module.exports.findWithdraws = findWithdraws;
module.exports.findDisputeById = findDisputeById;
module.exports.findAllDispute = findAllDispute;
module.exports.findOneWithdraw = findOneWithdraw;
module.exports.findC2CDeposits = findC2CDeposits;
module.exports.findC2CNewCount = findC2CNewCount;
module.exports.findBitcoinDeposits = findBitcoinDeposits;
module.exports.findBitcoinNewCount = findBitcoinNewCount;
module.exports.findPmDeposits = findPmDeposits;
module.exports.findPmNewCount = findPmNewCount;
module.exports.findAll = findAll;
module.exports.findAllPromise = findAllPromise;
module.exports.findAllFaq = findAllFaq;
module.exports.getGameModes = getGameModes;
module.exports.findAllGames = findAllGames;
module.exports.findAllModes = findAllModes;
module.exports.findAllTournaments = findAllTournaments;
module.exports.findAllServers = findAllServers;
module.exports.findAllByLangPromise = findAllByLangPromise;
module.exports.findById = findById;
module.exports.insert2v = insert2v;
module.exports.update = update;
module.exports.update2v = update2v;
module.exports.findByMultyName2v = findByMultyName2v;
module.exports.findByMultyNamePromise = findByMultyNamePromise;
module.exports.findByUrl = findByUrl;
module.exports.deletes = deletes;
module.exports.findAllSortByOrd = findAllSortByOrd;
module.exports.checkUser = checkUser;
module.exports.update2vPromise = update2vPromise;
module.exports.findUserPermissions = findUserPermissions;
module.exports.insertLoopPromise = insertLoopPromise;
module.exports.insert2vPromise = insert2vPromise;
module.exports.getDepositForUpdate = getDepositForUpdate;
module.exports.deletesPromise = deletesPromise;
module.exports.updateTerms = updateTerms;
module.exports.updateCookies = updateCookies;
module.exports.updatePrivacy = updatePrivacy;
module.exports.updateStaticsV3Promise = updateStaticsV3Promise;
module.exports.updateV3Promise = updateV3Promise;
module.exports.update2vPromiseG = update2vPromiseG;
module.exports.getAllNewOrdersCount = getAllNewOrdersCount;
module.exports.getSoltItemsCount = getSoltItemsCount;
module.exports.getAllProdCount = getAllProdCount;
module.exports.getAllUsersCount = getAllUsersCount;
