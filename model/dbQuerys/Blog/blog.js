const db = require('../../connection');
var log = require("../../../logs/log");
var static = require("../../../static");


var getAllBlog = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `blog`.`image`,`blog`.`id`,`blog_top`.`id` as blog_top_id,' +
            ' `blog_language`.`title` ' +
            ' FROM `blog` ' +
            ' JOIN `blog_language` ON `blog_language`.`blog_id` = `blog`.`id` AND `blog_language`.`lang_id` = "1"' +
            ' LEFT JOIN `blog_top` ON `blog_top`.`blog_id` = `blog`.`id` ' +
            ' ORDER BY `blog`.`ord` ASC ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var getBlog = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `blog`.`id`,' +
            " CONCAT('"+static.API_URL+"','/images/blog/',`blog`.`image`) as image," +
            ' GROUP_CONCAT(DISTINCT `blog_language`.`lang_id` ORDER BY `blog_language`.`id` ASC SEPARATOR "||") AS lang_id, ' +
            ' GROUP_CONCAT(DISTINCT `blog_language`.`title` ORDER BY `blog_language`.`id` SEPARATOR "||") AS title, ' +
            ' GROUP_CONCAT(DISTINCT `blog_language`.`text` ORDER BY `blog_language`.`id` SEPARATOR "||") AS text ' +
            ' FROM `blog` ' +
            ' JOIN `blog_language` ON `blog_language`.`blog_id` = `blog`.`id` ' +
            ' WHERE `blog`.`id` = '+db.escape(id)+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

module.exports.getAllBlog = getAllBlog;
module.exports.getBlog = getBlog;