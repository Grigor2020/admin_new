const db = require('../../connection');
var log = require("../../../logs/log");


var findAllLoyalities = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `loyality_status`.`id`,`loyality_status`.`from_money`,' +
            ' `loyality_status_language`.`name` ' +
            ' FROM `loyality_status` ' +
            ' JOIN `loyality_status_language` ON `loyality_status_language`.`loyality_status_id` = `loyality_status`.`id` AND `loyality_status_language`.`lang_id` = "2"';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var findLoyalitieById = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `loyality_status`.`id`,`loyality_status`.`from_money`,' +
            ' GROUP_CONCAT(DISTINCT `loyality_status_language`.`lang_id` SEPARATOR "||") AS lang_id, ' +
            ' GROUP_CONCAT(DISTINCT `loyality_status_language`.`name` SEPARATOR "||") AS name ' +
            ' FROM `loyality_status` ' +
            ' JOIN `loyality_status_language` ON `loyality_status_language`.`loyality_status_id` = `loyality_status`.`id` ' +
            ' WHERE `loyality_status`.`id` = '+db.escape(id)+'';
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

module.exports.findAllLoyalities = findAllLoyalities;
module.exports.findLoyalitieById = findLoyalitieById;
