const db = require('../../connection');
var log = require("../../../logs/log");


var getAllBrands = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `brand`.`brand_logo`,' +
            ' `brand_language`.`name` as brandName,`brand_language`.`brand_id` as brandId ' +
            ' FROM `brand` ' +
            ' JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` ' +
            ' WHERE `brand_language`.`lang_id` = "2" ' +
            ' ORDER BY `brand`.`ord` ASC ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var getBrand = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `brand`.`image`,`brand`.`brand_logo`,`brand`.`id`,`brand`.`cat_id`,' +
            ' GROUP_CONCAT(DISTINCT `brand_language`.`lang_id` ORDER BY `brand`.`id` DESC SEPARATOR "||") AS lang_id, ' +
            ' GROUP_CONCAT(DISTINCT `brand_language`.`title` ORDER BY `brand`.`id` DESC SEPARATOR "||") AS title, ' +
            ' GROUP_CONCAT(DISTINCT `brand_language`.`name` ORDER BY `brand`.`id` DESC SEPARATOR "||") AS name, ' +
            ' GROUP_CONCAT(DISTINCT `brand_language`.`text` ORDER BY `brand`.`id` DESC SEPARATOR "||") AS text ' +
            ' FROM `brand` ' +
            ' JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` ' +
            '  WHERE `brand`.`id` = '+db.escape(id)+' GROUP BY `brand`.`id`';
        // var prepareSql = '' +
        //     'SELECT `brand`.`image`,`brand`.`brand_logo`,`brand`.`id`,`brand`.`cat_id`,`brand_language`.`lang_id`,`brand_language`.`title`,`brand_language`.`name`,`brand_language`.`text`' +
        //     ' FROM `brand` ' +
        //     ' JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` AND `brand_language`.`lang_id` = 1' +
        //     ' WHERE `brand`.`id` = '+db.escape(id)+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                console.log('rows',rows)
                resolve(rows)
            }
        })
    })
}


var getAllBrandNames = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT ' +
            ' `brand_language`.`name` as brandName,`brand_language`.`brand_id` as brandId ' +
            ' FROM `brand` ' +
            ' JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` ' +
            ' WHERE `brand_language`.`lang_id` = "1" ' +
            ' ORDER BY `brand`.`ord` ASC ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

module.exports.getAllBrands = getAllBrands;
module.exports.getBrand = getBrand;
module.exports.getAllBrandNames = getAllBrandNames;