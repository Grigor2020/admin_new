const db = require('../../connection');
var log = require("../../../logs/log");
let statics = require('../../../static');

var getAllLitrages = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `litrages`.`id`,`litrages`.`name` ' +
            ' FROM `litrages` ' +
            ' ORDER BY `litrages`.`ord` ASC';
        // console.log('....................prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


module.exports.getAllLitrages = getAllLitrages;