const db = require('../../connection');
var log = require("../../../logs/log");


var getAllFaq = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `faq`.`id`,' +
            ' `faq_language`.`name`,`faq_language`.`question` ' +
            ' FROM `faq` ' +
            ' JOIN `faq_language` ON `faq_language`.`faq_id` = `faq`.`id` AND `faq_language`.`lang_id` = "2"' +
            ' ORDER BY `faq`.`ord` ASC ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var getFaq = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `faq`.`id`,' +
            ' GROUP_CONCAT(DISTINCT `faq_language`.`lang_id`  ORDER BY `faq_language`.`id` ASC  SEPARATOR "||") AS lang_id, ' +
            ' GROUP_CONCAT(DISTINCT `faq_language`.`name`  ORDER BY `faq_language`.`id` SEPARATOR "||") AS name, ' +
            ' GROUP_CONCAT(DISTINCT `faq_language`.`answer`  ORDER BY `faq_language`.`id` SEPARATOR "||") AS answer, ' +
            ' GROUP_CONCAT(DISTINCT `faq_language`.`question`  ORDER BY `faq_language`.`id` SEPARATOR "||") AS question ' +
            ' FROM `faq` ' +
            ' JOIN `faq_language` ON `faq_language`.`faq_id` = `faq`.`id` ' +
            ' WHERE `faq`.`id` = '+db.escape(id)+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

module.exports.getAllFaq = getAllFaq;
module.exports.getFaq = getFaq;