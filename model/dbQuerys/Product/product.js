const db = require('../../connection');
var log = require("../../../logs/log");
var staticMethods = require('../../../model/staticMethods')

var getAllProducts = function(page){
    var pagination = staticMethods.detectPagination(page);
    console.log('pagination',pagination)
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `product`.`image`,`product`.`in_stock`,`product`.`price`,`product`.`price_before_sale`,`product`.`sale_percent`,`product`.`id`,`product`.`litrage_id`,' +
            ' `product_language`.`name`,`popular_products`.`product_id` as popularId ' +
            ' FROM `product` ' +
            ' JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "2"' +
            ' LEFT JOIN `popular_products` ON `popular_products`.`product_id` = `product`.`id` ' +
            ' ORDER BY `product`.`id` ASC LIMIT '+pagination.start+','+pagination.limit+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var getProductCount = function(page){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT  COUNT(`product`.`id`) as productRow " +
            " FROM `product`";
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows[0].productRow)
            }
        })
    })
}
var getProductById = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `product`.`image`,`product`.`cover_image`,`product`.`hover_image`,`product`.`price`,`product`.`image_top`,`product`.`litrage_id`,' +
            ' `product`.`price_before_sale`,`product`.`sale_percent`,`product`.`id`,`product`.`brand_id`,' +
            ' GROUP_CONCAT(DISTINCT `filter_product`.`filter_prop_id` ORDER BY `filter_product`.`id` ASC SEPARATOR "||") AS filter_prop_id, ' +
            ' GROUP_CONCAT(DISTINCT `product_language`.`lang_id` ORDER BY `product_language`.`id` ASC  SEPARATOR "||") AS lang_id, ' +
            ' GROUP_CONCAT(DISTINCT `product_language`.`description` ORDER BY `product_language`.`id` ASC  SEPARATOR "||") AS description, ' +
            ' GROUP_CONCAT(DISTINCT `product_language`.`name` ORDER BY `product_language`.`id` ASC  SEPARATOR "||") AS name, ' +
            ' GROUP_CONCAT(DISTINCT `product_language`.`seo_desc` ORDER BY `product_language`.`id` ASC  SEPARATOR "||") AS seo_desc, ' +
            ' GROUP_CONCAT(DISTINCT `product_language`.`seo_title` ORDER BY `product_language`.`id` ASC  SEPARATOR "||") AS seo_title ' +
            ' FROM `product` ' +
            ' JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` ' +
            ' JOIN `filter_product` ON `filter_product`.`product_id` = `product`.`id` ' +
            ' WHERE `product`.`id` = "'+id+'" ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var getCountPopularProduct = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT COUNT(`popular_products`.`id`) as rowCount ' +
            ' FROM `popular_products` ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}
var getPopularProductById = function(id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `popular_products`.`id` ' +
            ' FROM `popular_products` ' +
            ' WHERE `popular_products`.`product_id` = "'+id+'" ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

module.exports.getAllProducts = getAllProducts;
module.exports.getProductById = getProductById;
module.exports.getCountPopularProduct = getCountPopularProduct;
module.exports.getPopularProductById = getPopularProductById;
module.exports.getProductCount = getProductCount;