var query = require('./dbQuerys/querys')
const pageProductCount = 10;

litrages = () => {
    var arr = [];
    const litrages = [0.05,0.25,0.375,0.5,0.7,0.75,1,1.5,1.75,3,4.5]
    for(var i = 0; i < litrages.length; i++){
        arr.push({
            id : i+1,
            name : litrages[i]
        })
    }
    console.log('litrages arr',arr)
    return arr;
}

function detectPagination(page) {
    let obj = {
        start : 0,
        limit : pageProductCount
    }

    obj.start = (page - 1) * pageProductCount;
    return obj
}

var checkPermissions = function(pagePermissions,permissions){
    return new Promise(function (resolve) {
        // console.log('pagePermissions',pagePermissions)
        // console.log('permissions',permissions)
        let selfPermissions = [];

        for (let i = 0; i < permissions.length; i++) {
            if (permissions[i]['permissions_id'] == pagePermissions) {
                selfPermissions.push(permissions[i]);
                break;
            }
        }
        let access = true;
        // console.log('selfPermissions[0]',selfPermissions)
        if (selfPermissions[0].is_open == '0') {
            access = false;
        }
        let retObj = {
            access: access,
            readOrWrite: selfPermissions[0].read_or_write
        };
        // console.log('retObj', retObj)
        // console.log('............',retObj)
        resolve(retObj)
    });
}


var getDateInUnix = function(date,time){
    var sDate = date.split('-');
    var tari = parseInt(sDate[0]);
    var amis = parseInt(sDate[1]);
    var or = parseInt(sDate[2]);

    var sTime = time.split(":");
    var jam = parseInt(sTime[0]);
    var rope = parseInt(sTime[1]);

    var datum = new Date(Date.UTC(tari,amis-1,or,jam,rope,0));
    return datum.getTime()/1000;
};

function generateUrlFrmoString(string) {
    const a = 'àáäâãåăæąçćčđďèéěėëêęğǵḧìíïîįłḿǹńňñòóöôœøṕŕřßşśšșťțùúüûǘůűūųẃẍÿýźžż·/_,:;'
    const b = 'aaaaaaaaacccddeeeeeeegghiiiiilmnnnnooooooprrsssssttuuuuuuuuuwxyyzzz------'
    const p = new RegExp(a.split('').join('|'), 'g')

    return string.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, '-and-') // Replace & with 'and'
        .replace(/[^\w\-]+/g, '') // Remove all non-word characters
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, '') // Trim - from end of text
}

function sortData(table,data,cb){
    var allData = data.split(",");
    var updateQuerys = [];
    for(var i = 0; i < allData.length; i++){
        var idVal = allData[i].split('_');
        var updateData =
            {
                values: {
                    ord: allData.indexOf(allData[i])
                },
                where: {
                    id: idVal[1]
                }
            };
        updateQuerys.push(query.update2vPromise(table,updateData));
    }
    Promise.all(updateQuerys)
        .then(function ([result]) {
            cb({error:false})
        })
        .catch(function (error) {
            cb({error:true})
        })
}
function convertStringToSlug(title){
    var langs = ['’','"',"'",'.',' ','ա','բ','գ','դ','ե','զ','է','ը','թ','ժ','ի','լ','խ','ծ','կ','հ','ձ','ղ','ճ',
        'մ','յ','ն','շ','ո','չ','պ','ջ','ռ','ս','վ','տ','ր','ց','ւ','փ','ք','և','օ','ֆ',
        'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с',
        'т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я','э',
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',
        'p','q','r','s','t','u','v','w','x','y','z',
        '0','1','2','3','4','5','6','7','8','9',
        'á','č','ď','é','ě','í','ň','ó','ř','š','ť','ú','ů','ý','ž'];
    var mirors = ['','','','-','-','a','b','g','d','e','z','e','y','t','zh','i','l','kh','ts','k','h','dz','gh','ch',
        'm','y','n','sh','o','ch','p','j','r','s','v','t','r','c','u','p','q','ev','o','f',
        'a','b','v','g','d','e','io','jh','z','i','i','k','l','m','n','o','p','r','s','t',
        'u','f','kh','c','ch','sh','sh','','','e','u','ia','e','',
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',
        'p','q','r','s','t','u','v','w','x','y','z',
        '0','1','2','3','4','5','6','7','8','9',
        'a','c','d','e','e','i','n','0','r','s','t','u','u','y','z'];

    var $charlist = title.toLowerCase().split('')
    // console.log('$charlist',$charlist)
    var $str_len = 0;
    var $new = '';
    if($charlist.length > 250)
        $str_len = 250;
    else $str_len = $charlist.length;
    for(let i=0;i<$str_len;i++){
        let $key = langs.indexOf($charlist[i]);
        if($key !== -1){
            $new += mirors[$key];
        }else{
            $new += '-'
        }

    }

    let resultString = '';
    var splitedText = $new.split('');
    for(let i = 0; i < splitedText.length; i++){
        if(i === 0 || i === splitedText.length - 1){
            if(splitedText[i] === '-'){
                continue
            }
        }
        if( i !== 0 && i !== splitedText.length - 1){
            if(splitedText[i] === '-' && splitedText[i] === splitedText[i-1]){
                continue;
            }
        }

        resultString+= splitedText[i]
    }

    return resultString;
}

function generateRandomIntegers(length) {
    var result           = '';
    var characters       = '0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return parseInt(result);
}
function generateRandomString(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

module.exports.checkPermissions = checkPermissions;
module.exports.getDateInUnix = getDateInUnix;
module.exports.generateUrlFrmoString = generateUrlFrmoString;
module.exports.sortData = sortData;
module.exports.convertStringToSlug = convertStringToSlug;
module.exports.generateRandomIntegers = generateRandomIntegers;
module.exports.generateRandomString = generateRandomString;
module.exports.detectPagination = detectPagination;
module.exports.pageProductCount = pageProductCount;
module.exports.litrages = litrages;
